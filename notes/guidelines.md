# Guidelines

## general

- keep it responsive
- always develop to look good in both light and dark themes
- pay attention to a11y stuff during dev, then do an a11y sweep later to really improve and polish the experience

## loading animations

- see about using skeleton animations for gallery and any other page that will depend on a potentially slow backend call to load
- use a spinner for things like signing up, logging in, and anything where the components are loaded and you are just waiting for a backend call
