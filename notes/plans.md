# Plans

## MVP

### general

- create logo
- make favicon

### frontend

- responsive
- a11y/WCAG A at least
- user preferences
  - change avatar/profile image (if I decide to add this)
- admin dash
  - for admins, change other user perms, delete users, accept new users, and add upload limits to users for the sake of protecting bandwidth and storage limits
- gallery view
  - display dirs and images like a file explorer
  - image click opens larger image with tags, etc
- image uploads
  - file explorer uploads
  - drag and drop uploads
- mobile
  - keep responsive
  - make a PWA
- extra
  - maybe add a view where users can randomize images order?
  - maybe add slideshow view?
  - fullscreen/full window view of images?
  - explore context menu options to make saving, getting EXIF data, etc easier?

### backend

- GraphQL instead of REST? Not a big deal, but using hooks with Apollo would be nice if the setup isn't too bad, although data not really complex enough for this to be a must as REST call returns wouldn't be that big.
- basic security (helmet, cors, etc)
- more detailed logging
- get Swagger setup working better
- rate limit server calls per user

### unit tests and Storybook

- I'm not going for 100%. I just want decent enough test coverage to potentially catch regressions and find a few potential issues I wasn't previously aware of.
- write backend tests once it is fleshed out enough. Right now I'm still changing too much because I'm new to Fastify and still figuring things out.
- now that Storybook is configured, create stories for eveyerhting to streamline UI development and click testing

### repo

- add standard-version once enough progress is made to want tags/versions

---

## After MVP

### frontend

- "add to favorites" functionality for users to favorite images
- add favorites section for users to view their previously favorited images
- add social share buttons to share directly to social media sites
- add overall feature toggling to admin panel so admin decides if things like social sharing, favorites, etc are turned on. This is a good idea because these things will cost bandwidth and, if hosted on a little VPS, this might be a concern. For exmaple, if someone shares an image from this app and it is hosted on a $6 DigitalOcean server, the monthly bandwidth allotment could be exceeded if the image is linked directly and viewed a lot.
- add high contrast theme options (light and dark) and a way to toggle it in user settings

### backend

- add favorites to users
- add ability to upload avatar/profile pic
- maybe add route to zip all images and download zip? If so, add admin option to disable/enable this feature as it will be CPU heavy

### repo

- add docker config once MVP level features are reached. This would make deployment/usage simpler and encourage more adoption.

---

## Stretch goals

- add image editing tools
