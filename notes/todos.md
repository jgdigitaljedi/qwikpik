# TODOS

## backend

- password reset route (answer security question)
- figure out swagger and improve/make functional for testing routes in Postman
- figure out how long hashed passwords should be at min and do pw validation on that amount
- add something to user model for showing notices
  - notices should be shown after creating account to explain what the user can do
  - notices should be shown after an admin changes someone's role and they login again to explain their new permissions.
- add something to user role to indicate if admin has approved account and restrict EVERYTHING from user account until approved

## frontend

- home should route to a "recent activity" kind of view if user logged in
- forgot password page (use security question)
- see about moving fonts to assets to not rely on Google Fonts
- make sure to route new users to their notice view after the admin approves their account
- make sure to show existing users a notice after login if an admin changed their role
- create view for after account creation explaining that the account will not be active until an admin approves it
- refactor modal hook. It didn't end up being needed the way I wrote it originally. I'd like the modal to be used via hook.

## repo

- once more basic backend functionality is working, write unit tests for server
- get standard version working once basic functionality completed
- add more unit tests (I'm trying to write them once a file has enough functionality that I won't have to go back and completely reqrite them later)

## next up

- tackle backend for images
  - am I doing videos too?
  - how are animated GIFs gonna work?
  - nested directories should be possible
  - provide option to delete exif data on upload? If so, add option to preferences so it can happen automagically.
  - convert to webp/webm after upload? It would be more efficient to store and display, but at what cost?
  - figure out version control for storing images for backend but not including them in repo

## loop back to

- preferences view
  - leaving all "Delete photos" functionality until after I have basic photo upload, storage, and display done
