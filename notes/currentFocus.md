# current focus

## images

- create a way to call only images for each directory
  - do I need to create a "directory" model in the backend for this? If not, is there a simple way for me to send directory strings with nested directory structures seperated by slashes and it wouldn't get too complex?
- create image layout
- images card should expand to fullscreen modal when clicked with image details somewhere
- edit/delete image if you were the uploader or are an admin/sup? Remember to check user perms
- upload images
  - not only upload from backend but store in proper directory (if really even needed) and save image details
  - I need to add a file type filter so non-image files cannot be uploaded. This not only makes sense for the app but makes it more secure. Do this on client and backend for double coverage.
- search images
  - by tag, name, title, or uploader
  - remember to filter by isNsfw user preference
- add delete images functionality to user prefs
  - remember, can only delete their own
- add delete images functionality to admin dash
  - maybe some stats on total images, images per user, recently added image gallery to make it easier to catch ones that need deleting, etc
- I think with my fastify-static setHeaders function that the images will be linkable to other sites, but make sure this is the case (cross origin)
- add social sharing functionality even though I HATE social media (most people don't)
- right now editing images would only mean editing the details, but I could explore actually having a simple image editor that runs 100% client side (contrast, brightness, etc)
- add decent unit test coverage to all gallery and image components
- add user images to profile page which isn't started yet (will worry about other profile stuff later)
