import { Box, ThemeProvider } from 'theme-ui';
import { AppContextProvider, useAppContext } from '../src/contexts/app.context';
import { UserContextProvider, useUserContext } from '../src/contexts/user.context';
import { NotificationContextProvider } from '../src/contexts/notification.context';
import { theme } from '../src/theme/theme';
import NavbarThemeSwitch from '../src/components/NavbarComponent/NavbarThemeSwitch';
import { BrowserRouter } from 'react-router-dom';
import { mockUserContext } from '../src/stories/storybook.mocks';

export const parameters = {
  layout: 'centered',
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  }
};

const Wrapper = ({ children }) => {
  const { appContextValue } = useAppContext();
  const { setUserContext } = useUserContext();
  setUserContext(mockUserContext);

  return (
    <ThemeProvider theme={theme}>
      <AppContextProvider value={appContextValue}>
        <UserContextProvider value={mockUserContext}>
          <NotificationContextProvider>
            <BrowserRouter>
              <Box sx={{ width: '100%' }}>
                <Box sx={{ position: 'absolute', top: '1rem', mb: 4 }}>
                  <NavbarThemeSwitch />
                </Box>
                {children}
              </Box>
            </BrowserRouter>
          </NotificationContextProvider>
        </UserContextProvider>
      </AppContextProvider>
    </ThemeProvider>
  );
};

export const decorators = [
  (Story) => (
    <Wrapper sx={{ width: '100%', height: '100%' }}>
      <Story />
    </Wrapper>
  )
];
