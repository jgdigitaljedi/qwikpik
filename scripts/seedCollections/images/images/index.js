const path = require('path');
const UserModel = require(path.join(__dirname, '../../../../server/models/user.model'));
const today = new Date();

const baseImages = [
  {
    tags: ['twitter', 'marriage', 'meme'],
    title: 'Problem solved',
    isNsfw: true,
    directory: 'memes',
    fileName: 'download (1).jpeg',
    uploadDate: today,
    lastModified: today
  },
  {
    tags: ['irs', 'taxation', 'meme'],
    title: 'Truth!',
    isNsfw: false,
    directory: 'memes',
    fileName: 'download (6).jpeg',
    uploadDate: today,
    lastModified: today
  },
  {
    tags: ['tinfoil', 'tml', 'logo'],
    title: 'Tinfoil My Life',
    isNsfw: false,
    directory: 'tinfoil',
    fileName: 'tinfoilguy390.png',
    uploadDate: today,
    lastModified: today
  },
  {
    tags: ['sky', 'stars', 'wallpaper'],
    title: 'Night sky',
    isNsfw: false,
    directory: 'wallpaper',
    fileName: 'sky-828648_1280.jpg',
    uploadDate: today,
    lastModified: today
  }
];

module.exports = baseImages.map((image) => {
  const uploadedUser = UserModel.findOne({ name: 'Joey' });
  const otherUser = UserModel.findOne({ name: 'Cloud' });
  if (image.fileName === 'download (1).jpeg') {
    image.uploadedBy = otherUser._id;
  } else {
    image.uploadedBy = uploadedUser._id;
  }
  return image;
});
