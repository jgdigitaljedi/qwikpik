const { getObjectId } = require('mongo-seeding');
const User = require('../../../../server/models/user.model');

const now = new Date();

const mockDirs = [
  {
    _id: 'root',
    name: '',
    path: '',
    children: ['logos', 'memes', 'wallpaper'],
    createdDate: now,
    lastModified: now
  },
  {
    _id: 'logos',
    name: 'Logos',
    path: 'root,',
    children: ['tinfoil'],
    createdDate: now,
    lastModified: now
  },
  {
    _id: 'tinfoil',
    name: 'Tinfoil',
    path: 'root,logos,',
    children: [],
    createdDate: now,
    lastModified: now
  },
  {
    _id: 'memes',
    name: 'Memes',
    path: 'root,',
    children: [],
    createdDate: now,
    lastModified: now
  },
  {
    _id: 'wallpaper',
    name: 'Wallpaper',
    path: 'root,',
    children: [],
    createdDate: now,
    lastModified: now
  }
];

module.exports = mockDirs;
