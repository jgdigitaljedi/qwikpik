const { getObjectId } = require('mongo-seeding');

function getRandomBirthday() {
  const year = Math.floor(Math.random() * (2013 - 1960) + 1960);
  const month = Math.floor(Math.random() * (13 - 1) + 1);
  const day = Math.floor(Math.random() * (29 - 1) + 1);
  return new Date(year, month - 1, day);
}

const baseUsers = [
  {
    name: 'Joey',
    username: 'digitaljedi',
    active: true,
    role: 'admin',
    email: 'test@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2', // password is Blahblah123!, but I stored it encrypted for simplicity
    securityQuestion: 'favorite dog',
    securityAnswer: 'Odin',
    preferredLabel: 'name',
    prefersHighContrast: false,
    birthday: new Date(1981, 4, 28)
  },
  {
    name: 'Cloud',
    username: 'FFVII',
    active: true,
    role: 'supervisor',
    email: 'cloud@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'favorite dog',
    securityAnswer: 'Molly',
    preferredLabel: 'name',
    prefersHighContrast: false
  },
  {
    name: 'Kratos',
    username: 'GOW',
    active: false,
    role: 'basic',
    email: 'kratos@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'name',
    prefersHighContrast: true
  },
  {
    name: 'Dante',
    username: 'DMC',
    active: false,
    role: 'basic',
    email: 'dante@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Samus',
    username: 'Metroid',
    active: false,
    role: 'basic',
    email: 'samus@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Aloy',
    username: 'Horizon',
    active: false,
    role: 'basic',
    email: 'aloy@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Doomguy',
    username: 'Doom',
    active: false,
    role: 'basic',
    email: 'doom@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Sam',
    username: 'Serious',
    active: false,
    role: 'basic',
    email: 'sam@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Master Chief',
    username: 'Halo',
    active: false,
    role: 'basic',
    email: 'mc@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'theDeputy',
    username: 'Far Cry',
    active: false,
    role: 'basic',
    email: 'fc5@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Blazkowicz',
    username: 'Wolfenstein',
    active: false,
    role: 'basic',
    email: 'wolf@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Luigi',
    username: 'MarioBros',
    active: false,
    role: 'basic',
    email: 'luigi@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Conker',
    username: 'BadFur',
    active: false,
    role: 'basic',
    email: 'conker@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Link',
    username: 'Zelda',
    active: false,
    role: 'basic',
    email: 'link@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Sonic',
    username: 'TheHedgehog',
    active: false,
    role: 'basic',
    email: 'sonic@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Fox',
    username: 'StarFox',
    active: false,
    role: 'basic',
    email: 'fox@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Gordon',
    username: 'HalfLife',
    active: false,
    role: 'basic',
    email: 'gordon@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Ratchet',
    username: 'AndClank',
    active: false,
    role: 'basic',
    email: 'rc@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Walker',
    username: 'Rage',
    active: false,
    role: 'basic',
    email: 'walker@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Eddie',
    username: 'BrutalLegend',
    active: false,
    role: 'basic',
    email: 'eddie@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Lara Croft',
    username: 'TombRaider',
    active: false,
    role: 'basic',
    email: 'lara@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Strife',
    username: 'Darksiders',
    active: false,
    role: 'basic',
    email: 'strife@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Artyom',
    username: 'MetroExodus',
    active: false,
    role: 'basic',
    email: 'metro@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  },
  {
    name: 'Geralt',
    username: 'Witcher',
    active: false,
    role: 'basic',
    email: 'geralt@test.com',
    password: '$2a$08$vLstFMkC0JVcfj9jtK51TeDg83TYIUdka3GahfylE48r0sh6tsGX2',
    securityQuestion: 'answer is UP',
    securityAnswer: 'UP',
    preferredLabel: 'username',
    prefersHighContrast: false
  }
];

module.exports = baseUsers.map((user, index) => {
  return {
    ...user,
    hideNsfwImages: index < (baseUsers.length - 1) / 2,
    hideNsfwComments: index < (baseUsers.length - 1) / 2,
    birthday: user.birthday || getRandomBirthday(),
    dateTimeFormat: 'MM/dd/yyyy hh:mm a',
    dateCreated: new Date(),
    lastModified: new Date()
  };
});
