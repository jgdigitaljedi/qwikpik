import { Seeder } from 'mongo-seeding';
import path from 'path';
import chalk from 'chalk';
import { MongoClient } from 'mongodb';
const mongo = new MongoClient('mongodb://localhost:27017/qwikpik');

const config = {
  database: {
    host: 'localhost',
    port: 27017,
    name: 'qwikpik'
  }
};

async function seedUsers() {
  try {
    const seeder = new Seeder({ ...config });
    const userColl = seeder.readCollectionsFromPath(
      path.resolve('./scripts/seedCollections/users')
    );
    await seeder.import(userColl);
    console.log(chalk.green.bold('You users were successfully seeded in your DB.'));
  } catch (error) {
    console.log(chalk.bold.red('There was an error seeding users in your DB.'));
    console.error(error);
    process.exit(1);
  }
}

async function seedDirs() {
  try {
    const connection = await mongo.connect();
    const usersColl = await connection.db('qwikpik').collection('users');
    const user = await usersColl.findOne({ name: 'Joey' });
    const otherUser = await usersColl.findOne({ name: 'Cloud' });
    const seeder = new Seeder({ ...config });
    const dirsColl = seeder.readCollectionsFromPath(
      path.resolve('./scripts/seedCollections/directories'),
      {
        transformers: [
          (data) => {
            const dirs = data.documents;
            const withUserIds = dirs.map((dir) => {
              if (dir._id === 'memes') {
                dir.createdBy = otherUser._id;
              } else {
                dir.createdBy = user._id;
              }
              return dir;
            });
            data.documents = withUserIds;
            return data;
          }
        ]
      }
    );
    await seeder.import(dirsColl);
    connection.close();
    console.log(chalk.green.bold('You directories were successfully seeded in your DB.'));
  } catch (error) {
    console.log(chalk.bold.red('There was an error seeding dirs in your DB.'));
    console.error(error);
    process.exit(1);
  }
}

async function seedImages() {
  try {
    const connection = await mongo.connect();
    const usersColl = await connection.db('qwikpik').collection('users');
    const user = await usersColl.findOne({ name: 'Joey' });
    const otherUser = await usersColl.findOne({ name: 'Cloud' });

    const seeder = new Seeder(config);
    const imageColl = seeder.readCollectionsFromPath(
      path.resolve('./scripts/seedCollections/images'),
      {
        transformers: [
          (data) => {
            const images = data.documents;
            const withUserIds = images.map((image) => {
              if (image.directory === 'memes') {
                image.uploadedBy = otherUser._id;
              } else {
                image.uploadedBy = user._id;
              }
              return image;
            });
            data.documents = withUserIds;
            return data;
          }
        ]
      }
    );
    await seeder.import(imageColl);
    connection.close();
    console.log(chalk.green.bold('You images were successfully seeded in your DB.'));
  } catch (error) {
    console.log(chalk.bold.red('There was an error seeding images in your DB.'));
    console.error(error);
    process.exit(1);
  }
}

await seedUsers();
await seedDirs();
await seedImages();
