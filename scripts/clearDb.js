const MongoClient = require('mongodb').MongoClient;
const url = `mongodb://localhost:27017/`;

MongoClient.connect(url, function (err, db) {
  if (err) throw err;
  var dbo = db.db('qwikpik');
  dbo.collection('images').drop(function (err, delOK) {
    if (err) {
      console.log(chalk.red.bold('Images not deleted'));
      console.log('error', err);
    }
    if (delOK) console.log('Images collection deleted');
    db.close();
  });

  dbo.collection('directories').drop(function (err, delOK) {
    if (err) {
      console.log(chalk.red.bold('Directories not deleted'));
      console.log('error', err);
    }
    if (delOK) console.log('Directories collection deleted');
    db.close();
  });

  dbo.collection('users').drop(function (err, delOK) {
    if (err) {
      console.log(chalk.red.bold('Users not deleted'));
      console.log('error', err);
    }
    if (delOK) console.log('Users collection deleted');
    db.close();
  });
});
