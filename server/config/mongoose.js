const fp = require('fastify-plugin');
const mongoose = require('mongoose');
const User = require('../models/user.model');
const Image = require('../models/image.model');
const Comment = require('../models/comment.model');
const models = { User, Image, Comment };
const ConnectDB = async (fastify, options) => {
  try {
    mongoose.connection.on('connected', () => {
      fastify.log.info({ actor: 'MongoDB' }, 'connected');
    });
    mongoose.connection.on('disconnected', () => {
      fastify.log.error({ actor: 'MongoDB' }, 'disconnected');
    });
    const db = await mongoose.connect(options.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    // decorates fastify with our model
    fastify.decorate('db', { models });
  } catch (error) {
    fastify.log.error({ actor: 'MongoDB', error });
  }
};

module.exports = fp(ConnectDB);
