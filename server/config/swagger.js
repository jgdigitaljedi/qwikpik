const pJson = require('../../package.json');

exports.options = {
  routePrefix: '/documentation',
  exposeRoute: true,
  swagger: {
    info: {
      title: 'QwikPik API',
      description: 'API for QwikPik photo gallery built with Node, Fastify, and Swagger',
      version: pJson.version
    },
    externalDocs: {
      url: 'https://swagger.io',
      description: 'Find more info here'
    },
    host: 'localhost',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json']
  }
};
