const User = require('../models/user.model');
const fp = require('fastify-plugin');
const { HTTP_CODES } = require('./constants');

module.exports.verifyJWT = fp(async function (fastify, opts) {
  fastify.register(require('fastify-jwt'), {
    secret: process.env.JWT_SECRET
  });

  fastify.decorate('authenticate', async function (request, reply) {
    try {
      await request.jwtVerify();
      const userToken = await User.findById(request.user.userPayload._id);
      if (!userToken) {
        reply
          .status(HTTP_CODES.UNAUTHORIZED)
          .send({ error: true, message: 'You must be logged in to do that.' });
      }
    } catch (err) {
      reply(HTTP_CODES.UNAUTHORIZED).send(err);
    }
  });
});

module.exports.verifyUsernamePassword = async function (request, reply) {
  try {
    if (!request.body) {
      reply
        .code(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'Username and password are required!' });
    }
    const user = await User.findByCredentials(request.body.username, request.body.password);
    request.user = user;
  } catch (error) {
    reply
      .code(HTTP_CODES.UNAUTHORIZED)
      .send({ error: true, message: 'The username and password you provided do not match.' });
  }
};
