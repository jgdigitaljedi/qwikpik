const User = require('../models/user.model');

const lettersNumbersRegex = /^[\w\s]+$/;

function validatePassword(passwordHash) {
  return passwordHash?.length > 8 ? null : 'Password does not meet the requirements.';
}

function validateUsername(username) {
  return username?.length > 3
    ? null
    : 'Username is not long enough (must be at least 4 characters).';
}

function validateName(name) {
  return name?.length > 0 && name.match(lettersNumbersRegex)
    ? null
    : 'Name is invalid and must be at least 1 letter and contain only letters and numbers.';
}

async function validateEmail(email) {
  if (!email) {
    return Promise.resolve('You must provide an email address to register.');
  }
  const isValid = email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  if (!isValid) {
    return Promise.resolve('Email address is not valid');
  }
  const inUse = await User.find({ email });
  if (inUse?.length > 0) {
    return Promise.resolve('Email address already registered.');
  }
  return Promise.resolve(null);
}

function validateSecurity(str) {
  return str?.length > 0;
}

async function validateNewUser(user) {
  try {
    const emailValid = await validateEmail(user.email);
    return Promise.resolve(
      [
        { key: 'username', error: validateUsername(user.username) },
        { key: 'password', error: validatePassword(user.password) },
        { key: 'name', error: validateName(user.name) },
        { key: 'email', error: emailValid },
        { key: 'secQuestion', error: validateSecurity(user.secQuestion) },
        { key: 'secAnswer', error: validateSecurity(user.secAnswer) }
      ].filter((e) => e.error)
    );
  } catch (error) {
    return Promise.reject('Something went wrong trying to create new user.');
  }
}

function removeValueFromUserAfterUpdate(user) {
  if (user?.value) {
    return user.value;
  }
  return user;
}

module.exports = {
  removeValueFromUserAfterUpdate,
  validateNewUser,
  validateName,
  validatePassword,
  validateSecurity,
  validateUsername
};
