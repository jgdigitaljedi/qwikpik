const fs = require('fs');
const util = require('util');
const { pipeline } = require('stream');
const pump = util.promisify(pipeline);
const piexif = require('piexifjs');

async function removeExifFromJpeg(image) {
  const imgData = fs.readFileSync(srcImgPath).toString('binary');
  const newImgData = piexif.remove(imgData);
  Promise.resolve(newImgData);
}

module.exports.uploadImage = async function (file, details) {
  console.log('file', file);
  try {
    const data = await file();
    console.log('data', data);
    const isJpeg = file?.filename?.split('/')[1].toLowerCase() === 'jpeg';
    let cleanedImage = isJpeg ? await removeExifFromJpeg(data.file) : data.file;
    await pump(cleanedImage, fs.createWriteStream(data.filename));
    return Promise.resolve({ error: false, message: 'File successfully uploaded' });
  } catch (error) {
    return Promise.reject({ error: true, message: error });
  }
};
