const { HTTP_CODES } = require('../config/constants');
const Image = require('../models/image.model');
const { uploadImage } = require('../util/image.util');

// get all images
exports.getImages = async (req, reply) => {
  try {
    const directory = req.body.directory;
    if (directory) {
      const images = await Image.find({ directory });
      return { images };
    } else {
      const images = await Image.find();
      return { images };
    }
  } catch (err) {
    reply
      .code(HTTP_CODES.SERVER_ERROR)
      .send({ error: true, message: 'There was an error fetching your images.' });
  }
};

// Get single image by ID
exports.getSingleImage = async (req, reply) => {
  try {
    const id = req.params.id;
    const image = await Image.findById(id);
    return image;
  } catch (err) {
    reply.code(500).send(err);
  }
};

// Upload and save image array
exports.uploadImages = async (req, reply) => {
  try {
    const images = req.body;
    console.log('reqBody', images);
    if (images?.length) {
      const streamed = images.map(async (image) => {
        const uploaded = await uploadImage(image.fileInfo.file);
        if (uploaded) {
          return uploadImage;
        }
        return null;
      });
      console.log('streamed', streamed);
    }
    reply.send(true); // TODO: change response once logic worked out

    // TODO: actually move image to correct directory within library
    // TODO: save image data to DB
    // TODO: if image data save fails, remove image file
  } catch (error) {
    reply.code(500).send(error);
  }
};

// Update an existing image
exports.updateImage = async (req, reply) => {
  try {
    const id = req.params.id;
    const image = req.body;
    const { ...updateData } = image;
    const update = await Image.findByIdAndUpdate(id, updateData, { new: true });
    return update;
  } catch (err) {
    reply.code(500).send(err);
  }
};

// Delete an image
exports.deleteImage = async (req, reply) => {
  try {
    const id = req.params.id;
    const image = await Image.findByIdAndRemove(id);
    return image;
  } catch (err) {
    reply.code(500).send(err);
  }
};
