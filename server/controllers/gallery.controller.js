const { HTTP_CODES } = require('../config/constants');
const Directory = require('../models/directory.model');
const Image = require('../models/image.model');

exports.getGallery = async (req, reply) => {
  try {
    const dir = req.body.directory || 'root';
    const images = await Image.find({ directory: dir });
    const directory = await Directory.findById(dir);
    return { images, directory };
  } catch (err) {
    reply.code(HTTP_CODES.SERVER_ERROR).send({
      error: true,
      details: err,
      message: 'There was an error fetching your gallery data.'
    });
  }
};
