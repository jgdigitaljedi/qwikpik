const { DateTime } = require('luxon');
const { HTTP_CODES } = require('../config/constants');
const Directory = require('../models/directory.model');

exports.getDirectories = async (req, reply) => {
  try {
    const directory = req.body.directory;
    if (directory) {
      const dirs = await Directory.findById(directory.toLowerCase());
      return { dirs };
    } else {
      const dirs = await Directory.find();
      return { dirs };
    }
  } catch (err) {
    reply
      .code(HTTP_CODES.SERVER_ERROR)
      .send({ error: true, message: 'There was an error fetching your directories.' });
  }
};

exports.addDirectory = async (req, reply) => {
  try {
    const ownerId = req.user.userPayload._id;
    const newDirName = req.body.newDirName;
    const potentialId = newDirName.toLowerCase();
    const currentDirectory = req.body.currentDirectory;
    const currentChildren = currentDirectory.children;

    // check that potentialId isn't already an _id. If it is, add a unix ts to make unique
    const idCheck = await Directory.findById(potentialId);
    const newDirId = idCheck?.length ? `${potentialId}-${DateTime.now().toMillis()}` : potentialId;

    // create new dir
    const now = DateTime.now();
    const newDirObj = {
      name: newDirName,
      _id: newDirId,
      createdDate: now,
      lastModified: now,
      children: [],
      createdBy: ownerId,
      path: `${currentDirectory.path}${currentDirectory._id},`
    };
    const newDir = await new Directory(newDirObj);
    newDir.save();

    // add new dir _id to children of currentDirectory
    const newChildren = [...currentChildren, newDirId];
    const updatedParent = await Directory.findByIdAndUpdate(
      currentDirectory._id,
      { $set: { children: newChildren, lastModified: DateTime.now() } },
      { new: true, rawResult: true }
    );
    reply.status(HTTP_CODES.CREATED).send(updatedParent);
  } catch (err) {
    reply
      .code(HTTP_CODES.SERVER_ERROR)
      .send({ error: true, message: 'There was an error adding your new directory.', diag: err });
  }
};
