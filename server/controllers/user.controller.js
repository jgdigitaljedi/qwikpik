const { DateTime } = require('luxon');
const { HTTP_CODES } = require('../config/constants');
const User = require('../models/user.model');
const {
  validateNewUser,
  removeValueFromUserAfterUpdate,
  validatePassword
} = require('../util/users.util');
const bcrypt = require('bcryptjs');
const _sortBy = require('lodash').sortBy;

async function setNewUserPermissions() {
  const users = await User.find();
  if (users?.length === 0) {
    return { role: 'admin', active: true };
  }
  return { role: 'basic', active: false };
}

function getBaseUserForPayload(newUser) {
  return {
    username: newUser.username,
    active: newUser.active,
    role: newUser.role,
    dateCreated: newUser.dateCreated,
    lastModified: newUser.lastModified,
    name: newUser.name,
    birthday: newUser.birthday,
    hideNsfwImages: newUser.hideNsfwImages,
    hideNsfwComments: newUser.hideNsfwComments,
    preferredLabel: newUser.preferredLabel,
    prefersHighContrast: newUser.prefersHighContrast,
    dateTimeFormat: newUser.dateTimeFormat,
    email: newUser.email,
    _id: newUser._id
  };
}

function getUserTokenReturn(userPayload, token) {
  return { token, ...userPayload };
}

async function validateCurrentAndNewPw(user, currentPw, newPw) {
  try {
    const userFromCreds = await User.findByCredentials(user.username, currentPw);
    Promise.resolve(userFromCreds);
  } catch (error) {
    throw new Error(error.message);
  }
  const newPwHashed = await bcrypt.hash(newPw, 8);
  const pwInvalid = validatePassword(newPwHashed);
  if (pwInvalid) {
    return Promise.resolve({ error: true, message: pwInvalid });
  }
  return Promise.resolve({ password: newPwHashed });
}

function canToggleNsfw(birthday) {
  const bday = DateTime.fromJSDate(birthday || new Date());
  const today = DateTime.now();
  const diffInYears = today.diff(bday, 'years');
  return diffInYears >= 17;
}

// get all users
exports.getUsers = async (req, reply) => {
  try {
    let users = [];
    const role = req.user.userPayload.role;
    if (role !== 'basic' && req.body.inactiveOnly) {
      users = await User.find({ active: false });
    } else if (role !== 'basic') {
      users = await User.find();
    } else {
      reply
        .status(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'You do not have permission to do that.' });
    }
    let filteredUsers = [];
    if (role === 'supervisor') {
      filteredUsers = users.filter((user) => user.role === 'basic');
    } else {
      filteredUsers = users;
    }
    if (req.body.amount) {
      const start = req.body.start || 0;
      const amount = req.body.amount;
      const sortBySplit = (req.body.sortBy || 'name-asc').split('-');
      const sortedUsers = _sortBy(filteredUsers, sortBySplit[0]);
      if (sortBySplit[1] === 'desc') {
        sortedUsers.reverse();
      }
      const startIndex = start ? start * amount : start;
      const userPage = sortedUsers.slice(startIndex, startIndex + amount);

      return { users: userPage, total: filteredUsers.length };
    }
    return users;
  } catch (err) {
    reply.code(HTTP_CODES.SERVER_ERROR).send(err);
  }
};

exports.userToken = async (req, reply) => {
  try {
    reply.send(removeValueFromUserAfterUpdate(req.user.userPayload));
  } catch (error) {
    reply.code(HTTP_CODES.SERVER_ERROR).send(error);
  }
};

// Get single user by ID
exports.getSingleUser = async (req, reply) => {
  reply.send({ status: 'Authenticated!', user: req.user });
};

// Add a new user
exports.addUser = async (req, reply) => {
  const user = req?.body?.user || {};
  if (user.active) {
    delete user.active;
  }
  try {
    const validateUserErrors = await validateNewUser(user);

    if (validateUserErrors?.length === 0) {
      const now = DateTime.now();
      const userPerms = await setNewUserPermissions();
      const nsfwDefault = canToggleNsfw();
      const newUser = await new User({
        ...user,
        role: userPerms.role,
        active: userPerms.active,
        lastModified: now,
        dateCreated: now,
        prefersHighContrast: false,
        preferredLabel: 'name',
        hideNsfwComments: nsfwDefault,
        hideNsfwImages: nsfwDefault,
        email: user.email.toLowerCase()
      });
      await newUser.save();

      const userPayload = getBaseUserForPayload(newUser);

      const token = await reply.jwtSign({ userPayload }, { expiresIn: '3d' });
      reply.status(HTTP_CODES.CREATED).send(getUserTokenReturn(userPayload, token));
    } else {
      reply.status(HTTP_CODES.SERVER_ERROR).send({ errors: validateUserErrors });
    }
  } catch (error) {
    reply.code(HTTP_CODES.SERVER_ERROR).send(error);
  }
};

// User updating their own usr object
exports.updateSelf = async (req, reply) => {
  if (req.body.prop) {
    try {
      let userPropAndValue = null;
      const key = Object.keys(req.body.prop)[0];
      const cleanedUser = removeValueFromUserAfterUpdate(req.user.userPayload);
      if (key === 'password') {
        try {
          userPropAndValue = await validateCurrentAndNewPw(
            cleanedUser,
            req.body.currentPw,
            req.body.prop[key]
          );
        } catch (error) {
          userPropAndValue = { error: true, message: 'Your current password is incorrect.' };
          reply.status(HTTP_CODES.UNAUTHORIZED).send(userPropAndValue);
        }
      } else if (key === 'securityQuestion') {
        if (!req.body.prop.securityQuestion || !req.body.securityAnswer) {
          userPropAndValue = {
            error: true,
            message: 'Your security question and answer must be at least 1 character each.'
          };
          reply.status(HTTP_CODES.UNAUTHORIZED).send(userPropAndValue);
        } else {
          userPropAndValue = {
            securityQuestion: req.body.prop.securityQuestion,
            securityAnswer: req.body.securityAnswer
          };
        }
      } else if (key === 'email') {
        const lcEmail = req.body.prop.email.toLowerCase();
        const users = await User.find({ email: lcEmail });
        if (users.length) {
          userPropAndValue = { error: true, message: 'That email address is already in use.' };
          reply.status(HTTP_CODES.BAD_REQUEST).send(userPropAndValue);
        } else {
          userPropAndValue = { email: lcEmail };
        }
      }

      if (userPropAndValue?.error) {
        reply.status(HTTP_CODES.UNAUTHORIZED).send(userPropAndValue);
        return;
      }
      if (!userPropAndValue) {
        userPropAndValue = req.body.prop;
      }
      const userPayload = await User.findByIdAndUpdate(
        cleanedUser._id,
        { $set: { ...userPropAndValue, lastModified: DateTime.now() } },
        { new: true, rawResult: true }
      );
      const baseUser = getBaseUserForPayload(removeValueFromUserAfterUpdate(userPayload));
      const token = await reply.jwtSign({ userPayload: baseUser }, { expiresIn: '3d' });
      const newUserObj = getUserTokenReturn(baseUser, token);
      reply.status(HTTP_CODES.SUCCESS).send(newUserObj);
    } catch (error) {
      reply.code(HTTP_CODES.SERVER_ERROR).send(error);
    }
  } else {
    reply.status(HTTP_CODES.BAD_REQUEST);
  }
};

// Delete an user
exports.deleteUser = async (req, reply) => {
  try {
    const id = req.params.id;
    const user = await User.findByIdAndRemove(id);
    return user;
  } catch (err) {
    reply.code(HTTP_CODES.SERVER_ERROR).send(err);
  }
};

exports.login = async (req, reply) => {
  try {
    const cleanedUser = removeValueFromUserAfterUpdate(req.user);
    const baseUser = getBaseUserForPayload(cleanedUser);
    const newToken = await reply.jwtSign({ userPayload: baseUser }, { expiresIn: '3d' });
    reply.send({ status: 'You are logged in', user: { ...baseUser, token: newToken } });
  } catch (error) {
    reply
      .code(HTTP_CODES.SERVER_ERROR)
      .send({ error: true, message: 'There was an error logging you in.' });
  }
};
