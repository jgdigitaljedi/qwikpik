const { HTTP_CODES } = require('../config/constants');
const User = require('../models/user.model');
const { DateTime } = require('luxon');
const bcrypt = require('bcryptjs/dist/bcrypt');

exports.adminApprove = async (req, reply) => {
  try {
    const callerRole = req.user.userPayload.role;
    if (callerRole === 'admin' || callerRole === 'supervisor') {
      if (req.body.whoseId) {
        const userPayload = await User.findByIdAndUpdate(
          req.body.whoseId,
          { $set: { active: true, lastModified: DateTime.now() } },
          { new: true, rawResult: true }
        );
        reply.status(HTTP_CODES.SUCCESS).send(userPayload);
      } else {
        reply
          .send(HTTP_CODES.BAD_REQUEST)
          .send({ error: true, message: 'You must send a user to approve.' });
      }
    } else {
      reply
        .code(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'You do not have permission to do that.' });
    }
  } catch (error) {
    console.log('error', error);
    reply.code(HTTP_CODES.SERVER_ERROR).send({ error: true, message: error });
  }
};

exports.adminDelete = async (req, reply) => {
  try {
    const callerRole = req.user.userPayload.role;
    if (callerRole === 'admin' || callerRole === 'supervisor') {
      if (req.body.whoseId) {
        const userPayload = await User.findByIdAndDelete(req.body.whoseId);
        reply.status(HTTP_CODES.SUCCESS).send(true);
      } else {
        reply
          .send(HTTP_CODES.BAD_REQUEST)
          .send({ error: true, message: 'You must send a user to approve.' });
      }
    } else {
      reply
        .code(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'You do not have permission to do that.' });
    }
  } catch (error) {
    console.log('error', error);
    reply.code(HTTP_CODES.SERVER_ERROR).send({ error: true, message: error });
  }
};

exports.adminDeactivate = async (req, reply) => {
  try {
    const callerRole = req.user.userPayload.role;
    if (callerRole === 'admin' || callerRole === 'supervisor') {
      if (req.body.whoseId) {
        const userPayload = await User.findByIdAndUpdate(
          req.body.whoseId,
          { $set: { active: false, lastModified: DateTime.now() } },
          { new: true, rawResult: true }
        );
        reply.status(HTTP_CODES.SUCCESS).send(userPayload);
      } else {
        reply
          .send(HTTP_CODES.BAD_REQUEST)
          .send({ error: true, message: 'You must send a user to approve.' });
      }
    } else {
      reply
        .code(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'You do not have permission to do that.' });
    }
  } catch (error) {
    console.log('error', error);
    reply.code(HTTP_CODES.SERVER_ERROR).send({ error: true, message: error });
  }
};

exports.adminUserStats = async (req, reply) => {
  try {
    const callerRole = req.user.userPayload.role;
    if (callerRole === 'admin' || callerRole === 'supervisor') {
      const users = await User.find();
      const filteredUsers =
        callerRole === 'supervisor' ? users.filter((user) => user.role === 'basic') : users;
      const stats = {
        total: filteredUsers.length,
        active: filteredUsers.filter((user) => user.active).length,
        inactive: filteredUsers.filter((user) => !user.active).length
      };
      reply.status(HTTP_CODES.SUCCESS).send(stats);
    } else {
      reply
        .code(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'You do not have permission to do that.' });
    }
  } catch (error) {
    console.log('error', error);
    reply.code(HTTP_CODES.SERVER_ERROR).send({ error: true, message: error });
  }
};

exports.adminUserEdit = async (req, reply) => {
  try {
    const callerRole = req.user.userPayload.role;
    if (callerRole === 'admin' || callerRole === 'supervisor') {
      const editedUserReq = req.body.toEdit;
      const editedUserObj = User.findById(editedUserReq._id);
      if (editedUserReq?.password !== editedUserObj.password) {
        const onlyHashed = editedUserReq.password;
        editedUserReq.password = await bcrypt.hash(onlyHashed, 8);
      }
      const saved = await User.updateOne(
        { _id: editedUserReq._id },
        { $set: { ...editedUserReq, lastModified: DateTime.now() } },
        { new: true, rawResult: true }
      );
      reply.status(HTTP_CODES.SUCCESS).send(saved.acknowledged);
    } else {
      reply
        .code(HTTP_CODES.UNAUTHORIZED)
        .send({ error: true, message: 'You do not have permission to do that.' });
    }
  } catch (error) {
    console.log('error', error);
    reply.code(HTTP_CODES.SERVER_ERROR).send({ error: true, message: error });
  }
};
