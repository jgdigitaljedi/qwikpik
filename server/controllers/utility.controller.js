const User = require('../models/user.model');

// get all users
exports.checkIfEmailTaken = async (req, reply) => {
  try {
    const users = await User.find({ email: req.body.email?.toLowerCase() });
    reply.send({ inUse: users.length > 0 });
  } catch (err) {
    reply.code(500).send(err);
  }
};
