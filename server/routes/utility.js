const { checkIfEmailTaken } = require('../controllers/utility.controller');

const usersRoutes = async (fastify, opts) => {
  fastify.after(() => {
    // our routes goes here
    fastify.route({
      method: ['POST', 'HEAD'],
      url: '/email',
      logLevel: 'warn',
      handler: checkIfEmailTaken
    });
  });
};

module.exports = usersRoutes;
