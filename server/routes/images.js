const imageController = require('../controllers/image.controller');
const FastifyAuth = require('fastify-auth');
const { verifyJWT } = require('../config/authDecorators');

const imagesRoutes = async (fastify, opts) => {
  fastify
    .decorate('asyncVerifyJWT', verifyJWT)
    .register(FastifyAuth)
    .after(() => {
      fastify.route({
        method: ['POST', 'HEAD'],
        url: '/images',
        logLevel: 'warn',
        handler: imageController.getImages
      });

      fastify.route({
        method: ['GET', 'HEAD'],
        url: '/images/:id',
        logLevel: 'warn',
        handler: imageController.getSingleImage
      });

      // fastify.route({
      //   method: 'PUT',
      //   url: '/images',
      //   logLevel: 'warn',
      //   preValidation: [fastify.authenticate],
      //   handler: imageController.addImage
      // });

      fastify.route({
        method: 'POST',
        url: '/images/:id',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: imageController.updateImage
      });

      fastify.route({
        method: 'DELETE',
        url: '/images/:id',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: imageController.deleteImage
      });

      fastify.route({
        method: 'PUT',
        url: '/upload',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: imageController.uploadImages
      });
    });
};

module.exports = imagesRoutes;
