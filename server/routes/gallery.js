const galleryController = require('../controllers/gallery.controller');
const FastifyAuth = require('fastify-auth');
const { verifyJWT } = require('../config/authDecorators');

const galleryRoutes = async (fastify, opts) => {
  fastify
    .decorate('asyncVerifyJWT', verifyJWT)
    .register(FastifyAuth)
    .after(() => {
      fastify.route({
        method: ['POST', 'HEAD'],
        url: '/gallery',
        logLevel: 'warn',
        handler: galleryController.getGallery
      });
    });
};

module.exports = galleryRoutes;
