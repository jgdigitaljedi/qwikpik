const FastifyAuth = require('fastify-auth');
const {
  addUser,
  login,
  getSingleUser,
  userToken,
  updateSelf,
  getUsers
} = require('../controllers/user.controller');
const { verifyUsernamePassword, verifyJWT } = require('../config/authDecorators');

const usersRoutes = async (fastify, opts) => {
  fastify
    .decorate('asyncVerifyJWT', verifyJWT)
    .decorate('asyncVerifyUsernameAndPassword', verifyUsernamePassword)
    .register(FastifyAuth)
    .after(() => {
      // signup route
      fastify.route({
        method: ['POST', 'HEAD'],
        url: '/register',
        logLevel: 'warn',
        handler: addUser
      });

      // login route
      fastify.route({
        method: ['POST', 'HEAD'],
        url: '/login',
        logLevel: 'warn',
        preHandler: fastify.auth([fastify.asyncVerifyUsernameAndPassword]),
        handler: login
      });

      // get user object from token route
      fastify.route({
        method: 'GET',
        url: '/usertoken',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: userToken
      });

      fastify.route({
        method: 'POST',
        url: '/updateself',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: updateSelf
      });

      // proifle route
      // TODO: put here in beginning, but I think it can be deleted now
      fastify.route({
        method: ['GET', 'HEAD'],
        url: '/profile',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: getSingleUser
      });

      // TODO: not fully fleshed out, auth guard this later
      fastify.route({
        method: 'POST',
        url: '/getusers',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: getUsers
      });
    });
};

module.exports = usersRoutes;
