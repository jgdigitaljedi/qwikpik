const dirController = require('../controllers/directory.controller');
const FastifyAuth = require('fastify-auth');
const { verifyJWT } = require('../config/authDecorators');

const directoryRoutes = async (fastify, opts) => {
  fastify
    .decorate('asyncVerifyJWT', verifyJWT)
    .register(FastifyAuth)
    .after(() => {
      fastify.route({
        method: ['POST', 'HEAD'],
        url: '/directories',
        logLevel: 'warn',
        handler: dirController.getDirectories
      });

      fastify.route({
        method: 'PUT',
        url: '/directories',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: dirController.addDirectory
      });
    });
};

module.exports = directoryRoutes;
