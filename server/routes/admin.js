const FastifyAuth = require('fastify-auth');
const { verifyJWT } = require('../config/authDecorators');
const {
  adminApprove,
  adminDelete,
  adminDeactivate,
  adminUserStats,
  adminUserEdit
} = require('../controllers/admin.controller');

const adminRoutes = async (fastify, opts) => {
  fastify
    .decorate('asyncVerifyJWT', verifyJWT)
    .register(FastifyAuth)
    .after(() => {
      // approve user
      fastify.route({
        method: 'POST',
        url: '/approve',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: adminApprove
      });

      // delete user
      fastify.route({
        method: 'DELETE',
        url: '/deleteuser',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: adminDelete
      });

      // deactivate user
      fastify.route({
        method: 'POST',
        url: '/deactivateuser',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: adminDeactivate
      });

      // user stats
      fastify.route({
        method: 'GET',
        url: '/adminuserstats',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: adminUserStats
      });

      // edit user
      fastify.route({
        method: 'POST',
        url: '/adminedituser',
        logLevel: 'warn',
        preValidation: [fastify.authenticate],
        handler: adminUserEdit
      });
    });
};

module.exports = adminRoutes;
