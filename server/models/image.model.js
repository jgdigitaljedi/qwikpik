const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
  tags: [String],
  title: String,
  isNsfw: Boolean,
  directory: { type: String, required: true },
  uploadedBy: { type: mongoose.Schema.ObjectId, ref: 'User' },
  fileName: {
    type: String,
    required: true
  },
  uploadDate: Date,
  lastModified: Date
});

module.exports = mongoose.model('Image', imageSchema);
