const mongoose = require('mongoose');

const directorySchema = new mongoose.Schema(
  {
    _id: String, // make same as name but lowercase only; useful for path; MUST BE UNIQUE so do check on creation
    name: String, // whatever; maybe no special characters
    path: String, // comma delimited list of _ids to get there; ex. ',logos,tinfoil,svg',
    children: [String], // array of directory _ids of immediate children; this would make getting subdirs easy
    createdBy: { type: mongoose.Schema.ObjectId, ref: 'User' },
    createdDate: Date,
    lastModified: Date
  },
  { _id: false }
);

module.exports = mongoose.model('Directory', directorySchema);
