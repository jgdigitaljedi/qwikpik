const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
  author: { type: mongoose.Schema.ObjectId, ref: 'User', required: true },
  content: {
    type: String,
    require: true
  },
  targetImage: { type: mongoose.Schema.ObjectId, ref: 'Image', required: true },
  commentDate: {
    type: Date,
    required: true
  },
  lastModified: {
    type: Date,
    required: true
  },
  isNsfw: {
    type: Boolean,
    required: true
  }
});

commentSchema.pre('save', async function (next) {
  const comment = this;
  const now = new Date();
  comment.lastModified = now;
  if (!comment.commentDate) {
    comment.commentDate = now;
  }
  next();
});

module.exports = mongoose.model('Comment', commentSchema);
