const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/** roles
 * - basic: can upload and view images, can delete images they uploaded
 * - supervisor: same as basic but can delete any image
 * - admin: same as supervisor but can delete users
 */

const userSchema = new mongoose.Schema({
  name: String,
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  role: {
    required: true,
    type: String,
    default: 'basic',
    enum: ['basic', 'supervisor', 'admin']
  },
  securityQuestion: {
    type: String,
    required: true
  },
  securityAnswer: {
    type: String,
    required: true
  },
  active: {
    type: Boolean,
    required: true,
    default: false
  },
  preferredLabel: {
    type: String,
    default: 'name',
    enum: ['name', 'username']
  },
  prefersHighContrast: {
    type: Boolean,
    default: false,
    required: true
  },
  hideNsfwImages: {
    type: Boolean,
    default: false
  },
  hideNsfwComments: {
    type: Boolean,
    default: false
  },
  birthday: {
    type: Date,
    required: true
  },
  dateTimeFormat: {
    type: String,
    enum: ['MM/dd/yyyy HH:mm a', 'MM/dd/yyyy hh:mm', 'dd/MM/yyyy HH:mm a', 'dd/MM/yyyy hh:mm'],
    default: 'MM/dd/yyyy HH:mm a'
  },
  dateCreated: Date,
  lastModified: Date
});

// // encrypt password using bcrypt conditionally. Only if the user is newly created.
// // Hash the plain text password before saving
userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

// create a new mongoose method for user login authenticationn
userSchema.statics.findByCredentials = async function (username, password) {
  const user = await this.findOne({ username });
  if (!user) {
    throw new Error('Unable to login. Wrong username!');
  }
  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    throw new Error('Unable to login. Wrong Password!');
  }
  return user;
};

module.exports = mongoose.model('User', userSchema);
