const fastify = require('fastify')({
  logger: true,
  disableRequestLogging: true
});
const dotenv = require('dotenv');
const swagger = require('./config/swagger');
const cors = require('fastify-cors');
const db = require('./config/mongoose');
const userRoutes = require('./routes/users');
const adminRoutes = require('./routes/admin');
const directoryRoutes = require('./routes/directories');
const imageRoutes = require('./routes/images');
const galleryRoutes = require('./routes/gallery');
const utilRoutes = require('./routes/utility');
const authPlugin = require('./config/authDecorators');
const helmet = require('fastify-helmet');
const path = require('path');

dotenv.config();
const uri = `mongodb://${process.env.CONNECT_DB}`;
const Port = process.env.PORT;

fastify.register(db, { uri });
fastify.register(require('fastify-swagger'), swagger.options);
fastify.register(authPlugin.verifyJWT).after((err) => {
  if (err) throw err;
});
fastify.register(cors, {
  origin: (origin, cb) => {
    console.log('origin', origin);
    cb(null, true); // TODO: come back to this and actually set CORS policy
    return;
  },
  methods: ['GET', 'PUT', 'POST', 'DELETE', 'HEAD', 'OPTIONS']
});
fastify.register(helmet, { contentSecurityPolicy: false });
fastify.register(
  require('fastify-multipart', {
    limits: {
      fieldNameSize: 100, // Max field name size in bytes
      fieldSize: 1000, // Max field value size in bytes
      fields: 10, // Max number of non-file fields
      fileSize: 20 * 1024 * 1024, // For multipart forms, the max file size in bytes
      files: 10, // Max number of file fields
      headerPairs: 2000 // Max number of header key=>value pairs
    }
  })
);
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, '/library'),
  prefix: '/library/',
  index: false,
  list: true,
  wildcard: false,
  setHeaders: (res) => {
    res.setHeader('Cross-Origin-Resource-Policy', 'cross-origin');
  }
});

fastify.register(userRoutes);
fastify.register(adminRoutes);
fastify.register(directoryRoutes);
fastify.register(imageRoutes);
fastify.register(galleryRoutes);
fastify.register(utilRoutes);

fastify.addHook('onRequest', (req, reply, done) => {
  req.log.info({ url: req.raw.url, id: req.id }, 'received request');
  done();
});

fastify.addHook('onResponse', (req, reply, done) => {
  req.log.info(
    {
      url: req.raw.url, // add url to response as well for simple correlating
      statusCode: reply.raw.statusCode,
      durationMs: reply.getResponseTime()
    },
    'request completed'
  );
  done();
});

const start = async () => {
  try {
    await fastify.listen(Port);
    fastify.swagger();
    fastify.log.info(`listening on ${fastify.server.address().port}`);
    fastify.log.info(`server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
