# QwikPik

Host your own photo gallery and control who has access!

## Description

QwikPik aims to be a lightweight, self-hosted photo/image gallery solution with a basic "roles" system in place for managing uploaded images and registered users.

I decided to write my own self-hosted solution after searching through a list of other self-hosted solutions. I felt like many other alternatives are missing some of the basic features I wanted or are full of features I don't want. I planned on hosting a solution on a $6 a month VPS, and having functionality like AI driven object or character recognition isn't condusive to keeping CPU usage and hosting costs low. I'm a huge fan of privacy so the idea of using big-tech solutions just isn't an option. I'd rather have a custom app where friend groups or families can share media that isn't on a big-tech platform or social network.

My basic goals with this app:

- create an app where a friend group or family can share pictures and possibly even videos
- make the media easily searchable and viewable
- allow users to do some basic customization to their experience
- have some users with elevated priveldges for the sake of moderation (just in case you have that family member or friend that uploads things you don't want your group exposed to)
- keep the app as efficient as possible so it can be hosted cheaply and easily and provide a snappy experience
- have some basic a11y considerations baked in for those with visual impairments
- make the app a PWA so people can have an app shortcut on their mobile device, the illusion of it being a native app, and some offline functionality if they currently are without a mobile data signal

## Completed Features

As of right now, it's too early to have anything fully functional.
You can currently:

- sign up
  - has validation and error messages if a field is incorrect
- log in
- log out
- change some user preferences

## Plans

#### Images

- upload images
- download images
- tag images
- search images by tags, name, upload date, and user
- organize images in collections/directories
- strip EXIF data for privacy reasons
- STRETCH: convert media to more efficient formats

#### Users

Users will have roles as follows:

- "basic"
  - can upload images
  - can view all images uploaded by themselves and other users
  - can edit tags on images they uploaded
  - can delete images they uploaded
- "supervisor"
  - can do everything in "basic" role
  - can edit tags for any image uploaded by any user
  - can delete images uploaded by other users
  - can deactivate users
  - can reset user passwords
  - can change basic users to supervisor
- "admin"
  - can do everything in "basic" and "supervisor" roles
  - The first account created will be the admin account so create your account immediately after deployment.
  - can delete other user accounts
  - can blacklist email addresses from creating accounts
  - can edit every aspect of a user

Users will have the following customization options:

- change between light and dark themes (by default, it will use you system theme setting)
- choose to use a high-contrast theme. This is meant for visually impaired users and there will be light and dark high contrast themes available.
- change preferrred date/time format
- change between "name" and "username" as their display name. The idea here is to provide the option of anonymity.
- change email address. Email addresses will really only be used by admins if they want to contact another user. I chose not to build in an email service since they can be costly and have their own set of privacy concerns.
- delete their uploaded images or account
- reset or change their password
- change their security question. Since there will not be an email service, a security question made sense for password resets.

## Getting started

TBD

## Installation

TBD

## License

GNU GPL

## Project status

Development of QwikPik has just begun. There is nothing even resembling functional about this project at the moment as I have merely stood up a repo, brought in some dependencies, and begun scaffolding out the project. This is being worked on an hour or two here and there, but I hope to have an MVP ready to ship in a few months.

After the core feature set is completed, I plan on adding a Docker file to simplify deployment for those who wish to host QwikPik on their own server/VPS.
