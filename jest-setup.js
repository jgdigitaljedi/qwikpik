// require('@testing-library/react/cleanup-after-each');
require('@testing-library/jest-dom/extend-expect');

// eslint-disable-next-line
const JSDOM = require('jsdom').JSDOM;
const dom = new JSDOM();
global.document = dom.window.document;
global.window = dom.window;
global.navigator = {
  userAgent: 'node.js'
};
global.HTMLElement = dom.window.HTMLElement;
global.document = dom.window.document;
window.document = dom.window.document;
// global.document.addEventListener = jest.fn();
// global.document.removeEventListener = jest.fn();
window.document.addEventListener = jest.fn();
window.document.removeEventListener = jest.fn();

Object.defineProperty(global.document, 'documentElement', {
  configurable: true,
  get() {
    return document.createElement('document');
  }
});

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // Deprecated
    removeListener: jest.fn(), // Deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn()
  }))
});

const localStorageMock = (function () {
  const store = {};
  return {
    getItem: function (key) {
      return store[key];
    },
    setItem: function (key, value) {
      store[key] = value.toString();
    },
    clear: function () {
      store = {};
    },
    removeItem: function (key) {
      delete store[key];
    }
  };
})();
Object.defineProperty(window, 'localStorage', { value: localStorageMock });
global.localStorage = localStorageMock;

global.window.document.createRange = function createRange() {
  return {
    setEnd: () => {},
    setStart: () => {},
    getBoundingClientRect: () => {
      return { right: 0 };
    },
    getClientRects: () => [],
    commonAncestorContainer: document.createElement('div')
  };
};
