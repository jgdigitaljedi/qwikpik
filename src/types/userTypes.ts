export type UserRoles = 'admin' | 'supervisor' | 'basic';

export type DateTimeFormatType =
  | 'MM/dd/yyyy HH:mm a'
  | 'MM/dd/yyyy hh:mm'
  | 'dd/MM/yyyy HH:mm a'
  | 'dd/MM/yyyy hh:mm';

export interface LoginTypes {
  username: string;
  password: string;
}

export interface CreateUserTypes {
  pwConfirm: string;
  name: string;
  email: string;
  secQuestion: string;
  secAnswer: string;
  birthday: Date;
}

export interface FullUserRequest extends LoginTypes, CreateUserTypes {}

export type UserPrefsPreferredDisplay = 'username' | 'name';

export interface BaseUserObj {
  username: string;
  email: string;
  role: UserRoles;
  active: boolean;
  dateCreated: string;
  lastModified: string;
  name: string;
  prefersHighContrast: boolean;
  dateTimeFormat: DateTimeFormatType;
  preferredLabel: UserPrefsPreferredDisplay;
  hideNsfwImages: boolean;
  hideNsfwComments: boolean;
  birthday: string;
  _id: string;
}

export interface BaseUserWithToken extends BaseUserObj {
  token: string;
}

export interface FullUserObj extends BaseUserWithToken {
  password?: string;
  securityQuestion?: string;
  securityAnswer?: string;
}
