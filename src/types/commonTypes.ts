import { BaseUserObj } from './userTypes';

export enum NavLinks {
  HOME = '/',
  GALLERY = '/gallery',
  USER = '/user',
  USER_IMAGES = '/user/images',
  NOTICE = '/notice',
  FORGOT_PASSWORD = '/forgotpassword',
  ADMIN_DASH = '/admindashboard',
  PREFS = '/user/preferences'
}

export interface FormValidation {
  key: string | null;
  error: string | null;
}

export interface StringKeyBoolVal {
  [key: string]: boolean;
}

export interface StringKeyBoolOrStringVal {
  [key: string]: string | boolean;
}

export interface StringKeyBoolOrStringOrDateVal {
  [key: string]: string | boolean | Date;
}

export interface GenericError {
  error: any;
  message: string;
}

export interface RouteCompProps {
  getFromTokenCb: () => Promise<BaseUserObj | GenericError>;
}

export type ToastVariant = 'info' | 'success' | 'warning' | 'error';
export type ToastPosition = 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right';

export interface NotificationProps {
  variant: ToastVariant;
  message: string;
  position?: ToastPosition;
  closable?: boolean;
}

export interface UserPropMessages {
  title: string;
  sub: string;
  error: string;
  success: string;
  label?: string;
  label2?: string;
  tooltip?: string;
}
