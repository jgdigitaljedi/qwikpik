export interface BaseImageNotSaved {
  path: string;
  pathDisplay: string;
  fileName: string;
  isNsfw: boolean;
  tags: string[];
  title: string;
}

export interface BaseImage extends BaseImageNotSaved {
  lastModified: string;
  uploadDate: string;
  uploadedBy: string;
  directory: string;
  _id: string;
}

export interface SelectedFilesDetails {
  file: File;
  details: BaseImageNotSaved;
  tempId?: number;
  hasDetails?: boolean; // used to determine if image details are complete before upload
}

export interface ImageUploadBody {
  fileInfo: SelectedFilesDetails;
  formData: FormData;
}

export interface SelectedCb {
  files: SelectedFilesDetails[];
  formData?: FormData;
}
