import { BaseUserObj } from './userTypes';

export interface AdminUserStats {
  total: number;
  active: number;
  inactive: number;
}

export interface EditedUser extends BaseUserObj {
  password?: string;
}
