export interface BaseDirectory {
  _id: string;
  name: string;
  path: string;
  children: string[];
  createdBy: string;
  createdDate: string;
  lastModified: string;
}
