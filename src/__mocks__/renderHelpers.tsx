import React from 'react';
import { render } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { ColorModeProvider } from '@theme-ui/color-modes';
import { Box, ThemeProvider } from 'theme-ui';
import { theme } from '../theme/theme';
import { AppContextProvider } from 'contexts/app.context';
import { createMemoryHistory } from 'history';
import { defaultUserContextValues, UserContextProvider } from 'contexts/user.context';
import { BaseUserWithToken } from 'types/userTypes';
import { NotificationContextProvider } from 'contexts/notification.context';
import { NotificationProps } from 'types/commonTypes';
import { DateTime } from 'luxon';

const memHistory = createMemoryHistory();

interface CreateWrapperProps {
  Comp: React.FC | JSX.Element;
  passedHistory?: any;
  width?: string;
  isChild?: boolean;
  loggedIn?: Partial<BaseUserWithToken>;
  notify?: NotificationProps;
}

export const mockUser: BaseUserWithToken = {
  username: 'tester1',
  name: 'Testy McTesterson',
  email: 'test@test.com',
  active: true,
  token: '8675309',
  dateCreated: '2022-01-14T12:18:06.963Z',
  lastModified: '2022-01-14T12:18:06.963Z',
  role: 'basic',
  prefersHighContrast: false,
  dateTimeFormat: 'MM/dd/yyyy HH:mm a',
  preferredLabel: 'name',
  birthday: DateTime.fromFormat('01/01/1980', 'MM/dd/yyyy').toISODate(),
  hideNsfwComments: false,
  hideNsfwImages: false,
  _id: '1234567890'
};

export const createWrapper = ({
  Comp,
  passedHistory,
  width,
  isChild,
  loggedIn,
  notify
}: CreateWrapperProps) => {
  const history = passedHistory || memHistory;

  return render(
    <ThemeProvider theme={theme}>
      <ColorModeProvider key="test-color">
        <Router key="test-router" location={history.location} navigator={history}>
          <AppContextProvider key="test-app-provider">
            <UserContextProvider
              key="test-user-provider"
              value={loggedIn ? { ...mockUser, ...loggedIn } : defaultUserContextValues}
            >
              <NotificationContextProvider value={notify}>
                {[
                  <Box sx={{ width: width || '100%' }} key="test-variable-width-box">
                    {/** @ts-ignore */}
                    {isChild ? Comp : <Comp key="test" />}
                  </Box>
                ]}
              </NotificationContextProvider>
            </UserContextProvider>
          </AppContextProvider>
        </Router>
      </ColorModeProvider>
    </ThemeProvider>
  );
};

export const createThemeOnlyWrapper = (children: JSX.Element[]) => {
  return render(
    <ThemeProvider theme={theme}>
      <ColorModeProvider key="test-color">{children}</ColorModeProvider>
    </ThemeProvider>
  );
};
