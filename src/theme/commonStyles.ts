import { ThemeUIStyleObject } from 'theme-ui';
import { theme } from 'theme/theme';

export const inputWrapperStyles: ThemeUIStyleObject = {
  display: 'flex',
  flexDirection: 'column',
  input: {
    mt: 3
  }
};

export const dashPrefsWrapper: ThemeUIStyleObject = {
  width: '100%',
  p: 4
};

export const loginCardStyles: ThemeUIStyleObject = {
  width: '100%'
};

export const userFieldStyle = {
  my: 3
};

export const mutedDividerStyle: ThemeUIStyleObject = {
  color: 'elevated'
};

export const dashPrefsInputFieldWrapperStyle: ThemeUIStyleObject = {
  ...inputWrapperStyles,
  maxWidth: '35rem'
};
