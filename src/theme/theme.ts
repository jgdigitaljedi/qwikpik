import { darken } from '@theme-ui/color';
import type { Theme } from 'theme-ui';

// export const breakpointValues = ['0px', '600px', '960px', '1280px', '1920px'];
export const breakpointValues = ['0px', '32em', '48em', '64em', '96em', '128em'];

export const newColors = {
  dark: '#191324',
  darkless: '#171C26',
  darker: '#0a080f',
  white: '#EFEEFB',
  steel: '#323546',
  slate: '#464a5e',
  muted: '#595b69',
  smoke: '#c6c8c9',
  snow: '#F5F0F6',
  red: '#8d081c',
  orange: '#E08E45',
  yellow: '#FFDC5E',
  green: '#1A6111',
  cyan: '#5AC5E0',
  blue: '#0A2D70',
  purple: '#654598'
};

// export const colors = {
//   dark: '#17171d',
//   darkless: '#252429',
//   darker: '#121217',
//   white: '#fff',
//   steel: '#273444',
//   slate: '#3C4858',
//   muted: '#8492A6',
//   smoke: '#E0E6ED',
//   snow: '#F9FAFC',
//   red: '#EC3750',
//   orange: '#FF8C37',
//   yellow: '#F1C40F',
//   green: '#33D6A6',
//   cyan: '#5BC0DE',
//   blue: '#338EDA',
//   purple: '#a633d6'
// };

export const theme: Theme = {
  config: {
    useColorSchemeMediaQuery: 'system'
  },
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
  fonts: {
    body: "'PT Sans', sans-serif;",
    heading: "'PT Sans Narrow', sans-serif;",
    monospace: "'PT Mono', monospace"
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700
  },
  colors: {
    dark: newColors.dark,
    darker: newColors.darker,
    darkless: newColors.darkless,
    steel: newColors.steel,
    slate: newColors.slate,
    gray: newColors.muted,
    smoke: newColors.smoke,
    snow: newColors.snow,
    white: newColors.white,
    red: newColors.red,
    orange: newColors.orange,
    yellow: newColors.yellow,
    green: newColors.green,
    cyan: newColors.cyan,
    blue: newColors.blue,
    purple: newColors.purple,
    background: newColors.snow,
    text: newColors.dark,
    primary: newColors.blue,
    secondary: newColors.purple,
    muted: newColors.muted,
    elevated: newColors.white,
    sunken: newColors.slate,
    accent: newColors.cyan,
    error: newColors.red,
    modes: {
      dark: {
        text: newColors.snow,
        background: newColors.dark,
        primary: newColors.cyan,
        secondary: newColors.orange,
        muted: newColors.darkless,
        elevated: newColors.darkless,
        sunken: newColors.darker,
        accent: newColors.cyan,
        error: newColors.red,
        blue: newColors.blue
      }
    }
  },
  images: {
    gallery: {
      width: '24rem',
      height: '24rem',
      objectFit: 'cover'
    },
    fill: {
      maxWidth: '100%',
      maxHeight: '100%',
      objectFit: 'contain'
    }
  },
  shadows: {
    text: '0 1px 2px rgba(0, 0, 0, 0.25), 0 2px 4px rgba(0, 0, 0, 0.125)',
    small: '0 1px 2px rgba(0, 0, 0, 0.0625), 0 2px 4px rgba(0, 0, 0, 0.0625)',
    card: '0 4px 8px rgba(0, 0, 0, 0.125)',
    elevated: '0 1px 2px rgba(0, 0, 0, 0.0625), 0 8px 12px rgba(0, 0, 0, 0.125)'
  },
  lineHeights: {
    limit: 0.875,
    title: 1,
    heading: 1.125,
    subheading: 1.25,
    caption: 1.375,
    body: 1.5
  },
  letterSpacings: {
    title: '-0.009em',
    headline: '0.009em'
  },
  radii: {
    small: 4,
    default: 8,
    extra: 12,
    ultra: 16,
    circle: 99999
  },
  buttons: {
    primary: {
      cursor: 'pointer',
      fontFamily: 'inherit',
      fontWeight: 'bold',
      borderRadius: 'circle',
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
      boxShadow: 'card',
      letterSpacing: 'headline',
      WebkitTapHighlightColor: 'transparent',
      transition: 'transform .125s ease-in-out, box-shadow .125s ease-in-out',
      color: 'darker',
      ':focus,:hover': {
        boxShadow: 'elevated',
        transform: 'scale(1.0625)'
      },
      ':disabled': {
        backgroundColor: 'muted',
        cursor: 'default',
        boxShadow: 'none',
        ':focus, :hover': {
          boxShadow: 'none',
          transform: 'none'
        }
      },
      svg: {
        ml: -1,
        mr: 2
      }
    },
    lg: {
      variant: 'buttons.primary',
      fontSize: 3,
      lineHeight: 'title',
      px: 4,
      py: 3
    },
    outline: {
      variant: 'buttons.primary',
      bg: 'transparent',
      color: 'primary',
      border: '2px solid currentColor'
    },
    outlineLg: {
      variant: 'buttons.primary',
      bg: 'transparent',
      color: 'primary',
      border: '2px solid currentColor',
      lineHeight: 'title',
      fontSize: 3,
      px: 4,
      py: 3
    },
    cta: {
      variant: 'buttons.primary',
      backgroundColor: 'blue',
      color: 'white',
      fontSize: 2
    },
    ctaLg: {
      variant: 'buttons.primary',
      lineHeight: 'title',
      backgroundColor: 'blue',
      color: 'white',
      fontSize: 3,
      px: 4,
      py: 3
    },
    linkButton: {
      variant: 'buttons.primary',
      bg: 'transparent',
      border: 'none',
      fontSize: 2,
      textDecoration: 'underline',
      boxShadow: 'none',
      color: 'text',
      transition: 'all .3s ease-in-out',
      ':focus,:hover': {
        boxShadow: 'none',
        transform: 'scale(1.1)'
      }
    },
    textButton: {
      variant: 'buttons.primary',
      bg: 'transparent',
      color: 'white'
    },
    pureText: {
      variant: 'buttons.primary',
      boxShadow: 'none',
      backgroundColor: 'transparent',
      color: 'white',
      p: 0
    }
    // icon: {
    //   fontSize: 3
    // }
  },
  forms: {
    input: {
      bg: 'elevated',
      color: 'text',
      fontFamily: 'inherit',
      borderRadius: 'base',
      border: `1px solid ${newColors.slate}`,
      '::-webkit-input-placeholder': {
        color: 'placeholder'
      },
      '::-moz-placeholder': {
        color: 'placeholder'
      },
      ':-ms-input-placeholder': {
        color: 'placeholder'
      },
      '&[type="search"]::-webkit-search-decoration': {
        display: 'none'
      }
    },
    textarea: {
      variant: 'forms.input'
    },
    select: {
      variant: 'forms.input'
    },
    label: {
      color: 'text',
      display: 'flex',
      textAlign: 'left',
      lineHeight: 'caption',
      fontSize: 2
    },
    labelHoriz: {
      color: 'text',
      display: 'flex',
      alignItems: 'center',
      textAlign: 'left',
      lineHeight: 'caption',
      fontSize: 2,
      svg: {
        color: 'muted'
      }
    },
    slider: {
      color: 'primary'
    },
    hidden: {
      position: 'absolute',
      height: '1px',
      width: '1px',
      overflow: 'hidden',
      clip: 'rect(1px, 1px, 1px, 1px)',
      whiteSpace: 'nowrap'
    }
  },
  layout: {
    container: {
      maxWidth: ['layout', null, 'layoutPlus'],
      width: '100%',
      mx: 'auto',
      px: 3
    },
    wide: {
      variant: 'layout.container',
      maxWidth: ['layout', null, 'wide']
    },
    copy: {
      variant: 'layout.container',
      maxWidth: ['copy', null, 'copyPlus']
    },
    narrow: {
      variant: 'layout.container',
      maxWidth: ['narrow', null, 'narrowPlus']
    }
  },
  cards: {
    primary: {
      bg: 'elevated',
      color: 'text',
      p: [3, 4],
      borderRadius: 'extra',
      boxShadow: 'card'
      // overflow: 'hidden'
    },
    sunken: {
      bg: 'sunken',
      p: [3, 4],
      borderRadius: 'extra'
    },
    interactive: {
      variant: 'cards.primary',
      textDecoration: 'none',
      WebkitTapHighlightColor: 'transparent',
      transition: 'transform .125s ease-in-out, box-shadow .125s ease-in-out',
      ':hover,:focus': {
        transform: 'scale(1.0625)',
        boxShadow: 'elevated'
      }
    },
    galleryCard: {
      variant: 'cards.interactive',
      p: 1,
      textAlign: 'center'
    },
    translucent: {
      backgroundColor: 'rgba(255, 255, 255, 0.98)',
      color: 'text',
      boxShadow: 'none',
      '@supports (-webkit-backdrop-filter: none) or (backdrop-filter: none)': {
        backgroundColor: 'rgba(255, 255, 255, 0.75)',
        backdropFilter: 'saturate(180%) blur(20px)',
        WebkitBackdropFilter: 'saturate(180%) blur(20px)'
      },
      '@media (prefers-reduced-transparency: reduce)': {
        backdropFilter: 'none',
        WebkitBackdropFilter: 'none'
      }
    },
    translucentDark: {
      backgroundColor: 'rgba(0, 0, 0, 0.875)',
      color: 'white',
      boxShadow: 'none',
      '@supports (-webkit-backdrop-filter: none) or (backdrop-filter: none)': {
        backgroundColor: 'rgba(0, 0, 0, 0.625)',
        backdropFilter: 'saturate(180%) blur(16px)',
        WebkitBackdropFilter: 'saturate(180%) blur(16px)'
      },
      '@media (prefers-reduced-transparency: reduce)': {
        backdropFilter: 'none',
        WebkitBackdropFilter: 'none'
      }
    }
  },
  text: {
    heading: {
      fontWeight: 'bold',
      lineHeight: 'heading',
      mt: 0,
      mb: 0
    },
    ultratitle: {
      fontSize: [5, 6, 7],
      lineHeight: 'limit',
      fontWeight: 'bold',
      letterSpacing: 'title'
    },
    title: {
      fontSize: [4, 5, 6],
      fontWeight: 'bold',
      letterSpacing: 'title',
      lineHeight: 'title'
    },
    subtitle: {
      mt: 3,
      fontSize: [2, 3],
      fontWeight: 'body',
      letterSpacing: 'headline',
      lineHeight: 'subheading'
    },
    headline: {
      variant: 'text.heading',
      letterSpacing: 'headline',
      lineHeight: 'heading',
      fontSize: 4,
      mt: 3,
      mb: 3
    },
    subheadline: {
      variant: 'text.heading',
      letterSpacing: 'headline',
      fontSize: 2,
      mt: 0,
      mb: 3
    },
    eyebrow: {
      color: 'muted',
      fontSize: [3, 4],
      fontWeight: 'heading',
      letterSpacing: 'headline',
      lineHeight: 'subheading',
      textTransform: 'uppercase',
      mt: 0,
      mb: 2
    },
    lead: {
      fontSize: [2, 3],
      my: [2, 3]
    },
    caption: {
      color: 'muted',
      fontWeight: 'medium',
      letterSpacing: 'headline',
      lineHeight: 'caption'
    },
    errorText: {
      color: 'red',
      fontWeight: 400
    }
  },
  alerts: {
    primary: {
      borderRadius: 'default',
      bg: 'cyan',
      color: 'darker',
      fontWeight: 'bold'
    },
    success: {
      variant: 'alerts.primary',
      bg: 'green'
    },
    warning: {
      variant: 'alerts.primary',
      bg: 'orange'
    },
    error: {
      variant: 'alerts.primary',
      bg: 'red'
    },
    info: {
      variant: 'alerts.primary',
      bg: 'purple'
    }
  },
  badges: {
    pill: {
      borderRadius: 'circle',
      px: 3,
      py: 1,
      fontSize: 1
    },
    outline: {
      variant: 'badges.pill',
      bg: 'transparent',
      border: '1px solid',
      borderColor: 'currentColor',
      fontWeight: 'body'
    }
  },
  styles: {
    root: {
      fontFamily: 'body',
      lineHeight: 'body',
      fontWeight: 'body',
      color: 'text',
      margin: 0,
      minHeight: '100vh',
      textRendering: 'optimizeLegibility',
      WebkitFontSmoothing: 'antialiased',
      MozOsxFontSmoothing: 'grayscale'
    },
    h1: {
      color: 'text',
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 5
    },
    h2: {
      color: 'text',
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 4
    },
    h3: {
      color: 'text',
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 3
    },
    h4: {
      color: 'text',
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 2
    },
    h5: {
      color: 'text',
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 1
    },
    h6: {
      color: 'text',
      fontFamily: 'heading',
      lineHeight: 'heading',
      fontWeight: 'heading',
      fontSize: 0
    },
    p: {
      color: 'text',
      fontFamily: 'body',
      fontWeight: 'body',
      lineHeight: 'body'
    },
    img: {
      maxWidth: '100%'
    },
    hr: {
      border: 0,
      borderBottom: '1px solid',
      borderColor: 'border'
    },
    a: {
      color: 'primary',
      textDecoration: 'underline',
      textUnderlinePosition: 'under',
      ':focus,:hover': {
        textDecorationStyle: 'wavy'
      }
    },
    pre: {
      fontFamily: 'monospace',
      overflowX: 'auto',
      code: {
        color: 'inherit'
      }
    },
    code: {
      fontFamily: 'monospace',
      fontSize: 'inherit'
    },
    table: {
      width: '100%',
      borderCollapse: 'separate',
      borderSpacing: 0
    },
    th: {
      textAlign: 'left',
      borderBottomStyle: 'solid'
    },
    td: {
      textAlign: 'left',
      borderBottomStyle: 'solid'
    }
  },
  links: {
    nav: {
      px: 2,
      py: 1,
      textTransform: 'uppercase',
      letterSpacing: '0.1em'
    }
  },
  breakpoints: breakpointValues
};
