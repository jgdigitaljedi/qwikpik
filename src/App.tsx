/** @jsxImportSource theme-ui */
import React from 'react';
import { ThemeProvider, Box } from 'theme-ui';
import { theme } from './theme/theme';
import { Routes, Route } from 'react-router-dom';
import Home from 'routes/Home';
import Gallery from 'routes/Gallery';
import NavbarComponent, { NavMenu } from './components/NavbarComponent';
import { AppContextProvider, useAppContext } from './contexts/app.context';
import { NavLinks } from 'types/commonTypes';
import ForgotPasswordComponent from 'routes/ForgotPassword';
import NoticesComponent from 'routes/Notices';
import { UserContextProvider } from 'contexts/user.context';
import UserPreferencesComponent from 'routes/UserPreferences';
import Wrapper from 'components/Wrapper';
import AdminDashboardComponent from 'routes/AdminDashboard';
import { NotificationContextProvider } from 'contexts/notification.context';
import ToastComponent from 'components/common/ToastComponent';

const App = () => {
  const { appContextValue } = useAppContext();

  return (
    <ThemeProvider theme={theme}>
      <AppContextProvider value={appContextValue}>
        <UserContextProvider>
          <NotificationContextProvider>
            <NavbarComponent />
            <NavMenu />
            <Box sx={{ p: 4 }}>
              <Wrapper>
                <Routes>
                  <Route path={NavLinks.HOME} element={<Home />} />
                  <Route path={NavLinks.GALLERY} element={<Gallery />} />
                  <Route path={NavLinks.FORGOT_PASSWORD} element={<ForgotPasswordComponent />} />
                  <Route path={NavLinks.NOTICE} element={<NoticesComponent />} />
                  <Route path={NavLinks.ADMIN_DASH} element={<AdminDashboardComponent />} />
                  <Route path={NavLinks.PREFS} element={<UserPreferencesComponent />} />
                </Routes>
              </Wrapper>
            </Box>
            <ToastComponent />
          </NotificationContextProvider>
        </UserContextProvider>
      </AppContextProvider>
    </ThemeProvider>
  );
};
export default App;
