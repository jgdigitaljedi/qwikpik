import NavbarThemeSwitch from 'components/NavbarComponent/NavbarThemeSwitch';
import { defaultUserContextValues, useUserContext } from 'contexts/user.context';
import React, { useEffect, useRef } from 'react';
import { BsArrowBarLeft, BsSliders } from 'react-icons/bs';
import { useNavigate } from 'react-router-dom';
import { Box, Button, Divider, useColorMode } from 'theme-ui';
import { NavLinks } from 'types/commonTypes';
import { ddItemsStyle, settingsWrapper } from './SettingsDropdown.styles';

interface SettingsDropdownProps {
  closeCb: () => void;
}

const SettingsDropdown: React.FC<SettingsDropdownProps> = ({ closeCb }) => {
  const { setUserContext } = useUserContext();
  const [colorMode] = useColorMode();
  const navigate = useNavigate();
  const ddRef = useRef(null);

  const logoutUser = (): void => {
    setUserContext(defaultUserContextValues);
    localStorage.removeItem('qpToken');
    closeCb();
    navigate(NavLinks.HOME);
  };

  const ddItemHandler = () => {
    closeCb();
    navigate(NavLinks.PREFS);
  };

  const handleClickOutside = (event: MouseEvent) => {
    // @ts-ignore
    if (ddRef.current && !ddRef.current.contains(event.target)) {
      closeCb();
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });

  return (
    <Box sx={settingsWrapper(colorMode)} ref={ddRef}>
      <Box sx={{ mb: 2, ml: 2 }}>
        <NavbarThemeSwitch />
      </Box>
      <Button variant="linkButton" sx={ddItemsStyle} onClick={ddItemHandler}>
        <BsSliders />
        Preferences
      </Button>
      <Divider />
      <Button variant="linkButton" onClick={logoutUser} sx={ddItemsStyle}>
        <BsArrowBarLeft />
        Log out
      </Button>
    </Box>
  );
};

export default SettingsDropdown;
