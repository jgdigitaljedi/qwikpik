import { ThemeUIStyleObject } from 'theme-ui';
import { theme } from 'theme/theme';

export function settingsWrapper(colorMode: string): ThemeUIStyleObject {
  return {
    position: 'absolute',
    top: '2rem',
    right: '-100%',
    zIndex: 10,
    bg: 'elevated',
    boxShadow: 'elevated',
    p: 2,
    pt: 3,
    width: 'auto',
    minWidth: '11rem',
    borderRadius: 'default',
    '&:before': {
      content: '""',
      borderLeft: '8px solid transparent',
      borderRight: '8px solid transparent',
      borderBottom: `8px solid ${
        colorMode === 'dark' ? theme?.colors?.modes?.dark.elevated : theme?.colors?.elevated
      }`,
      position: 'absolute',
      top: '-8px',
      right: '1.75rem'
    }
  };
}

export const ddItemsStyle: ThemeUIStyleObject = {
  textDecoration: 'none'
};
