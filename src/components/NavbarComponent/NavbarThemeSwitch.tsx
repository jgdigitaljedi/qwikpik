import React, { useCallback } from 'react';
import { Flex, Label, Switch, useColorMode } from 'theme-ui';

const NavbarThemeSwitch: React.FC = () => {
  const [colorMode, setColorMode] = useColorMode();

  const onColorModeChange = useCallback(() => {
    setColorMode(colorMode === 'default' ? 'dark' : 'default');
  }, [colorMode]);

  return (
    <Flex sx={{ justifyContent: 'flex-start' }}>
      <Label sx={{ width: 'auto', mr: '.8rem' }}>☀️</Label>
      <Switch
        onChange={onColorModeChange}
        label="🌙"
        aria-label="Color theme"
        checked={colorMode === 'dark'}
      />
    </Flex>
  );
};

export default NavbarThemeSwitch;
