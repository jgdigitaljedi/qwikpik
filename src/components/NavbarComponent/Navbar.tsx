import React, { useCallback, useEffect, useState } from 'react';
import { Flex, Heading, MenuButton, Box, useColorMode, NavLink } from 'theme-ui';
import { useAppContext } from '../../contexts/app.context';
import { breakpointValues } from '../../theme/theme';
import {
  menuButtonStyle,
  navInnerWrapperStyle,
  navLinkContainer,
  navLinksStyle,
  navLogoLinkStyle,
  navSettingsIconStyle
} from './Navbar.styles';
import NavLinksComponent from './NavLinks';
import { useBreakpointIndex } from '@theme-ui/match-media';
import { NavLinks } from 'types/commonTypes';
import { useLocation, useNavigate } from 'react-router-dom';
import { BsGearFill } from 'react-icons/bs';
import SettingsDropdown from 'components/SettingsDropdownComponent/SettingsDropdown';
import { useUserContext } from 'contexts/user.context';

const NavbarComponent: React.FC = () => {
  const { appContextValue, onAppContextChange } = useAppContext();
  const { userContextValue } = useUserContext();
  const [colorMode, setColorMode] = useColorMode();
  const [settingsOpen, setSettingsOpen] = useState(false);
  const bpIndex = useBreakpointIndex();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    const storedColorMode = localStorage.getItem('theme-ui-color-mode');
    if (storedColorMode && colorMode !== storedColorMode) {
      setColorMode(storedColorMode);
    }
  }, []);

  const menuButtonClick = () => {
    onAppContextChange({ ...appContextValue, navMenuOpen: !appContextValue.navMenuOpen });
  };

  const handleResize = useCallback(() => {
    const isMobileView = window.innerWidth <= bpIndex;
    onAppContextChange({
      ...appContextValue,
      navMenuOpen: isMobileView && appContextValue.navMenuOpen
    });
  }, [breakpointValues]);

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <Flex as="nav" sx={{ justifyContent: 'center', pt: 2 }}>
      <Flex sx={navInnerWrapperStyle}>
        <MenuButton aria-label="Toggle Menu" sx={menuButtonStyle} onClick={menuButtonClick} />
        <NavLink
          sx={{ ...navLinksStyle(location.pathname as NavLinks, 'HOME'), ...navLogoLinkStyle }}
          onClick={() => navigate(NavLinks.HOME)}
        >
          <Heading as="h1">QwikPik</Heading>
        </NavLink>
        <Flex sx={navLinkContainer}>
          <NavLinksComponent />
        </Flex>
        {!!userContextValue.username && (
          <Box sx={navSettingsIconStyle(settingsOpen, colorMode)}>
            <BsGearFill onClick={() => setSettingsOpen(!settingsOpen)} />
            {settingsOpen && <SettingsDropdown closeCb={() => setSettingsOpen(false)} />}
          </Box>
        )}
      </Flex>
    </Flex>
  );
};

export default NavbarComponent;
