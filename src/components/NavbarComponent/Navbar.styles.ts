import { ThemeUICSSObject, ThemeUIStyleObject } from 'theme-ui';
import { fadeIn, fadeOut } from 'theme/animations';
import { theme } from 'theme/theme';
import { NavLinks } from 'types/commonTypes';

export const navInnerWrapperStyle = {
  maxWidth: '900px', // TODO: set breakpoints in theme and use one here
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',
  py: 2,
  px: 4
};

export const navLinkContainer = {
  justifyContent: 'space-around',
  display: ['none', 'none', 'none', 'flex', 'flex'],
  a: { mx: 2 }
};

export const menuButtonStyle = {
  display: ['block', 'block', 'block', 'none', 'none'],
  ml: 2
};

export const navMenuWrapperStyle = (isOpen: boolean): ThemeUICSSObject => {
  return {
    height: '100%',
    width: '70%',
    maxWidth: '25rem',
    position: 'absolute',
    zIndex: 3,
    top: 0,
    left: 0,
    backgroundColor: 'muted',
    overflowX: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    transform: isOpen ? 'scaleX(1)' : 'scaleX(0)',
    transition: 'transform 0.3s ease-in-out',
    transformOrigin: 'left',
    boxShadow: isOpen ? 'card' : 'none'
  };
};

export const navMenuBackdropStyle = (isOpen: boolean): ThemeUICSSObject => {
  return {
    display: isOpen ? 'block' : 'none',
    position: 'fixed',
    zIndex: 2,
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    overflow: 'auto',
    backgroundColor: 'rgba(0,0,0,0.4)',
    animation: isOpen ? `${fadeIn} 0.3s ease-in` : `${fadeOut} 0.3s ease-in`
  };
};

export const navMenuTopBarStyle = (isOpen: boolean): ThemeUICSSObject => ({
  width: isOpen ? '100%' : 0,
  px: 4,
  py: 2
});

export function navLinksStyle(
  currentRoute: NavLinks,
  item: keyof typeof NavLinks
): ThemeUIStyleObject {
  return {
    cursor: 'pointer',
    color: currentRoute === NavLinks[item] ? 'secondary' : 'text',
    transition: 'all .3s ease-in-out',
    '&:hover': {
      transform: 'scale(1.1)'
    }
  };
}

export const navLogoLinkStyle: ThemeUIStyleObject = {
  textTransform: 'none',
  cursor: 'pointer'
};

function settingsClosedFillColor(colorMode: string) {
  if (colorMode === 'dark') {
    return theme?.colors?.modes?.dark?.text;
  }
  return theme?.colors?.text;
}

export function navSettingsIconStyle(isOpen: boolean, colorMode: string): ThemeUIStyleObject {
  return {
    position: 'relative',
    svg: {
      fill: isOpen ? theme.colors?.accent : settingsClosedFillColor(colorMode),
      width: '1.5rem',
      height: '1.5rem'
    }
  };
}
