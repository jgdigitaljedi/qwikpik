import React from 'react';
import { NavLink } from 'theme-ui';
import { NavLinks } from '../../types/commonTypes';
import { useLocation, useNavigate } from 'react-router-dom';
import { useAppContext } from 'contexts/app.context';
import { navLinksStyle } from './Navbar.styles';
import { useUserContext } from 'contexts/user.context';

const NavLinksComponent = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { appContextValue, onAppContextChange } = useAppContext();
  const { userContextValue } = useUserContext();

  const closeAndNavigate = (where: NavLinks): void => {
    if (appContextValue.navMenuOpen) {
      onAppContextChange({ ...appContextValue, navMenuOpen: false });
    }
    navigate(where);
  };

  return (
    <React.Fragment>
      {!!userContextValue.username && (
        <NavLink
          onClick={() => closeAndNavigate(NavLinks.GALLERY)}
          p={2}
          sx={navLinksStyle(location.pathname as NavLinks, 'GALLERY')}
        >
          Gallery
        </NavLink>
      )}
      {!!userContextValue.username && userContextValue?.role !== 'basic' && (
        <NavLink
          onClick={() => closeAndNavigate(NavLinks.ADMIN_DASH)}
          p={2}
          sx={navLinksStyle(location.pathname as NavLinks, 'ADMIN_DASH')}
        >
          Admin Dash
        </NavLink>
      )}
      {!!userContextValue.username && (
        <NavLink
          onClick={() => closeAndNavigate(NavLinks.USER)}
          p={2}
          sx={navLinksStyle(location.pathname as NavLinks, 'USER')}
        >
          Profile
        </NavLink>
      )}
    </React.Fragment>
  );
};

export default NavLinksComponent;
