import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Navbar from '.';
import { Box } from 'theme-ui';

export default {
  title: 'Global/Navbar',
  component: Navbar
} as ComponentMeta<typeof Navbar>;

const Template: ComponentStory<typeof Navbar> = (args) => {
  return (
    <Box sx={{ width: '100%' }}>
      <Navbar {...args} />
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {};
