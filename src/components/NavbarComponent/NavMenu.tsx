import React, { useEffect, useRef } from 'react';
import { Box, MenuButton } from 'theme-ui';
import { useAppContext } from '../../contexts/app.context';
import {
  navMenuBackdropStyle,
  navMenuWrapperStyle,
  menuButtonStyle,
  navMenuTopBarStyle
} from './Navbar.styles';
import NavLinksComponent from './NavLinks';

export const NavMenu: React.FC = () => {
  const { appContextValue, onAppContextChange } = useAppContext();
  const containerRef = useRef<HTMLDivElement>(null);

  const menuButtonClick = () => {
    onAppContextChange({ ...appContextValue, navMenuOpen: !appContextValue.navMenuOpen });
  };

  const handleClick = (e: MouseEvent) => {
    if (containerRef?.current?.contains(e.target as Node)) {
      return;
    }
    onAppContextChange({ ...appContextValue, navMenuOpen: false });
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClick);
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  return (
    <>
      <Box
        sx={navMenuBackdropStyle(appContextValue.navMenuOpen)}
        data-testid="nav-menu-container"
      ></Box>
      <Box sx={navMenuWrapperStyle(appContextValue.navMenuOpen)} ref={containerRef}>
        <Box sx={navMenuTopBarStyle(appContextValue.navMenuOpen)}>
          <MenuButton aria-label="Toggle Menu" sx={menuButtonStyle} onClick={menuButtonClick} />
        </Box>
        <NavLinksComponent />
      </Box>
    </>
  );
};
