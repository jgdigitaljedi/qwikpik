import React, { useMemo, useState } from 'react';
import { Flex, Grid } from 'theme-ui';
import { BaseImage } from 'types/imageTypes';
import ImageCard from 'components/ImageCard';
import { mainGalleryGridStyle } from './ImageGallery.styles';
import DirectoryCard from 'components/DirectoryCard';
import { BaseDirectory } from 'types/directoryTypes';
import ModalComponent from 'components/common/ModalComponent';
import AddDirectoryDialog from 'components/AddDirectoryDialog';
import useModal from 'hooks/useModal';
import { addDirectory } from 'services/directories.service';
import { useNotificationContext } from 'contexts/notification.context';
import AddImageDialog from 'components/AddImageDialog';

interface ImageGalleryProps {
  images: BaseImage[];
  directory: BaseDirectory | null;
  subdirCb: (dir: string) => void;
}
type OpenDialog = null | 'image' | 'directory';

/** plans
 * - infinite scroll
 *   - gonna need to implement DOM functionality and scroll position tracking
 *   - gonna need a cb passed in from Gallery to fire back out when more images needed
 *
 */

const ImageGallery: React.FC<ImageGalleryProps> = ({ images, directory, subdirCb }) => {
  const { toggle, isShowing } = useModal();
  const { setNotificationContext } = useNotificationContext();
  const [openDialog, setOpenDialog] = useState<OpenDialog>(null);

  const currentPath = useMemo(() => {
    const dirPathParsed = directory ? directory.path.replace('root,', '/').replace(/,/g, '/') : '/';
    return `${dirPathParsed}${directory?._id || ''}`;
  }, [directory]);

  console.log('currentPath', currentPath);

  const onAddDirClose = () => {
    setOpenDialog(null);
    toggle();
  };

  const addItem = (item: string): void => {
    setOpenDialog(item as OpenDialog);
    toggle();
  };

  const addNewDirectory = async (name: string) => {
    if (directory) {
      const newDirAdded = await addDirectory(name, directory);
      if (newDirAdded.error) {
        setNotificationContext({
          message: `There was a problem adding ${name}. Please refresh and try again.`,
          variant: 'error',
          closable: true
        });
      } else {
        subdirCb(directory._id);
        setNotificationContext({
          message: `The directory ${name} was successfully created.`,
          variant: 'success',
          closable: true
        });
      }
      toggle();
    }
  };

  return (
    <>
      <Grid gap={3} width={['100%']} sx={mainGalleryGridStyle}>
        {directory &&
          directory.children.map((dir: string) => (
            <DirectoryCard directory={dir} clickCb={subdirCb} key={dir} />
          ))}
        {images &&
          images.map((image: BaseImage) => (
            <ImageCard image={image} key={image._id} currentPath={currentPath} />
          ))}
        <DirectoryCard directory={'+'} clickCb={addItem} key="add-item-8675309" />
      </Grid>
      <ModalComponent
        size="sm"
        title="Create new directory"
        isShowing={isShowing && openDialog === 'directory'}
        hide={onAddDirClose}
        modalBody={
          <AddDirectoryDialog
            currentPath={currentPath}
            hide={onAddDirClose}
            addDirCb={addNewDirectory}
          />
        }
      />
      <ModalComponent
        size="lg"
        title="Upload images"
        isShowing={isShowing && openDialog === 'image'}
        hide={onAddDirClose}
        modalBody={
          <AddImageDialog
            path={directory?.path || ''}
            currentPath={currentPath}
            modalClosed={toggle}
          />
        }
      />
    </>
  );
};

export default ImageGallery;
