import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import ImageGallery from '.';
import { mockDirectory, placeholderImage } from 'stories/storybook.mocks';
import { Box } from 'theme-ui';

export default {
  title: 'Gallery/ImageGallery',
  component: ImageGallery
} as ComponentMeta<typeof ImageGallery>;

const Template: ComponentStory<typeof ImageGallery> = (args) => {
  return (
    <Box sx={{ width: '100%' }}>
      <ImageGallery {...args} />
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {
  images: [placeholderImage, placeholderImage, placeholderImage],
  directory: mockDirectory,
  subdirCb: () => {}
};
