import { ThemeUIStyleObject } from 'theme-ui';

export const mainGalleryWrapperStyle: ThemeUIStyleObject = {
  justifyContent: 'center',
  width: '100%'
};

export const mainGalleryGridStyle: ThemeUIStyleObject = {
  gridTemplateColumns: 'repeat(auto-fill, 24rem)',
  justifyContent: 'center',
  margin: 'auto',
  '@media screen and (max-width: 32rem)': {
    gridTemplateColumns: 'repeat(auto-fill, 100%)'
  }
};
