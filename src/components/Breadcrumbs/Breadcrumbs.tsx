import React from 'react';
import { Box, Button, Flex, Text } from 'theme-ui';
import { BaseDirectory } from 'types/directoryTypes';

interface BreadcrumbsProps {
  currentDir: BaseDirectory | null;
  onBcClick: (dir: string) => void;
}

const Breadcrumbs: React.FC<BreadcrumbsProps> = ({ currentDir, onBcClick }) => {
  const pathFormatted = () => {
    if (!currentDir || currentDir._id === 'root') {
      return ['home'];
    }
    return [
      'home',
      ...currentDir.path.split(',').filter((item: string) => item && item !== 'root'),
      currentDir._id
    ];
  };

  const fullPath = pathFormatted();

  return (
    <Flex sx={{ flexWrap: 'wrap', button: { px: 2 }, fontSize: 3, mt: 3, mb: 4 }}>
      {fullPath.map((item: string, index: number) => {
        return (
          <React.Fragment key={`${item}`}>
            <Button variant="pureText" onClick={() => onBcClick(item)}>
              {item}
            </Button>
            {index < fullPath.length - 1 && <Box>/</Box>}
          </React.Fragment>
        );
      })}
    </Flex>
  );
};

export default Breadcrumbs;
