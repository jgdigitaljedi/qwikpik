import React, { ChangeEvent, useMemo, useState } from 'react';
import { BsFillXCircleFill, BsFolderPlus } from 'react-icons/bs';
import { Box, Button, Divider, Flex, Input, Label, Text } from 'theme-ui';
import { BaseDirectory } from 'types/directoryTypes';
import { alphaNumeric } from 'utils/utils';

interface AddDirectoryDialogProps {
  currentPath: string;
  hide: () => void;
  addDirCb: (name: string) => void;
}

const AddDirectoryDialog: React.FC<AddDirectoryDialogProps> = ({ currentPath, hide, addDirCb }) => {
  const [newDirName, setNewDirName] = useState<string>('');
  const [invalidName, setInvalidName] = useState(false);
  const fullPath = useMemo(() => {
    const ndn = newDirName?.length ? newDirName : '<your new directory>';
    if (currentPath === 'root') {
      return `/root/${ndn}`;
    }
    return `/root${currentPath}/${ndn}`;
  }, [currentPath, newDirName]);

  const onAddDir = () => {
    const isValid = alphaNumeric(newDirName); // TODO: validate that it only has letters and  numbers
    if (isValid) {
      setInvalidName(false);
      addDirCb(newDirName);
    } else {
      setInvalidName(true);
    }
  };

  return (
    <Box>
      <Text as="p" sx={{ mb: 3 }}>
        Your new directory will be added as a sub-directory of the currently viewed directory.
      </Text>
      <Text
        as="div"
        sx={{ fontStyle: 'italic', width: '100%', mb: 2 }}
      >{`The new directory will have the path:`}</Text>
      <Text sx={{ backgroundColor: 'sunken', fontWeight: 'bold', fontSize: 2, p: 1 }}>
        {fullPath}
      </Text>
      <Label htmlFor="new-dir-name" sx={{ mt: 3 }}>
        New directory name
      </Label>
      <Input
        name="new-dir-name"
        value={newDirName}
        onChange={(event) => setNewDirName(event.target.value)}
        sx={{ mb: 3 }}
      />
      {invalidName && (
        <Text variant="errorText">Your directory name can only have letters and numbers.</Text>
      )}
      <Divider />
      <Flex sx={{ justifyContent: 'flex-end', pt: 2 }}>
        <Button sx={{ mr: 2 }} variant="outline" onClick={hide}>
          <BsFillXCircleFill />
          Cancel
        </Button>
        <Button variant="primary" onClick={onAddDir}>
          <BsFolderPlus />
          Add directory
        </Button>
      </Flex>
    </Box>
  );
};

export default AddDirectoryDialog;
