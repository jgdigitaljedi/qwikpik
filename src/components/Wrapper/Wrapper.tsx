import { defaultUserContextValues, useUserContext } from 'contexts/user.context';
import React, { useCallback, useEffect } from 'react';
import { RouteProps, useLocation, useNavigate } from 'react-router-dom';
import { getUserObjFromToken } from 'services/user.service';
import { GenericError, NavLinks } from 'types/commonTypes';
import { BaseUserWithToken } from 'types/userTypes';

const Wrapper: React.FC<RouteProps> = ({ children }) => {
  const { setUserContext } = useUserContext();
  const location = useLocation();
  const navigate = useNavigate();

  const getUserFromToken = useCallback(async (): Promise<void> => {
    // TODO: set context values so userToken call is made less? I feel like something ain't right yet.
    if (!localStorage.getItem('qpToken')) {
      setUserContext(defaultUserContextValues);
      if (!location || location.pathname !== NavLinks.HOME) {
        navigate(NavLinks.HOME);
      }
    } else {
      const user = await getUserObjFromToken();
      if ((user as GenericError).error) {
        console.log('user error', user);
        localStorage.removeItem('qpToken');
        navigate(NavLinks.HOME);
      } else {
        setUserContext(user as BaseUserWithToken);
      }
    }
  }, [setUserContext, navigate]);

  useEffect(() => {
    getUserFromToken();
  }, [location]);

  return <>{children}</>;
};

export default Wrapper;
