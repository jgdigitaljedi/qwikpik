import React from 'react';
import { Box, Card, Heading, Text } from 'theme-ui';
import { noticeCardWrapper, noticeHeading } from './NoticeStyles';
import { NoticeProps } from './NoticeTypes';

export const UserNoticeComponent: React.FC<NoticeProps> = ({ name }) => {
  return (
    <Card variant="primary" sx={noticeCardWrapper}>
      <Heading
        as="h2"
        sx={noticeHeading}
      >{`Your request for an account has been submitted.`}</Heading>
      <Text variant="subtitle" sx={{ pb: 6 }}>
        QwikPik was built to be a free solution for private groups of people to be able to easily
        share photos and videos.
      </Text>
      <Box as="p">
        By default, all new account requests have to be approved by an admin. This gives the admins
        control of who can post and see content to prevent strangers from seeing you content or spam
        bots from making posts. Once you account is approved, you'll be able to login, view, and
        post content.
      </Box>
    </Card>
  );
};
