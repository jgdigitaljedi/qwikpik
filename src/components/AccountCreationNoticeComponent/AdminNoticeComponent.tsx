import React from 'react';
import { Box, Card, Heading, Text } from 'theme-ui';
import { noticeCardWrapper, noticeHeading } from './NoticeStyles';
import { NoticeProps } from './NoticeTypes';

export const AdminNoticeComponent: React.FC<NoticeProps> = ({ name }) => {
  return (
    <Card variant="primary" sx={noticeCardWrapper}>
      <Heading as="h2" sx={noticeHeading}>{`Welcome, ${name}!`}</Heading>
      <Text variant="subtitle" sx={{ pb: 6 }}>
        Since you have an account with the "admin" role, you can do the following:
      </Text>
      <Box as="ul">
        <li>
          Approve/deny new requests for user accounts. In fact, only an admin can do this, so make
          sure to check the admin dashboard to give new users access.
        </li>
        <li>Delete user accounts.</li>
        <li>Delete ANY image (users with "basic" role can only delete images they upload).</li>
        <li>Tag/edit any image.</li>
      </Box>
    </Card>
  );
};
