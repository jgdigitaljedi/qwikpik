import { ThemeUIStyleObject } from 'theme-ui';

export const noticeCardWrapper: ThemeUIStyleObject = {
  maxWidth: '50rem',
  width: '100%'
};

export const noticeHeading: ThemeUIStyleObject = {
  color: 'secondary',
  mb: 2
};
