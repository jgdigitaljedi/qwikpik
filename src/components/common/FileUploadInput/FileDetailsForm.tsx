import React, { ChangeEvent, useEffect, useState } from 'react';
import { Box, Flex, Heading, Input, Label, Switch, Text } from 'theme-ui';
import { SelectedFilesDetails } from 'types/imageTypes';
import ChipsInput from '../ChipsInput';
import {
  fileMetaDataKeyStyle,
  formHeadingBottomMargin,
  formInputWrapperStyle,
  imageDetailsFormWrapper,
  fileMetaFieldStyle
} from './FileUpload.style';

interface FileDetailsFormProps {
  selected: SelectedFilesDetails;
  onFieldChange: (details: SelectedFilesDetails) => void;
}

export const FileDetailsForm: React.FC<FileDetailsFormProps> = ({ selected, onFieldChange }) => {
  const [currentTags, setCurrentTags] = useState<string[]>([]);
  const [currentTitle, setCurrentTitle] = useState<string>('');

  const onTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    console.log('newValue', newValue);
    setCurrentTitle(newValue);
    const newDetails = {
      ...selected.details,
      title: newValue || ''
    };
    onFieldChange({ file: selected.file, details: newDetails, tempId: selected.tempId });
  };

  const onChipsChange = (items: string[] | undefined) => {
    setCurrentTags(items || []);
    const newDetails = { ...selected.details, tags: items || [], title: currentTitle };
    onFieldChange({ file: selected.file, details: newDetails, tempId: selected.tempId });
  };

  const onNsfwChanged = () => {
    const newDetails = { ...selected.details, isNsfw: !selected.details.isNsfw };
    onFieldChange({ file: selected.file, details: newDetails, tempId: selected.tempId });
  };

  useEffect(() => {
    setCurrentTags(selected.details.tags);
    setCurrentTitle(selected.details.title || '');
  }, [selected]);

  // TODO: add elpsis for metadata fileName as it could be long (ex. adsasdasdasd....jpg)
  // TODO: wrap path if it gets very long in metadata

  return (
    <Flex sx={imageDetailsFormWrapper}>
      <Box sx={formInputWrapperStyle}>
        <Heading as="h4">File metadata</Heading>
        <Box>
          <Flex sx={fileMetaFieldStyle}>
            <Text sx={fileMetaDataKeyStyle}>File name</Text>
            <Text>{selected.details.fileName}</Text>
          </Flex>
          <Flex sx={fileMetaFieldStyle}>
            <Text sx={fileMetaDataKeyStyle}>Upload path</Text>
            <Text>{selected.details.pathDisplay}</Text>
          </Flex>
        </Box>
      </Box>
      <Box sx={formInputWrapperStyle}>
        <Heading as="h4" sx={formHeadingBottomMargin}>
          Image title *
        </Heading>
        <Input
          name="title"
          placeholder="Enter a title for this image"
          value={currentTitle}
          onChange={onTitleChange}
        />
      </Box>
      <Box sx={formInputWrapperStyle}>
        <Heading as="h4">Image tags *</Heading>
        <ChipsInput
          items={currentTags}
          placeholderText="Tags to make image searchable"
          onChipsChange={onChipsChange}
        />
      </Box>
      <Box sx={formInputWrapperStyle}>
        <Heading as="h4" sx={formHeadingBottomMargin}>
          Is image NSFW?
        </Heading>
        <Switch
          label="Yes"
          name="isNsfw"
          onChange={onNsfwChanged}
          checked={selected.details.isNsfw}
        />
      </Box>
    </Flex>
  );
};
