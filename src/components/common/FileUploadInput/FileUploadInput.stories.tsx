import React, { useState } from 'react';
import FileUploadInput from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Box } from 'theme-ui';

export default {
  title: 'Common/FileUploadInput',
  component: FileUploadInput
} as ComponentMeta<typeof FileUploadInput>;

const Template: ComponentStory<typeof FileUploadInput> = (args) => {
  const onSelectedCb = (val: FormData) => {
    console.log('upload value', val);
  };

  return (
    <Box sx={{ width: '40rem' }}>
      <FileUploadInput {...args} onSelectedCb={onSelectedCb} />
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {
  path: 'root,'
};
