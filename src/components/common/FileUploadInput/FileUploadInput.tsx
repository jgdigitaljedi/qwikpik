import React, { useState } from 'react';
import { Box, Divider, Flex, Input, Text } from 'theme-ui';
import { BaseImageNotSaved, SelectedFilesDetails } from 'types/imageTypes';
import { FileDetailsForm } from './FileDetailsForm';
import {
  fileInputStyle,
  fileWrapperStyle,
  imageUploadDetailsFormStyle,
  previewWrapperStyle
} from './FileUpload.style';
import { findIndex as _findIndex } from 'lodash';
import ImageEdit from '../ImageEdit';

export type FileEventTarget = EventTarget & { files: FileList };

interface SelectedCb {
  files: SelectedFilesDetails[];
  formData?: FormData;
}

interface UploaderProps {
  path: string;
  pathDisplay: string;
  onSelectedCb: (data: SelectedCb) => void;
}

// TODO: make new image component with icon overlays to indicate if details filled out

const FileUploadInput: React.FC<UploaderProps> = ({ path, pathDisplay, onSelectedCb }) => {
  const [selectedFiles, setSelectedFiles] = useState<File[]>();
  const [selectedFilesDetails, setSelectedFilesDetails] = useState<SelectedFilesDetails[] | null>(
    null
  );
  const [addingDetails, setAddingDetails] = useState<SelectedFilesDetails | null>();
  const [fileFormData, setFileFormData] = useState<FormData>();

  const getUnsetImageDetails = (file: File): BaseImageNotSaved => {
    console.log('file', file);
    return {
      path,
      pathDisplay,
      fileName: file.name,
      isNsfw: false,
      tags: [],
      title: ''
    };
  };

  const fileHasDetails = (details: BaseImageNotSaved) => {
    return !!details?.title && !!details.tags.length;
  };

  const changeSelectedImage = (file: SelectedFilesDetails) => {
    console.log('selectedFilesDetails', selectedFilesDetails);
    const newSelection = selectedFilesDetails?.filter((image: SelectedFilesDetails) => {
      return file.file.name === image.file.name;
    })[0];
    setAddingDetails(newSelection);
  };

  const onFieldChange = ({ file, details, tempId, hasDetails }: SelectedFilesDetails) => {
    const imageHasDetails = { file, details, tempId, hasDetails: fileHasDetails(details) };
    setAddingDetails(imageHasDetails);
    const currentIndex = _findIndex(selectedFilesDetails, { tempId });
    if (currentIndex > -1 && selectedFilesDetails?.length) {
      const newFiles = [...selectedFilesDetails];
      newFiles[currentIndex] = imageHasDetails;
      console.log('newFiles', newFiles);
      setSelectedFilesDetails(newFiles as SelectedFilesDetails[]);
      onSelectedCb({ files: newFiles as SelectedFilesDetails[], formData: fileFormData });
    }
  };

  const onFileSelect = (event: React.ChangeEvent<HTMLInputElement>): void => {
    if (!!event?.target?.files?.length) {
      const files: FileList = event.target.files;
      const formData = new FormData();
      console.log('formData', formData);
      const fileDetails: SelectedFilesDetails[] = [];
      for (let i = 0; i < files.length; i++) {
        if (files[i].type.match('image/*')) {
          console.log('item', files[i]);
          formData.append(`images`, files[i]);
          fileDetails.push({
            file: files[i],
            details: getUnsetImageDetails(files[i]),
            tempId: i,
            hasDetails: false
          });
        }
      }
      console.log('formData images', formData.getAll('images'));
      setFileFormData(formData);
      // const selected = Array.from(files)
      //   .map((file, index) => {
      //     console.log('file', file);
      //     if (file.type.match('image/*')) {
      //       // formData.append('images[]', file, file.name);
      //       fileDetails.push({
      //         file,
      //         details: getUnsetImageDetails(file),
      //         tempId: index,
      //         hasDetails: false
      //       });
      //       return file;
      //     }
      //     return null;
      //   })
      //   .filter((item: File | null) => !!item);
      // setSelectedFiles(selected);
      setSelectedFilesDetails(fileDetails);
      setAddingDetails(fileDetails[0]);
      console.log('adding', fileDetails[0]);
      // onSelectedCb(formData);
    } else {
      // TODO: handle
    }
  };

  const removeImage = (image: SelectedFilesDetails) => {
    // TODO: remove image from array
    console.log('remove image', image);
    if (selectedFilesDetails?.length) {
      const withImageDeleted = [...selectedFilesDetails].filter(
        (file: SelectedFilesDetails) => file.tempId !== image.tempId
      );
      setSelectedFilesDetails(withImageDeleted);
      if (addingDetails?.tempId === image.tempId && !!withImageDeleted.length) {
        setAddingDetails(withImageDeleted[0]);
      } else if (addingDetails?.tempId === image.tempId) {
        setAddingDetails(null);
      }
    }
  };

  return (
    <Box sx={fileWrapperStyle}>
      <Input
        type="file"
        name="file"
        onChange={(event) => onFileSelect(event)}
        multiple
        sx={fileInputStyle}
      />
      {!!selectedFilesDetails?.length && (
        <Flex sx={previewWrapperStyle}>
          {selectedFilesDetails?.map((file: SelectedFilesDetails) => {
            console.log('preview', file);
            return (
              <ImageEdit
                image={file}
                selected={addingDetails?.details?.fileName === file.file.name}
                changeSelectedImage={changeSelectedImage}
                onRemoveClick={(image: SelectedFilesDetails) => removeImage(image)}
                key={`image-${file.tempId}`}
              />
            );
          })}
        </Flex>
      )}
      {!!selectedFilesDetails?.length && (
        <Flex sx={imageUploadDetailsFormStyle}>
          <Text variant="headline" sx={{ mb: 0 }}>
            Click on each image preview above to add details
          </Text>
          <Text variant="subheadline">
            Every image must have the details filled out before any of them can be uploaded.
          </Text>
          <Divider />
          {addingDetails && (
            <FileDetailsForm selected={addingDetails} onFieldChange={onFieldChange} />
          )}
        </Flex>
      )}
    </Box>
  );
};

export default FileUploadInput;
