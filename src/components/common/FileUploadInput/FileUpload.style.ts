import { Theme, ThemeUIStyleObject } from 'theme-ui';

export const fileWrapperStyle: ThemeUIStyleObject = {
  width: '100%'
};

export const fileInputStyle: ThemeUIStyleObject = {
  width: '100%',
  '&::-webkit-file-upload-button': {
    visibility: 'hidden'
  },
  '&::before': {
    content: '"Select images"',
    display: 'inline-block',
    bg: 'blue',
    color: 'white',
    borderRadius: 'default',
    p: 2,
    outline: 'none',
    whiteSpace: 'nowrap',
    WebkitUserSelect: 'none',
    cursor: 'pointer',
    fontWeight: 700
  },
  '&:hover::before': {
    bg: 'primary',
    color: 'darker'
  },
  '&:active::before': {
    bg: 'muted'
  }
};

export const previewWrapperStyle: ThemeUIStyleObject = {
  flexWrap: 'wrap',
  mt: 3
};

export const previewImageStyle: ThemeUIStyleObject = {
  maxWidth: '12rem',
  maxHeight: '8rem',
  mr: 1
};

export const imageUploadDetailsFormStyle: ThemeUIStyleObject = {
  flexDirection: 'column',
  mt: 4
};

export const imageDetailsFormWrapper: ThemeUIStyleObject = {
  flexDirection: 'column'
};

export const formInputWrapperStyle: ThemeUIStyleObject = {
  mb: 4
};

export const formHeadingBottomMargin: ThemeUIStyleObject = {
  mb: 2
};

export const imageEditingStyle = (selected?: boolean): ThemeUIStyleObject => {
  return {
    borderColor: selected ? 'purple' : 'transparent',
    borderWidth: '4px',
    borderStyle: 'solid',
    filter: selected ? 'none' : 'brightness(30%) grayscale(10%)'
  };
};

export const fileMetaDataKeyStyle: ThemeUIStyleObject = {
  color: 'accent',
  fontWeight: 'bold'
};

export const fileMetaFieldStyle: ThemeUIStyleObject = {
  width: '100%',
  justifyContent: 'space-between',
  maxWidth: '20rem'
};
