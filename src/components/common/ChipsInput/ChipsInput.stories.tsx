import React, { useState } from 'react';
import ChipsInput from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Box } from 'theme-ui';

export default {
  title: 'Common/ChipsInput',
  component: ChipsInput
} as ComponentMeta<typeof ChipsInput>;

const Template: ComponentStory<typeof ChipsInput> = (args) => {
  const [currentChips, setCurrentChips] = useState(args.items);
  const onChipsChange = (chip: string[] | undefined) => {
    setCurrentChips(chip);
  };

  return (
    <Box sx={{ width: '40rem' }}>
      <ChipsInput {...args} items={currentChips} onChipsChange={onChipsChange} />
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {
  items: ['test', 'another', 'more'],
  placeholderText: 'Items to be stored as array'
};
