import { ThemeUIStyleObject } from 'theme-ui';

export const chipsInputStyle: ThemeUIStyleObject = {};

export const chipsInputWrapperStyle: ThemeUIStyleObject = {
  mt: 2,
  alignItems: 'center'
};
