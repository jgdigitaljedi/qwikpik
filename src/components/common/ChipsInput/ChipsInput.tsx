import React, { KeyboardEvent, useState } from 'react';
import { BsFillPlusCircleFill } from 'react-icons/bs';
import { Box, Flex, IconButton, Input } from 'theme-ui';
import Chips from '../Chips/Chips';
import { chipsInputStyle, chipsInputWrapperStyle } from './ChipsInput.style';

interface ChipsInputProps {
  items?: string[];
  placeholderText: string;
  onChipsChange: (items?: string[]) => void;
}

const ChipsInput: React.FC<ChipsInputProps> = ({ items, placeholderText, onChipsChange }) => {
  const [currentChip, setCurrentChip] = useState('');

  const onChipAdded = () => {
    if (currentChip?.length && items && items?.indexOf(currentChip) < 0) {
      const newChips = items?.length ? [...items, currentChip] : [currentChip];
      onChipsChange(newChips);
      setCurrentChip('');
    }
  };

  const onKeyDown = (event: KeyboardEvent<HTMLInputElement> | undefined): void => {
    if (event?.key === 'Enter' && currentChip?.length) {
      onChipAdded();
    } else if (event?.target) {
      // @ts-ignore
      setCurrentChip(event.target?.value);
    }
  };

  return (
    <Box sx={chipsInputStyle}>
      <Chips items={items} onRemoveCb={onChipsChange} />
      <Flex sx={chipsInputWrapperStyle}>
        <Input
          placeholder={placeholderText}
          value={currentChip}
          onChange={(event) => setCurrentChip(event.target.value)}
          onKeyDown={onKeyDown}
        />
        <IconButton onClick={onChipAdded}>
          <BsFillPlusCircleFill size="3rem" />
        </IconButton>
      </Flex>
    </Box>
  );
};

export default ChipsInput;
