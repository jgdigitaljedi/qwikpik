import { ThemeUIStyleObject } from 'theme-ui';
import { newColors } from 'theme/theme';

export const passwordWrapperStyles: ThemeUIStyleObject = {
  position: 'relative',
  width: '100%'
};

export const showIconStyles: ThemeUIStyleObject = {
  position: 'absolute',
  top: '1.5rem',
  right: '1rem',
  width: '1.5rem',
  height: '1.5rem',
  svg: {
    width: '1.5rem',
    height: '1.5rem'
  }
};

export const complexityBarBaseStyles = {
  borderTopRightRadius: 0,
  borderTopLeftRadius: 0,
  borderBottomLeftRadius: 1,
  borderBottomRightRadius: 1,
  height: '.6rem'
};

export function complexityBarColor(score: number) {
  switch (score) {
    case 1:
      return newColors.red;
    case 2:
      return newColors.orange;
    case 3:
      return newColors.yellow;
    case 4:
      return newColors.green;
    case 5:
      return newColors.blue;
  }
}
