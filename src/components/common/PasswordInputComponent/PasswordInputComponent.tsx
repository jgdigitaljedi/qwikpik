import React, { ChangeEvent, useState } from 'react';
import { Box, Input, Progress, ThemeUIStyleObject } from 'theme-ui';
import { BsFillEyeFill, BsFillEyeSlashFill } from 'react-icons/bs';
import {
  complexityBarBaseStyles,
  complexityBarColor,
  passwordWrapperStyles,
  showIconStyles
} from './PasswordInputComponent.styles';
import zxcvbn from 'zxcvbn';

interface PasswordInputProps {
  password: string;
  setPasswordCb: (pw: string) => void;
  placeHolder: string;
  ariaLabel: string;
  fieldName: string;
  baseStyles?: ThemeUIStyleObject;
  showComplexity?: boolean;
}

const PasswordInputComponent: React.FC<PasswordInputProps> = ({
  password,
  setPasswordCb,
  placeHolder,
  ariaLabel,
  fieldName,
  baseStyles,
  showComplexity
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const [pwComplexity, setPwComplexity] = useState(0);

  const onPwChange = (event: ChangeEvent) => {
    const pw = (event?.target as HTMLInputElement).value || '';
    setPasswordCb(pw);
    if (showComplexity) {
      setPwComplexity(zxcvbn(pw).score + (pw?.length === 0 ? 0 : 1));
    }
  };

  return (
    <Box sx={passwordWrapperStyles}>
      <Input
        type={showPassword ? 'text' : 'password'}
        value={password}
        onChange={onPwChange}
        sx={baseStyles || {}}
        placeholder={placeHolder}
        aria-label={ariaLabel}
        name={fieldName}
      />
      <Box sx={showIconStyles} onClick={() => setShowPassword(!showPassword)}>
        {showPassword && <BsFillEyeSlashFill aria-label="Hide password" />}
        {!showPassword && <BsFillEyeFill aria-label="Show password" />}
      </Box>
      {showComplexity && (
        <Progress
          data-testid="pw-complexity-bar"
          max={5}
          value={pwComplexity}
          sx={complexityBarBaseStyles}
          color={complexityBarColor(pwComplexity)}
        />
      )}
    </Box>
  );
};

export default PasswordInputComponent;
