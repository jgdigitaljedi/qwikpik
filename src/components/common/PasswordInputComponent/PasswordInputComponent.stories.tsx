import React, { useState } from 'react';
import PasswordInputComponent from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
  title: 'Common/PasswordInputComponent',
  component: PasswordInputComponent
} as ComponentMeta<typeof PasswordInputComponent>;

const Template: ComponentStory<typeof PasswordInputComponent> = (args) => {
  const [currentPw, setCurrentPw] = useState(args.password);
  const pwChangeCb = (val: string) => {
    console.log('password value', val);
    setCurrentPw(val);
  };

  return <PasswordInputComponent {...args} setPasswordCb={pwChangeCb} password={currentPw} />;
};

export const Default = Template.bind({});
Default.args = {
  password: 'Test',
  placeHolder: 'Enter password',
  ariaLabel: 'password field',
  fieldName: 'password-story',
  showComplexity: true
};
