import { ThemeUIStyleObject } from 'theme-ui';

export const imageEditImagePreviewStyle: ThemeUIStyleObject = {
  maxWidth: '12rem',
  maxHeight: '8rem'
};

export const imageEditWrapperStyle = (selected?: boolean): ThemeUIStyleObject => {
  return {
    borderColor: selected ? 'purple' : 'transparent',
    borderWidth: '4px',
    borderStyle: 'solid',
    filter: selected ? 'none' : 'brightness(30%) grayscale(10%)'
  };
};

export const imageStatusIconStyle: ThemeUIStyleObject = {
  top: 2,
  left: 2,
  position: 'absolute',
  zIndex: 1000
};

export const imageDeleteIconStyle: ThemeUIStyleObject = {
  top: 2,
  right: 2,
  position: 'absolute',
  zIndex: 1000,
  cursor: 'pointer'
};
