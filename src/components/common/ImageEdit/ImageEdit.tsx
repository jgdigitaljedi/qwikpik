import React from 'react';
import { BsCheckCircleFill, BsFillPatchExclamationFill, BsFillXCircleFill } from 'react-icons/bs';
import { Box, Image } from 'theme-ui';
import { SelectedFilesDetails } from 'types/imageTypes';
import {
  imageDeleteIconStyle,
  imageEditImagePreviewStyle,
  imageEditWrapperStyle,
  imageStatusIconStyle
} from './ImageEdit.style';
import { newColors } from 'theme/theme';

interface ImageEditProps {
  image: SelectedFilesDetails;
  selected: boolean;
  changeSelectedImage: (image: SelectedFilesDetails) => void;
  onRemoveClick: (image: SelectedFilesDetails) => void;
}

const ImageEdit: React.FC<ImageEditProps> = ({
  image,
  selected,
  changeSelectedImage,
  onRemoveClick
}) => {
  const getStatusIcon = () => {
    if (image.hasDetails) {
      return <BsCheckCircleFill color={newColors.green} />;
    }
    return <BsFillPatchExclamationFill color={newColors.red} />;
  };

  return (
    <Box sx={{ position: 'relative' }}>
      <Box sx={imageEditWrapperStyle(selected)}>
        <Image
          key={image.file.name}
          sx={imageEditImagePreviewStyle}
          src={URL.createObjectURL(image.file)}
          onClick={() => changeSelectedImage(image)}
        />
      </Box>
      <Box sx={imageStatusIconStyle}>{getStatusIcon()}</Box>
      <Box sx={imageDeleteIconStyle}>
        <BsFillXCircleFill color={newColors.white} onClick={() => onRemoveClick(image)} />
      </Box>
    </Box>
  );
};

export default ImageEdit;
