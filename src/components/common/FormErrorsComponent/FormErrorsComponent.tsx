import React from 'react';
import { Box, Text } from 'theme-ui';
import { FormValidation } from 'types/commonTypes';

interface FormErrorsProps {
  formErrors: FormValidation[];
}

const FormErrorsComponent: React.FC<FormErrorsProps> = ({ formErrors }) => {
  if (!formErrors?.length) {
    return <></>;
  }
  return (
    <Box
      as="ul"
      sx={{
        mt: 2,
        listStyle: 'none',
        paddingInlineStart: 0,
        li: { ml: 0, '&::before': { content: '"•"', color: 'red', mr: 2 } }
      }}
    >
      {formErrors.map((error, index: number) => {
        return (
          <li key={`${error.key}-${index}`}>
            <Text variant="errorText">{error.error}</Text>
          </li>
        );
      })}
    </Box>
  );
};

export default FormErrorsComponent;
