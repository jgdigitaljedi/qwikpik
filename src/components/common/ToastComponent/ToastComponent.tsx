import { useNotificationContext } from 'contexts/notification.context';
import React, { useEffect } from 'react';
import {
  BsCheckSquareFill,
  BsFillExclamationOctagonFill,
  BsFillExclamationTriangleFill,
  BsInfoCircleFill
} from 'react-icons/bs';
import { Alert, Close, Text } from 'theme-ui';
import { toastAlertWrapper, toastMessageStyle } from './ToastComponent.style';
import { newColors } from 'theme/theme';

const ToastComponent: React.FC = () => {
  const { notificationContextValue, setNotificationContext, defaultNotificationState } =
    useNotificationContext();

  const toastIcon = () => {
    switch (notificationContextValue.variant) {
      case 'success':
        return <BsCheckSquareFill aria-label="success" color={newColors.white} />;
      case 'error':
        return <BsFillExclamationOctagonFill aria-label="error" color={newColors.white} />;
      case 'info':
        return <BsInfoCircleFill aria-label="info" color={newColors.white} />;
      case 'warning':
        return <BsFillExclamationTriangleFill aria-label="warning" color={newColors.white} />;
      default:
        return <></>;
    }
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      setNotificationContext(defaultNotificationState);
    }, 5000);
    return () => {
      clearTimeout(timeout);
    };
  });

  if (notificationContextValue.message?.length > 0) {
    return (
      <Alert
        variant={notificationContextValue.variant}
        sx={toastAlertWrapper(notificationContextValue.position)}
      >
        {toastIcon()}
        <Text sx={toastMessageStyle}>{notificationContextValue.message}</Text>
        {notificationContextValue.closable && (
          <Close
            ml="auto"
            mr={-2}
            onClick={() => setNotificationContext(defaultNotificationState)}
            sx={{ cursor: 'pointer' }}
            aria-label="close"
            color={newColors.white}
          />
        )}
      </Alert>
    );
  }
  return <></>;
};

export default ToastComponent;
