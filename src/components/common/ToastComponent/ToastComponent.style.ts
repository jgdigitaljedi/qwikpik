import { ThemeUIStyleObject } from 'theme-ui';
import { newColors } from 'theme/theme';
import { ToastPosition } from 'types/commonTypes';

function getToastPosition(position?: ToastPosition) {
  switch (position) {
    case 'top-left':
      return { top: '5rem', left: '2rem' };
    case 'top-right':
      return { top: '5rem', right: '2rem' };
    case 'bottom-left':
      return { bottom: '2rem', left: '2rem' };
    case 'bottom-right':
      return { bottom: '2rem', right: '2rem' };
    default:
      return { bottom: '2rem', right: '2rem' };
  }
}

export const toastAlertWrapper = (position?: ToastPosition): ThemeUIStyleObject => {
  const toastPositionObj = getToastPosition(position);
  return {
    position: 'fixed',
    zIndex: 100,
    ...toastPositionObj
  };
};

export const toastMessageStyle: ThemeUIStyleObject = {
  mx: 2,
  color: newColors.white
};
