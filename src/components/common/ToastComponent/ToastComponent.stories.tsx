import React, { useState } from 'react';
import ToastComponent from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Button, Flex } from 'theme-ui';
import { useNotificationContext } from 'contexts/notification.context';

export default {
  title: 'Common/ToastComponent',
  component: ToastComponent
} as ComponentMeta<typeof ToastComponent>;

const Template: ComponentStory<typeof ToastComponent> = (args) => {
  const { setNotificationContext } = useNotificationContext();
  const showToast = (which: string) => {
    switch (which) {
      case 'success':
        setNotificationContext({
          message: `Test success message`,
          variant: 'success',
          closable: true
        });
        break;
      case 'info':
        setNotificationContext({
          message: `Test info message`,
          variant: 'info',
          closable: true
        });
        break;
      case 'warning':
        setNotificationContext({
          message: `Test warning message`,
          variant: 'warning',
          closable: true
        });
        break;
      case 'error':
        setNotificationContext({
          message: `Test error message`,
          variant: 'error',
          closable: true
        });
        break;
    }
  };

  return (
    <>
      <Flex sx={{ button: { mr: 2 } }}>
        <Button variant="primary" onClick={() => showToast('success')}>
          Success toast
        </Button>
        <Button variant="primary" onClick={() => showToast('info')}>
          Info toast
        </Button>
        <Button variant="primary" onClick={() => showToast('warning')}>
          Warning toast
        </Button>
        <Button variant="primary" onClick={() => showToast('error')}>
          Error toast
        </Button>
      </Flex>
      <ToastComponent />
    </>
  );
};

export const Default = Template.bind({});
Default.args = {};
