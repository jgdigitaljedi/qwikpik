import React, { useState } from 'react';
import ModalComponent from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Box, Button, Heading } from 'theme-ui';

export default {
  title: 'Common/ModalComponent',
  component: ModalComponent
} as ComponentMeta<typeof ModalComponent>;

const Template: ComponentStory<typeof ModalComponent> = (args) => {
  const [isShowing, setIsShowing] = useState(true);

  const getModalBody = () => {
    return (
      <Box>
        <Heading as="h4">This is a modal</Heading>
      </Box>
    );
  };

  return (
    <Box>
      <Button onClick={() => setIsShowing(!isShowing)}>Toggle modal</Button>
      <ModalComponent
        {...args}
        isShowing={isShowing}
        hide={() => setIsShowing(!isShowing)}
        modalBody={getModalBody()}
      />
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {
  title: 'Modal component',
  size: 'xl'
};
