import React from 'react';
import { createPortal } from 'react-dom';
import { Box, Card, Close, Divider, Flex, Heading } from 'theme-ui';
import {
  modalCardstyle,
  modalHeaderStyle,
  modalOverlayStyle,
  modalWrapperStyle
} from './ModalComponent.style';

export type ModalSizes = 'sm' | 'md' | 'lg' | 'xl';

interface ModalProps {
  size: ModalSizes;
  title: string;
  modalBody: any;
  isShowing: boolean;
  hide: () => void;
}

const ModalComponent: React.FC<ModalProps> = ({ size, title, modalBody, isShowing, hide }) =>
  isShowing
    ? createPortal(
        <React.Fragment>
          <Box sx={modalOverlayStyle} />
          <Flex sx={modalWrapperStyle}>
            <Card sx={modalCardstyle(size)}>
              <Flex sx={modalHeaderStyle}>
                <Heading as="h3">{title}</Heading>
                <Close onClick={hide} />
              </Flex>
              <Divider />
              <Box sx={{ height: '100%', maxHeight: '100%' }}>{modalBody}</Box>
            </Card>
          </Flex>
        </React.Fragment>,
        document.body
      )
    : null;

export default ModalComponent;
