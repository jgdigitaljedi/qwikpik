import { ThemeUIStyleObject } from 'theme-ui';
import { ModalSizes } from './ModalComponent';

export const modalOverlayStyle: ThemeUIStyleObject = {
  position: 'fixed',
  left: 0,
  top: 0,
  width: '100%',
  zIndex: 10,
  height: '100vh',
  bg: 'rgba(0, 0, 0, 0.75)'
};

export const modalWrapperStyle: ThemeUIStyleObject = {
  maxHeight: '98vh',
  p: 3,
  zIndex: 30,
  justifyContent: 'center',
  alignItems: 'center',
  position: 'fixed',
  top: '0',
  left: 0,
  width: '100%'
};

export const modalCardstyle = (size: ModalSizes): ThemeUIStyleObject => {
  let modalWidth = '';
  switch (size) {
    case 'sm':
      modalWidth = '25rem';
      break;
    case 'md':
      modalWidth = '38rem';
      break;
    case 'lg':
      modalWidth = '50rem';
      break;
    case 'xl':
      modalWidth = '100%';
      break;
    default: {
      modalWidth = '100%';
    }
  }

  return {
    width: modalWidth,
    maxWidth: modalWidth,
    height: 'auto',
    maxHeight: '97vh'
  };
};

export const modalHeaderStyle: ThemeUIStyleObject = {
  justifyContent: 'space-between'
};
