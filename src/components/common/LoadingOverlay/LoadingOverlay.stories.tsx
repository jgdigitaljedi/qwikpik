import React from 'react';
import LoadingOverlay from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
  title: 'Common/LoadingOverlay',
  component: LoadingOverlay
} as ComponentMeta<typeof LoadingOverlay>;

const Template: ComponentStory<typeof LoadingOverlay> = (args) => {
  return <LoadingOverlay {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  message: 'Testing the loader...'
};
