import { ThemeUIStyleObject } from 'theme-ui';

export const overlayStyle: ThemeUIStyleObject = {
  zIndex: 10,
  width: '100vw',
  height: '100vh',
  bg: 'rgba(0,0,0,0.6)',
  position: 'absolute',
  top: 0,
  left: 0,
  transition: 'all .3s ease-out'
};

export const spinnerBoxWrapperStyle: ThemeUIStyleObject = {
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  height: '100vh'
};

export const spinnerBoxStyle: ThemeUIStyleObject = {
  p: 6,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column'
};

export const overlayTextStyle: ThemeUIStyleObject = {
  fontStyle: 'italic',
  mt: 4
};
