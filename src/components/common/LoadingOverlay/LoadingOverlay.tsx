import React from 'react';
import { Box, Card, Flex, Spinner, Text } from 'theme-ui';
import {
  overlayStyle,
  overlayTextStyle,
  spinnerBoxStyle,
  spinnerBoxWrapperStyle
} from './LoadingOverlay.style';

interface LoadingOverlayProps {
  message?: string;
}

const LoadingOverlay: React.FC<LoadingOverlayProps> = ({ message = 'Fetching data...' }) => {
  return (
    <Box sx={overlayStyle}>
      <Flex sx={spinnerBoxWrapperStyle}>
        <Card sx={spinnerBoxStyle}>
          <Spinner size={80} />
          {message && <Text sx={overlayTextStyle}>{message}</Text>}
        </Card>
      </Flex>
    </Box>
  );
};

export default LoadingOverlay;
