import React from 'react';
import { screen } from '@testing-library/react';
import { createThemeOnlyWrapper } from '../../../__mocks__/renderHelpers';
import PaginateComponent from '../PaginateComponent';
import userEvent from '@testing-library/user-event';

const render = (total: number, page: number, amount: number, selectCb = jest.fn()) =>
  createThemeOnlyWrapper([
    <PaginateComponent
      total={total}
      page={page}
      amount={amount}
      pageSelectCb={selectCb}
      key="paginate"
    />
  ]);

describe('<PaginateComponent />', () => {
  it('should display 1-4, back and forward buttons, and first and last buttons when given more than 4 pages', async () => {
    render(100, 0, 10);

    await screen.findByText('1');
    expect(screen.getByText('2')).toBeInTheDocument();
    expect(screen.getByText('3')).toBeInTheDocument();
    expect(screen.getByText('4')).toBeInTheDocument();
    expect(screen.queryByText('5')).toBeNull();
    expect(screen.getByLabelText('Go to first page')).toBeInTheDocument();
    expect(screen.getByLabelText('Go to first page')).toBeDisabled();
    expect(screen.getByLabelText('Go back a page')).toBeInTheDocument();
    expect(screen.getByLabelText('Go back a page')).toBeDisabled();
    expect(screen.getByLabelText('Go forward a page')).toBeInTheDocument();
    expect(screen.getByLabelText('Go forward a page')).not.toBeDisabled();
    expect(screen.getByLabelText('Go to last page')).toBeInTheDocument();
    expect(screen.getByLabelText('Go to last page')).not.toBeDisabled();
  });

  it('should display 1-2 and forward/back buttons, but no first and last when given 2 pages', async () => {
    render(15, 0, 10);

    await screen.findByText('1');
    expect(screen.getByText('2')).toBeInTheDocument();
    expect(screen.queryByText('3')).toBeNull();
    expect(screen.queryByLabelText('Go to first page')).toBeNull();
    expect(screen.queryByLabelText('Go to last page')).toBeNull();
    expect(screen.getByLabelText('Go back a page')).toBeInTheDocument();
    expect(screen.getByLabelText('Go back a page')).toBeDisabled();
    expect(screen.getByLabelText('Go forward a page')).toBeInTheDocument();
    expect(screen.getByLabelText('Go forward a page')).not.toBeDisabled();
  });

  it('should change pages when page numbers clicked', async () => {
    const selectCb = jest.fn();
    const { rerender } = render(100, 0, 10, selectCb);

    await screen.findByText('1');
    expect(screen.getByLabelText('Current page (1)')).toBeInTheDocument();
    expect(screen.queryByText('5')).toBeNull();

    const thirdPage = screen.getByText('3');
    expect(thirdPage).toBeInTheDocument();
    userEvent.click(thirdPage);
    expect(selectCb).toHaveBeenCalledWith(2);

    rerender(<PaginateComponent total={100} page={2} amount={10} pageSelectCb={selectCb} />);
    await screen.getByLabelText('Current page (3)');
    expect(screen.getByText('5')).toBeInTheDocument();
    expect(screen.queryByText('1')).toBeNull();
  });

  it('should go forward and back a page when forward and back clicked', async () => {
    const selectCb = jest.fn();
    const { rerender } = render(100, 0, 10, selectCb);

    await screen.findByText('1');
    expect(screen.getByLabelText('Current page (1)')).toBeInTheDocument();
    expect(screen.queryByText('5')).toBeNull();

    const forwardButton = screen.getByLabelText('Go forward a page');
    expect(forwardButton).toBeInTheDocument();
    userEvent.click(forwardButton);
    expect(selectCb).toHaveBeenCalledWith(1);

    rerender(<PaginateComponent total={100} page={1} amount={10} pageSelectCb={selectCb} />);
    await screen.findByLabelText('Current page (2)');
    expect(screen.queryByText('5')).toBeNull();
    userEvent.click(screen.getByLabelText('Go forward a page'));
    expect(selectCb).toHaveBeenCalledWith(2);

    rerender(<PaginateComponent total={100} page={2} amount={10} pageSelectCb={selectCb} />);
    await screen.findByLabelText('Current page (3)');
    expect(screen.getByText('5')).toBeInTheDocument();
    expect(screen.queryByText('1')).toBeNull();
    userEvent.click(screen.getByLabelText('Go back a page'));
    expect(selectCb).toHaveBeenCalledWith(1);
  });

  it('should go to the first and last pages when buttons clicked', async () => {
    const selectCb = jest.fn();
    const { rerender } = render(100, 0, 10, selectCb);

    await screen.findByText('1');
    userEvent.click(screen.getByLabelText('Go to last page'));
    expect(selectCb).toHaveBeenCalledWith(9);

    rerender(<PaginateComponent total={100} page={9} amount={10} pageSelectCb={selectCb} />);
    await screen.findByLabelText('Current page (10)');
    userEvent.click(screen.getByLabelText('Go to first page'));
    expect(selectCb).toHaveBeenCalledWith(0);
  });
});
