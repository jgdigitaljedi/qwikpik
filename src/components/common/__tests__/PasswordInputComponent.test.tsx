import React from 'react';
import { screen } from '@testing-library/react';
import PasswordInputComponent from '../PasswordInputComponent';
import { createThemeOnlyWrapper } from '../../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';

const render = (children: JSX.Element[]) => createThemeOnlyWrapper(children);

describe('<PasswordInputComponent />', () => {
  it('should render all of the pieces of the component with all options', async () => {
    render([
      <PasswordInputComponent
        showComplexity={true}
        password="test123"
        placeHolder="Password"
        ariaLabel="Password"
        setPasswordCb={jest.fn()}
        fieldName="Password"
        key="pw-1"
      />
    ]);

    await screen.findByLabelText('Password');
    expect(screen.getByLabelText('Password')).toHaveAttribute('placeholder', 'Password');
    expect(screen.getByTestId('pw-complexity-bar')).toBeInTheDocument();
    expect(screen.getByLabelText('Show password')).toBeInTheDocument();
  });

  it('should show and hide password when icon is clicked', async () => {
    render([
      <PasswordInputComponent
        showComplexity={true}
        password="test123"
        placeHolder="password"
        ariaLabel="Password"
        setPasswordCb={jest.fn()}
        fieldName="Password"
        key="pw-1"
      />
    ]);

    await screen.findByLabelText('Password');
    const inputField = screen.getByLabelText('Password');
    expect(inputField).toHaveAttribute('type', 'password');
    userEvent.click(screen.getByLabelText('Show password'));
    expect(inputField).toHaveAttribute('type', 'text');
    userEvent.click(screen.getByLabelText('Hide password'));
    expect(inputField).toHaveAttribute('type', 'password');
  });

  it('should render without the compleixty bar when showComplexity is falsy', async () => {
    render([
      <PasswordInputComponent
        password="test123"
        placeHolder="password"
        ariaLabel="Password"
        setPasswordCb={jest.fn()}
        fieldName="Password"
        key="pw-1"
      />
    ]);

    await screen.findByLabelText('Password');
    expect(screen.queryByTestId('pw-complexity-bar')).toBeNull();
  });

  it('should return the password value when the user types', async () => {
    const mockCb = jest.fn();
    render([
      <PasswordInputComponent
        password="test123"
        placeHolder="password"
        ariaLabel="Password"
        setPasswordCb={mockCb}
        fieldName="Password"
        key="pw-1"
      />
    ]);

    await screen.findByLabelText('Password');
    const pwField = screen.getByLabelText('Password');
    userEvent.type(pwField, '4');
    expect(mockCb).toHaveBeenCalledTimes(1);
    expect(mockCb).toHaveBeenCalledWith('test1234');
  });
});
