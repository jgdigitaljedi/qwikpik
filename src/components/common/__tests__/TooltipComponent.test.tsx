import React from 'react';
import { screen } from '@testing-library/react';
import TooltipComponent from '../TooltipComponent';
import { createThemeOnlyWrapper } from '../../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';

const render = (children: JSX.Element[]) => createThemeOnlyWrapper(children);

describe('<TooltipComponent />', () => {
  it('should render the tooltip target', async () => {
    render([
      <TooltipComponent
        identifier="render-test"
        direction="top"
        message="This is a test tooltip"
        key="tt-1"
      />
    ]);
    await screen.findByLabelText('Information');
    expect(screen.getByText('This is a test tooltip')).not.toBeVisible();
  });

  it('should open the tooltip when the target is clicked', async () => {
    render([
      <TooltipComponent
        identifier="click-test"
        direction="top"
        message="This is a test tooltip"
        key="tt-1"
      />
    ]);
    await screen.findByLabelText('Information');
    expect(screen.getByText('This is a test tooltip')).not.toBeVisible();

    userEvent.click(screen.getByLabelText('Information'));
    await screen.findByText('This is a test tooltip');
    expect(screen.getByText('This is a test tooltip')).toBeVisible();
  });

  it('should open to the right direction', async () => {
    render([
      <TooltipComponent
        identifier="right-test"
        direction="right"
        message="This is a test tooltip"
        key="tt-1"
      />
    ]);
    await screen.findByLabelText('Information');
    expect(screen.getByText('This is a test tooltip')).not.toBeVisible();

    userEvent.click(screen.getByLabelText('Information'));
    await screen.findByText('This is a test tooltip');
    expect(screen.getByTestId('tooltip-wrapper')).toHaveStyle('left:1.5rem');
  });

  it('should open to the top direction', async () => {
    render([
      <TooltipComponent
        identifier="top-test"
        direction="top"
        message="This is a test tooltip"
        key="tt-1"
      />
    ]);
    await screen.findByLabelText('Information');
    expect(screen.getByText('This is a test tooltip')).not.toBeVisible();

    userEvent.click(screen.getByLabelText('Information'));
    await screen.findByText('This is a test tooltip');
    expect(screen.getByTestId('tooltip-wrapper')).toHaveStyle('left:-4.5rem');
  });

  it('should open to the bottom direction', async () => {
    render([
      <TooltipComponent
        identifier="bottom-test"
        direction="bottom"
        message="This is a test tooltip"
        key="tt-1"
      />
    ]);
    await screen.findByLabelText('Information');
    expect(screen.getByText('This is a test tooltip')).not.toBeVisible();

    userEvent.click(screen.getByLabelText('Information'));
    await screen.findByText('This is a test tooltip');
    expect(screen.getByTestId('tooltip-wrapper')).toHaveStyle('top:26px');
  });

  it('should open to the left direction', async () => {
    render([
      <TooltipComponent
        identifier="left-test"
        direction="left"
        message="This is a test tooltip"
        key="tt-1"
      />
    ]);
    await screen.findByLabelText('Information');
    expect(screen.getByText('This is a test tooltip')).not.toBeVisible();

    userEvent.click(screen.getByLabelText('Information'));
    await screen.findByText('This is a test tooltip');
    expect(screen.getByTestId('tooltip-wrapper')).toHaveStyle('right:26px');
  });
});
