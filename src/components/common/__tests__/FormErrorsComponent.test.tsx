import React from 'react';
import { render, screen } from '@testing-library/react';
import FormErrorsComponent from '../FormErrorsComponent';

const mockFormErrors = [
  { key: 'testField', error: 'Test error for testField' },
  { key: 'testField2', error: 'Test error for testField2' },
  { key: 'testField3', error: 'Test error for testField3' }
];

describe('<FormErrorsComponent />', () => {
  it('should render for errors when passed', async () => {
    render(<FormErrorsComponent formErrors={mockFormErrors} />);

    await screen.findByText('Test error for testField');
    expect(screen.getByText('Test error for testField2')).toBeInTheDocument();
    expect(screen.getByText('Test error for testField3')).toBeInTheDocument();
  });

  it('should render an empty fragment when formErrors is empty', () => {
    const { container } = render(<FormErrorsComponent formErrors={[]} />);
    expect(container.firstChild).toBeNull();
  });
});
