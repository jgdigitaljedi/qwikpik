import React from 'react';
import { screen } from '@testing-library/react';
import { createWrapper } from '../../../__mocks__/renderHelpers';
import ToastComponent from '../ToastComponent';
import { Box } from 'theme-ui';
import {
  NotificationContextProvider,
  useNotificationContext
} from '../../../contexts/notification.context';
import { NotificationProps } from '../../../types/commonTypes';

const render = (formCompWithProps: JSX.Element, value: NotificationProps) =>
  createWrapper({ Comp: formCompWithProps, isChild: true, notify: value });

const TestWrapper: React.FC = ({ children }) => {
  return (
    <Box>
      <ToastComponent />
    </Box>
  );
};

describe('ToastComponent', () => {
  it('should render a success message', async () => {
    const value: NotificationProps = {
      message: 'This is a test message for success.',
      variant: 'success',
      closable: true
    };
    render(<TestWrapper />, value);

    const message = await screen.findByText('This is a test message for success.');
    expect(message).toBeInTheDocument();
    expect(screen.getByLabelText('success')).toBeInTheDocument();
  });

  it('should render a error message', async () => {
    const value: NotificationProps = {
      message: 'This is a test message for error.',
      variant: 'error',
      closable: true
    };
    render(<TestWrapper />, value);

    const message = await screen.findByText('This is a test message for error.');
    expect(message).toBeInTheDocument();
    expect(screen.getByLabelText('error')).toBeInTheDocument();
  });

  it('should render a info message', async () => {
    const value: NotificationProps = {
      message: 'This is a test message for info.',
      variant: 'info',
      closable: true
    };
    render(<TestWrapper />, value);

    const message = await screen.findByText('This is a test message for info.');
    expect(message).toBeInTheDocument();
    expect(screen.getByLabelText('info')).toBeInTheDocument();
  });

  it('should render a warning message', async () => {
    const value: NotificationProps = {
      message: 'This is a test message for warning.',
      variant: 'warning',
      closable: true
    };
    render(<TestWrapper />, value);

    const message = await screen.findByText('This is a test message for warning.');
    expect(message).toBeInTheDocument();
    expect(screen.getByLabelText('warning')).toBeInTheDocument();
  });
});
