import React from 'react';
import TooltipComponent from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
  title: 'Common/TooltipComponent',
  component: TooltipComponent
} as ComponentMeta<typeof TooltipComponent>;

const Template: ComponentStory<typeof TooltipComponent> = (args) => {
  return <TooltipComponent {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  direction: 'top',
  message: 'This is a test tooltip',
  identifier: 'test'
};
