import React, { useEffect, useRef, useState } from 'react';
import { Box, Flex, Text, ThemeUIStyleObject, useColorMode } from 'theme-ui';
import {
  tooltipDirectionStyle,
  tooltipTargetStyle,
  tooltipWrapperBaseStyle
} from './TooltipComponent.styles';
import { BsFillInfoCircleFill } from 'react-icons/bs';
import { newColors } from 'theme/theme';

export type TooltipDirection = 'top' | 'right' | 'bottom' | 'left';

interface TooltipProps {
  direction: TooltipDirection;
  message: string;
  identifier: string;
  iconStyle?: ThemeUIStyleObject;
  wrapperStyle?: ThemeUIStyleObject;
}

const TooltipComponent: React.FC<TooltipProps> = ({
  direction,
  message,
  identifier,
  iconStyle,
  wrapperStyle
}) => {
  const targetRef = useRef<HTMLDivElement>(null);
  const tooltipRef = useRef<HTMLDivElement>(null);
  const iconRef = useRef<HTMLDivElement>(null);
  const [isVisible, setIsVisible] = useState(false);
  const [colorMode] = useColorMode();

  const handleClick = (event: Event): void => {
    if (
      tooltipRef?.current?.contains(event.target as Node) ||
      iconRef?.current?.contains(event.target as Node)
    ) {
      return;
    } else {
      setIsVisible(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClick);
    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, []);

  return (
    <Box ref={targetRef} sx={{ ...tooltipTargetStyle, ...iconStyle }}>
      <Box ref={iconRef} sx={{ cursor: 'pointer' }}>
        <BsFillInfoCircleFill
          fill={newColors.cyan}
          onClick={() => setIsVisible(!isVisible)}
          aria-label="Information"
        />
      </Box>
      <Flex
        role="tooltip"
        data-testid="tooltip-wrapper"
        ref={tooltipRef}
        sx={{
          ...tooltipWrapperBaseStyle(isVisible, colorMode),
          ...tooltipDirectionStyle(direction, tooltipRef, colorMode),
          ...wrapperStyle
        }}
      >
        <Text id={identifier} variant="primary">
          {message}
        </Text>
      </Flex>
    </Box>
  );
};

export default TooltipComponent;
