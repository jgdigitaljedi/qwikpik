import { RefObject } from 'react';
import { ThemeUIStyleObject } from 'theme-ui';
import { theme } from 'theme/theme';
import { TooltipDirection } from './TooltipComponent';

export const tooltipTargetStyle: ThemeUIStyleObject = {
  position: 'relative',
  display: 'inline-block'
};

export function tooltipWrapperBaseStyle(isVisible: boolean, colorMode: string): ThemeUIStyleObject {
  return {
    zIndex: 5,
    borderRadius: 'default',
    backgroundColor: colorMode === 'dark' ? 'background' : 'smoke',
    color: colorMode === 'dark' ? 'snow' : 'dark',
    boxShadow: 'elevated',
    position: 'absolute',
    p: 3,
    width: '10rem',
    justifyContent: 'center',
    alignItems: 'center',
    visibility: isVisible ? 'visible' : 'hidden',
    fontSize: 1
  };
}

export function tooltipDirectionStyle(
  direction: TooltipDirection,
  tooltipRef: RefObject<HTMLDivElement>,
  colorMode: string
): ThemeUIStyleObject {
  switch (direction) {
    case 'top':
      return {
        top: `-${tooltipRef?.current?.clientHeight || 0 + 10}px`,
        left: `-4.5rem`
      };
    case 'right':
      return {
        left: '1.5rem',
        top: `-${(tooltipRef?.current?.clientHeight || 0) / 2 - 10}px`,
        '&::before': {
          content: '""',
          borderStyle: 'solid',
          borderWidth: '10px 12px 10px 0',
          borderColor: `transparent ${
            colorMode === 'dark' ? theme?.colors?.dark : theme?.colors?.smoke
          } transparent transparent`,
          position: 'absolute',
          left: '-12px'
        }
      };
    case 'bottom':
      return {
        top: '26px',
        left: `-4.5rem`,
        '&::before': {
          content: '""',
          borderStyle: 'solid',
          borderWidth: '0 10px 12px 10px',
          borderColor: `transparent transparent ${
            colorMode === 'dark' ? theme?.colors?.dark : theme?.colors?.smoke
          } transparent`,
          position: 'absolute',
          top: '-12px'
        }
      };
    case 'left':
      return {
        right: `26px`,
        top: `-${(tooltipRef?.current?.clientHeight || 0) / 2 - 10}px`,
        '&::before': {
          content: '""',
          borderStyle: 'solid',
          borderWidth: '10px 0 10px 12px',
          borderColor: `transparent transparent transparent ${
            colorMode === 'dark' ? theme?.colors?.dark : theme?.colors?.smoke
          }`,
          position: 'absolute',
          right: '-12px'
        }
      };
  }
}
