import { ThemeUIStyleObject } from 'theme-ui';

export const paginateWrapperStyles: ThemeUIStyleObject = {
  justifyContent: 'center',
  alignItems: 'center',
  mt: 2
};

export const paginateNumbersWrapper: ThemeUIStyleObject = {
  justifyContent: 'space-between',
  button: {
    mx: 1
  }
};

export const paginateChevronStyle: ThemeUIStyleObject = {
  bg: 'blue',
  px: 2,
  py: 1,
  mx: 2,
  cursor: 'pointer'
};

export function getPaginationStyle(index: number, page: number): ThemeUIStyleObject {
  if (index === page) {
    return {
      fontSize: 3,
      fontWeight: 'bold',
      color: 'red'
    };
  }
  return {
    fontSize: 3,
    color: 'text'
  };
}
