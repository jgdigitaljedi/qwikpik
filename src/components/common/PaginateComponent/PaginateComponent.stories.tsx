import React, { useState } from 'react';
import PaginateComponent from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
  title: 'Common/PaginateComponent',
  component: PaginateComponent
} as ComponentMeta<typeof PaginateComponent>;

const Template: ComponentStory<typeof PaginateComponent> = (args) => {
  const [currentPage, setCurrentPage] = useState(args.page);
  const pageSelectedCb = (val: number) => {
    console.log('page value', val);
    setCurrentPage(val);
  };

  return <PaginateComponent {...args} page={currentPage} pageSelectCb={pageSelectedCb} />;
};

export const Default = Template.bind({});
Default.args = {
  total: 100,
  page: 0,
  amount: 10
};
