import React, { useEffect, useMemo, useState } from 'react';
import {
  BsChevronDoubleLeft,
  BsChevronDoubleRight,
  BsChevronLeft,
  BsChevronRight
} from 'react-icons/bs';
import { Button, Flex, IconButton, Text } from 'theme-ui';
import {
  getPaginationStyle,
  paginateChevronStyle,
  paginateNumbersWrapper,
  paginateWrapperStyles
} from './PaginateComponent.styles';
import { newColors, theme } from 'theme/theme';

interface PaginateProps {
  total: number;
  page: number;
  amount: number;
  pageSelectCb: (page: number) => void;
}

const PaginateComponent: React.FC<PaginateProps> = ({ total, page, amount, pageSelectCb }) => {
  const [showPagination, setShowPagination] = useState(false);
  const [totalPages, setTotalPages] = useState(1);

  const goToPage = (newPage: number) => {
    pageSelectCb(newPage);
  };

  const startPage = useMemo(() => {
    const enoughPages = totalPages > 3;
    const pastSecond = page > 1;
    if (enoughPages && pastSecond && page + 2 < totalPages) {
      return page - 1;
    } else if (enoughPages && pastSecond && page + 2 >= totalPages) {
      return totalPages - 4;
    } else {
      return 0;
    }
  }, [page, totalPages]);

  useEffect(() => {
    if (total > amount) {
      setShowPagination(true);
    }
    setTotalPages(Math.ceil(total / amount));
  }, [total, amount]);

  return (
    <Flex sx={paginateWrapperStyles}>
      {showPagination && totalPages > 4 && (
        <IconButton
          sx={paginateChevronStyle}
          onClick={() => goToPage(0)}
          disabled={page === 0}
          aria-label="Go to first page"
        >
          <BsChevronDoubleLeft color={newColors.white} />
        </IconButton>
      )}
      {showPagination && (
        <>
          <IconButton
            sx={paginateChevronStyle}
            onClick={() => goToPage(page - 1)}
            disabled={page === 0}
            aria-label="Go back a page"
          >
            <BsChevronLeft color={newColors.white} />
          </IconButton>
          <Flex sx={paginateNumbersWrapper}>
            {Array.apply(null, new Array(totalPages > 4 ? 4 : totalPages)).map(
              (val: any, index: number) => {
                return (
                  <Button
                    key={`pagination-button-${index + startPage}`}
                    variant="textButton"
                    onClick={() => goToPage(index + startPage)}
                    sx={getPaginationStyle(index + startPage, page)}
                    aria-label={
                      index + startPage === page
                        ? `Current page (${index + startPage + 1})`
                        : `Page ${index + startPage + 1}`
                    }
                  >
                    {index + startPage + 1}
                  </Button>
                );
              }
            )}
          </Flex>
          <IconButton
            sx={paginateChevronStyle}
            onClick={() => goToPage(page + 1)}
            disabled={page === totalPages - 1}
            aria-label="Go forward a page"
          >
            <BsChevronRight color={newColors.white} />
          </IconButton>
        </>
      )}
      {showPagination && totalPages > 4 && (
        <IconButton
          sx={paginateChevronStyle}
          onClick={() => goToPage(totalPages - 1)}
          disabled={page + 1 === totalPages}
          aria-label="Go to last page"
        >
          <BsChevronDoubleRight color={newColors.white} />
        </IconButton>
      )}
    </Flex>
  );
};

export default PaginateComponent;
