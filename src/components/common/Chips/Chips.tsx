import React from 'react';
import { BsFillXCircleFill } from 'react-icons/bs';
import { Flex, IconButton, Text } from 'theme-ui';
import { chipStyle, chipsWrapperStyle } from './Chips.style';

interface ChipsProps {
  items?: string[];
  onRemoveCb: (items: string[]) => void;
}

const Chips: React.FC<ChipsProps> = ({ items, onRemoveCb }) => {
  const removeChip = (item: string) => {
    const itemsMinusItem = items?.filter((i: string) => i !== item);
    onRemoveCb(itemsMinusItem || []);
  };

  if (!items?.length) {
    return <></>;
  }

  return (
    <Flex sx={chipsWrapperStyle}>
      {items.map((item: string) => {
        return (
          <Flex sx={chipStyle} key={item.replace(' ', '-')}>
            <Text>{item}</Text>
            <IconButton onClick={() => removeChip(item)}>
              <BsFillXCircleFill />
            </IconButton>
          </Flex>
        );
      })}
    </Flex>
  );
};

export default Chips;
