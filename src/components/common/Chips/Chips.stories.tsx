import React, { useState } from 'react';
import Chips from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Button, Flex } from 'theme-ui';

export default {
  title: 'Common/Chips',
  component: Chips
} as ComponentMeta<typeof Chips>;

const defaultChips = ['test', 'another', 'more'];

const Template: ComponentStory<typeof Chips> = (args) => {
  const [currentChips, setCurrentChips] = useState(args.items);
  const onRemoveCb = (items: string[]) => {
    setCurrentChips(items);
    args.onRemoveCb(items);
  };

  return (
    <Flex>
      <Button sx={{ mr: 4 }} onClick={() => setCurrentChips(defaultChips)}>
        Reset chips
      </Button>
      <Chips onRemoveCb={onRemoveCb} items={currentChips} />
    </Flex>
  );
};

export const Default = Template.bind({});
Default.args = {
  items: defaultChips,
  onRemoveCb: (items: string[]) => {
    console.log('remove item', items);
  }
};
