import { ThemeUIStyleObject } from 'theme-ui';

export const chipsWrapperStyle: ThemeUIStyleObject = {
  flexWrap: 'wrap'
};

export const chipStyle: ThemeUIStyleObject = {
  backgroundColor: 'elevated',
  borderRadius: 'ultra',
  mr: 2,
  pl: 3,
  pr: 1,
  mt: 1,
  alignItems: 'center',
  boxShadow: 'small',
  svg: {
    cursor: 'pointer'
  }
};
