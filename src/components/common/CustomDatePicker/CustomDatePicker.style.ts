import { ThemeUIStyleObject } from 'theme-ui';
import { theme } from '../../../theme/theme';

export function customDatepickerStyle(rawColors: any): ThemeUIStyleObject {
  return {
    '.react-datepicker__input-container': {
      height: 'auto',
      width: '100%',
      input: {
        lineHeight: '24px',
        backgroundColor: rawColors?.elevated || theme?.colors?.elevated,
        color: 'text',
        borderRadius: 'small',
        p: 2,
        fontSize: 2,
        borderImage: 'none',
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: 'slate',
        width: '100%',
        mt: 0
      }
    },
    '.custom-datepicker--popper .react-datepicker': {
      backgroundColor: rawColors?.elevated || theme?.colors?.elevated,
      color: 'text',
      borderRadius: 'default',

      '.react-datepicker__header, .react-datepicker__current-month, .react-datepicker__day-name, .react-datepicker__day, .react-datepicker__year-option, .react-datepicker__month-option':
        {
          backgroundColor: rawColors?.elevated || theme?.colors?.elevated,
          color: 'text'
        },
      '.react-datepicker__day--selected': {
        backgroundColor: rawColors?.primary || theme?.colors?.primary,
        borderRadius: 'small',
        color: theme?.colors?.dark
      },
      '.react-datepicker__triangle': {
        left: '2rem !important',
        transform: 'none !important',
        '::before, ::after': {
          borderBottomColor: 'elevated'
        }
      },
      '.react-datepicker__month-read-view': {
        width: '3.8rem'
      },
      '.react-datepicker__year-read-view': {
        ml: '1rem'
      },
      '.react-datepicker__year-read-view--down-arrow, .react-datepicker__month-read-view--down-arrow, .react-datepicker__month-year-read-view--down-arrow':
        {
          top: '4px'
        },
      '.react-datepicker__header__dropdown.react-datepicker__header__dropdown--scroll': {
        display: 'flex',
        p: '0 !important',
        m: '0 !important',
        justifyContent: 'center'
      },
      '.react-datepicker__month-year-dropdown-container--scroll': {
        mx: '12px'
      }
    }
  };
}
