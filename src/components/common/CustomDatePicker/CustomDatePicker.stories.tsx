import React, { useState } from 'react';
import CustomDatePicker from './';
import { ComponentStory, ComponentMeta } from '@storybook/react';

export default {
  title: 'Common/CustomDatePicker',
  component: CustomDatePicker
} as ComponentMeta<typeof CustomDatePicker>;

const Template: ComponentStory<typeof CustomDatePicker> = (args) => {
  const [dateSelected, setDateSelected] = useState(args.currentValue);
  const onChangeCb = (val: Date) => {
    setDateSelected(val);
  };

  return <CustomDatePicker {...args} currentValue={dateSelected} onChangeCb={onChangeCb} />;
};

export const Default = Template.bind({});
Default.args = {
  currentValue: new Date(),
  dtFormat: 'MM/dd/yyyy',
  label: 'Date picker',
  id: 'dp-story'
};
