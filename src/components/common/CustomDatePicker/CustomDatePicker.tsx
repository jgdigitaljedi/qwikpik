import React, { useMemo } from 'react';
import DatePicker from 'react-datepicker';
import 'node_modules/react-datepicker/dist/react-datepicker.css';
import { Box, Flex, Label } from 'theme-ui';
import { useThemeUI } from 'theme-ui';
import { customDatepickerStyle } from './CustomDatePicker.style';

interface CustomDatePickerProps {
  currentValue: Date | null;
  onChangeCb: (val: Date) => void;
  dtFormat: string | null;
  label?: string;
  id?: string;
}

const DPComponent: React.FC<CustomDatePickerProps> = ({
  currentValue,
  dtFormat,
  onChangeCb,
  id
}) => {
  const dateFormat = useMemo(() => {
    return dtFormat ? dtFormat.split(' ')[0] : 'MM/dd/yyyy';
  }, [dtFormat]);

  return (
    <DatePicker
      selected={currentValue}
      onChange={(date: Date) => onChangeCb(date)}
      dateFormat={dateFormat}
      scrollableYearDropdown={true}
      showMonthDropdown={true}
      showYearDropdown={true}
      popperClassName="custom-datepicker--popper"
      onMonthChange={(date: Date) => onChangeCb(date)}
      onYearChange={(date: Date) => onChangeCb(date)}
      startDate={new Date(1900, 0, 1)}
      endDate={new Date()}
      aria-label="date picker"
      id={id || 'date-picker'}
    />
  );
};

const CustomDatePicker: React.FC<CustomDatePickerProps> = ({
  currentValue,
  onChangeCb,
  dtFormat,
  label,
  id
}) => {
  const {
    theme: { rawColors }
  } = useThemeUI();

  return (
    <Box sx={customDatepickerStyle(rawColors)}>
      {label && (
        <Flex
          sx={{
            justifyContent: 'center',
            flexDirection: 'column',
            mt: 3,
            flex: 1,
            width: '100%'
          }}
        >
          <Label sx={{ mb: 0, fontWeight: 'bold' }}>{label}</Label>
          <DPComponent
            currentValue={currentValue}
            onChangeCb={onChangeCb}
            dtFormat={dtFormat}
            id={id}
          />
        </Flex>
      )}
      {!label && (
        <DPComponent
          currentValue={currentValue}
          onChangeCb={onChangeCb}
          dtFormat={dtFormat}
          id={id}
        />
      )}
    </Box>
  );
};

export default CustomDatePicker;
