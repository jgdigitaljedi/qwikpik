import { ThemeUIStyleObject } from 'theme-ui';

export const imageCardWrapperStyle: ThemeUIStyleObject = {
  // objectFit: 'contain'
};

export const imageCardImageStyle: ThemeUIStyleObject = {
  width: 'auto',
  cursor: 'pointer',
  objectFit: 'contain'
};

export const nsfwWrapperStyle: ThemeUIStyleObject = { maxWidth: '15rem', p: 2 };

export const nsfwDisclaimerStyle: ThemeUIStyleObject = { fontStyle: 'italic', mb: 2 };
