import ImageDialog from 'components/ImageDialog';
import ModalComponent from 'components/common/ModalComponent';
import useModal from 'hooks/useModal';
import React, { useEffect, useMemo, useState } from 'react';
import { Box, Button, Card, Image, Text } from 'theme-ui';
import { BaseImage } from 'types/imageTypes';
import {
  imageCardWrapperStyle,
  imageCardImageStyle,
  nsfwWrapperStyle,
  nsfwDisclaimerStyle
} from './ImageCard.styles';

interface ImageCardProps {
  image: BaseImage;
  currentPath: string;
}

// TODO: disable ability for underage folks to see nsfw images
// TODO: check user settings to see if it should just show nsfw images without a prompt
// TODO: improve appearence
const ImageCard: React.FC<ImageCardProps> = ({ image, currentPath }) => {
  const { toggle, isShowing } = useModal();
  const [showImage, setShowImage] = useState(false);

  const fullImagePath = useMemo(() => {
    // for Storybook only
    if (currentPath === 'https://via.placeholder.com/300') {
      return currentPath;
    } else {
      // for the app
      return `${process.env.BASEURL}library${currentPath}/${image.fileName}`;
    }
  }, [image]);

  const getModalBody = () => {
    return <ImageDialog image={image} fullImagePath={fullImagePath} hide={toggle} />;
  };

  useEffect(() => {
    if (!image.isNsfw) {
      setShowImage(true);
    }
  }, [image]);

  /** plans
   * - make mobile images fill screen width
   * - style up a bit?
   */

  return (
    <React.Fragment>
      <Card variant="galleryCard" sx={imageCardWrapperStyle}>
        <Text as="div">{image.title}</Text>
        {showImage && (
          <Image src={fullImagePath} sx={imageCardImageStyle} variant="gallery" onClick={toggle} />
        )}
        {!showImage && (
          <Box sx={nsfwWrapperStyle}>
            <Text as="p" sx={nsfwDisclaimerStyle}>
              This image has been marked as not safe for work and may include adult content. Click
              the button below if you wish to view the image.
            </Text>
            <Button onClick={() => setShowImage(true)}>Show image</Button>
          </Box>
        )}
      </Card>
      <ModalComponent
        isShowing={isShowing}
        hide={toggle}
        title={'Image Details'}
        modalBody={getModalBody()}
        size={'xl'}
      />
    </React.Fragment>
  );
};

export default ImageCard;
