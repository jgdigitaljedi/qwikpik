import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import ImageCard from '.';
import { placeholderImage } from 'stories/storybook.mocks';

export default {
  title: 'Gallery/ImageCard',
  component: ImageCard
} as ComponentMeta<typeof ImageCard>;

const Template: ComponentStory<typeof ImageCard> = (args) => {
  return <ImageCard {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  image: placeholderImage,
  currentPath: 'https://via.placeholder.com/300'
};

export const NSFW = Template.bind({});
NSFW.args = {
  image: { ...placeholderImage, isNsfw: true },
  currentPath: 'https://via.placeholder.com/300'
};
