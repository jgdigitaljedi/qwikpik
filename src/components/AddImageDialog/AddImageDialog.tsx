import FileUploadInput from 'components/common/FileUploadInput';
import React, { useState } from 'react';
import { addImages } from 'services/images.service';
import { Box, Button, Divider, Flex } from 'theme-ui';
import { ImageUploadBody, SelectedCb, SelectedFilesDetails } from 'types/imageTypes';

interface AddImageDialogProps {
  path: string;
  currentPath: string;
  modalClosed: () => void;
}

const AddImageDialog: React.FC<AddImageDialogProps> = ({ path, currentPath, modalClosed }) => {
  const [filesHaveDetails, setFilesHaveDetails] = useState(false);
  const [filesToUpload, setFilesToUpload] = useState<SelectedCb | null>(null);
  const filesSelectedCb = (files: SelectedCb) => {
    console.log('file in cb', files);
    setFilesToUpload(files);
    const readyForUpload =
      files.files
        .map((f: SelectedFilesDetails) => f.hasDetails)
        .filter((f: boolean | undefined) => !f).length === 0;
    setFilesHaveDetails(readyForUpload);
  };

  const onUploadImages = async () => {
    console.log('upload images addDialog', filesToUpload);
    console.log('path in AddImageDialog', path);
    if (!!filesToUpload) {
      const formData = filesToUpload.formData;
      formData?.append('data', JSON.stringify(filesToUpload.files));
      // const request: ImageUploadBody[] = filesToUpload.map((file: SelectedFilesDetails) => {
      //   const formData = new FormData();
      //   formData.append('File', file.file, file.file.name);
      //   console.log('formData in request', formData);
      //   return { fileInfo: file, formData };
      // });
      const uploaded = await addImages(formData);
      console.log('uploaded', uploaded);
      // TODO: make request to save and iron that out
    }
  };

  // TODO: my solution for height sucks. Find a better one

  return (
    <Box sx={{ overflowY: 'auto', maxHeight: '85vh', pb: 3 }}>
      <Flex>
        <FileUploadInput onSelectedCb={filesSelectedCb} path={path} pathDisplay={currentPath} />
      </Flex>
      <Divider />
      <Flex sx={{ justifyContent: 'flex-end', button: { mr: 2 }, mt: 3 }}>
        <Button variant="outline" onClick={modalClosed}>
          Cancel
        </Button>
        <Button disabled={!filesHaveDetails} variant="primary" onClick={onUploadImages}>
          Upload
        </Button>
      </Flex>
    </Box>
  );
};

export default AddImageDialog;
