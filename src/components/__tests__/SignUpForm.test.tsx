import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import { SignUpForm } from '../LoginComponent';
import { createWrapper } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';
import { signupErrorMessages } from '../../utils/userHelpers';
import * as userServiceCalls from '../../services/user.service';

const signUpMock = jest.fn();
jest.spyOn(userServiceCalls, 'addUser').mockImplementation(signUpMock);

const emailMock = jest.fn(() => Promise.resolve({ inUse: false }));
jest.spyOn(userServiceCalls, 'checkIfEmailInUse').mockImplementation(emailMock);

const render = (formCompWithProps: JSX.Element, history?: any, width?: string) =>
  createWrapper({ Comp: formCompWithProps, passedHistory: history, width, isChild: true });

describe('<SignUpForm />', () => {
  it('should render the signup form elements', async () => {
    render(<SignUpForm errorCb={jest.fn()} />);

    await screen.findByLabelText('Username');
    expect(screen.getByLabelText('Password')).toBeInTheDocument();
    expect(screen.getByLabelText('Confirm password')).toBeInTheDocument();
    expect(screen.getByLabelText('Name')).toBeInTheDocument();
    expect(screen.getByLabelText('Email address')).toBeInTheDocument();
    expect(screen.getByLabelText('Security question')).toBeInTheDocument();
    expect(screen.getByLabelText('Security question answer')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /Create account/ })).toBeInTheDocument();
    expect(screen.getByTestId('pw-complexity-bar')).toBeInTheDocument();
  });

  it.each([
    ['Username', 'a', signupErrorMessages.unInvalid],
    ['Password', 'a', signupErrorMessages.pwMatch],
    ['Name', '$', signupErrorMessages.nameInvalid],
    ['Email address', '$', signupErrorMessages.emailInvalid],
    ['Security question', '', signupErrorMessages.secQueInvalid],
    ['Security question answer', '', signupErrorMessages.secAnsInvalid]
  ])(
    'should show the proper error message when field with %s label fails validation',
    async (label, val, message) => {
      const errorCb = jest.fn();
      render(<SignUpForm errorCb={errorCb} />);

      await screen.findByLabelText(label);
      userEvent.type(screen.getByLabelText(label), val);
      userEvent.click(screen.getByRole('button', { name: /Create account/ }));
      const errorMessage = await screen.findByText(message);
      expect(errorMessage).toBeInTheDocument();
      expect(errorCb).toHaveBeenCalled();
    }
  );

  it('should show try to make the signup call', async () => {
    render(<SignUpForm errorCb={jest.fn()} />);

    await screen.findByLabelText('Username');

    // use valid field values to avoid validation errors
    userEvent.type(screen.getByLabelText('Username'), 'tester');
    userEvent.type(screen.getByLabelText('Password'), 'Tester123*');
    userEvent.type(screen.getByLabelText('Confirm password'), 'Tester123*');
    userEvent.type(screen.getByLabelText('Email address'), 'test@test.com');
    userEvent.type(screen.getByLabelText('Name'), 'Donald');
    userEvent.type(screen.getByLabelText('Security question'), 'Are you human');
    userEvent.type(screen.getByLabelText('Security question answer'), 'NO');
    userEvent.click(screen.getByRole('button', { name: /Create account/ }));

    await waitFor(() => expect(emailMock).toHaveBeenCalled());
    expect(signUpMock).toHaveBeenCalled();
    // TODO: once I deal with receiving result of adding user, write check for user object
  });
  // TODO: write test to check is addUser throws error
});
