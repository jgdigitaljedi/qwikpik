import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import LoginComponent from '../LoginComponent';
import { createWrapper } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';

const render = (history?: any, width?: string) =>
  createWrapper({ Comp: LoginComponent, passedHistory: history, width });

describe('<LoginComponent />', () => {
  it('should change to the signup form and back when toggle used', async () => {
    render();

    await screen.findByPlaceholderText('Username');
    expect(screen.queryByLabelText('Email address')).toBeNull();

    userEvent.click(screen.getByLabelText('Sign up'));
    const emailInput = await screen.findByLabelText('Email address');
    expect(emailInput).toBeInTheDocument();

    userEvent.click(screen.getByLabelText('Sign up'));
    expect(screen.queryByLabelText('Email address')).toBeNull();
  });
});
