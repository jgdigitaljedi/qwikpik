import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import NavbarComponent from '../NavbarComponent';
import { createWrapper } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';
import { createMemoryHistory } from 'history';
import { BaseUserWithToken } from '../../types/userTypes';

describe('<Navbar />', () => {
  const render = (history?: any, width?: string, loggedIn?: Partial<BaseUserWithToken>) =>
    createWrapper({ Comp: NavbarComponent, passedHistory: history, width, loggedIn });

  it('should render the navbar', async () => {
    render(undefined, undefined, { token: 'asdasd' });

    await screen.findByText('QwikPik');
    expect(screen.getByText('Gallery')).toBeInTheDocument();
    expect(screen.queryByText('Admin Dash')).toBeNull();
  });

  it('should try to navigate when a link is clicked', async () => {
    const history = createMemoryHistory();
    render(history, undefined, { token: 'asdasd' });

    await screen.findByText('QwikPik');
    const galleryLink = screen.getByText('Gallery');
    expect(galleryLink).toBeInTheDocument();
    userEvent.click(galleryLink);

    await waitFor(() => {
      expect(history.location.pathname).toBe('/gallery');
    });
  });

  it('should render the nav menu button when screen is smaller than lg breakpoint', async () => {
    const history = createMemoryHistory();
    render(history, '500px', { token: 'asdasd' });

    await screen.findByText('QwikPik');
    const menuButton = screen.getByLabelText('Toggle Menu');
    expect(menuButton).toBeInTheDocument();
  });

  it('should not show navlinks when not logged in', async () => {
    render();

    await screen.findByText('QwikPik');
    expect(screen.queryByText('Gallery')).toBeNull();
    expect(screen.queryByText('Profile')).toBeNull();
    expect(screen.queryByText('Admin Dash')).toBeNull();
  });

  it('should show admin dash if user is admin', async () => {
    render(undefined, undefined, { role: 'admin' });

    await screen.findByText('Gallery');
    expect(screen.getByText('Admin Dash')).toBeInTheDocument();
  });
});
