import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import { NavMenu } from '../NavbarComponent';
import { createWrapper } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';
import { createMemoryHistory } from 'history';
import { BaseUserWithToken } from '../../types/userTypes';

describe('<NavMenu />', () => {
  const render = (history?: any, width?: string, loggedIn?: Partial<BaseUserWithToken>) =>
    createWrapper({ Comp: NavMenu, passedHistory: history, width, loggedIn });

  it('should render the navmenu', async () => {
    render(undefined, undefined, { token: 'asdasda' });

    const galleryLink = await screen.findByText('Gallery');
    expect(galleryLink).toBeInTheDocument();
    expect(await screen.queryByText('Admin Dash')).toBeNull();
  });

  it('should try to navigate when a link is clicked', async () => {
    const history = createMemoryHistory();
    render(history, undefined, { token: 'asdasd' });

    const galleryLink = await screen.findByText('Gallery');
    // const galleryLink = screen.getByText('Gallery');
    expect(galleryLink).toBeInTheDocument();
    userEvent.click(galleryLink);

    waitFor(() => {
      expect(history.location.pathname).toBe('/gallery');
    });
  });

  it('should show admin dash if user is admin', async () => {
    render(undefined, undefined, { role: 'admin' });

    await screen.findByText('Gallery');
    expect(screen.getByText('Admin Dash')).toBeInTheDocument();
  });
});
