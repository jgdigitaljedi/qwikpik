import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import { LoginForm } from '../LoginComponent';
import { createWrapper } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';
import { createMemoryHistory } from 'history';
import { NavLinks } from '../../types/commonTypes';
import * as userServiceCalls from '../../services/user.service';

const render = (history?: any, width?: string) =>
  createWrapper({ Comp: LoginForm, passedHistory: history, width });

const loginMock = jest.fn();
const mockLoginCall = jest.spyOn(userServiceCalls, 'logInUser').mockImplementation(loginMock);

describe('<LoginForm />', () => {
  it('should show the elements of the login form', async () => {
    render();

    await screen.findByPlaceholderText('Username');
    expect(screen.getByLabelText('Username')).toBeInTheDocument();
    expect(screen.getByLabelText('Password')).toBeInTheDocument();
    expect(screen.getByText('Forgot password?')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: /Login/ })).toBeInTheDocument();
  });

  it('should display validation errors if username does not meet requirements', async () => {
    render();

    await screen.findByPlaceholderText('Username');

    // type username that is too short
    const unameInput = screen.getByLabelText('Username');
    userEvent.type(unameInput, 'ab');
    expect(unameInput).toHaveValue('ab');

    // check for validation error
    userEvent.click(screen.getByRole('button', { name: /Login/ }));
    await screen.findByText('You entered an invalid username.');

    // type username greater than 4 characters
    userEvent.type(unameInput, 'cde');
    expect(unameInput).toHaveValue('abcde');

    // check that validation error not there now
    userEvent.click(screen.getByRole('button', { name: /Login/ }));
    await screen.findByText(
      'The password you entered does not meet the minimum requirements so it cannot be your password.'
    );
    expect(screen.queryByText('You entered an invalid username.')).toBeNull();
  });

  it('should display validation errors if password does not meet requirements', async () => {
    render();

    await screen.findByPlaceholderText('Username');

    // type password that is too short
    const pwInput = screen.getByLabelText('Password');
    userEvent.type(pwInput, 'ab');
    expect(pwInput).toHaveValue('ab');

    // check for validation error
    userEvent.click(screen.getByRole('button', { name: /Login/ }));
    await screen.findByText(
      'The password you entered does not meet the minimum requirements so it cannot be your password.'
    );

    // type password that meets the minimum requirements
    userEvent.type(pwInput, 'cdefghijk1234*');
    expect(pwInput).toHaveValue('abcdefghijk1234*');

    // check that validation error not there now
    userEvent.click(screen.getByRole('button', { name: /Login/ }));
    await screen.findByText('You entered an invalid username.');
    expect(
      screen.queryByText(
        'The password you entered does not meet the minimum requirements so it cannot be your password.'
      )
    ).toBeNull();
  });

  it('should navigate to the forgot password route when the forgot password link button is clicked', async () => {
    const history = createMemoryHistory();
    render(history);

    await screen.findByPlaceholderText('Username');

    const forgotLink = screen.getByRole('button', { name: /Forgot password?/ });
    expect(forgotLink).toBeInTheDocument();
    userEvent.click(forgotLink);

    waitFor(() => {
      expect(history.location.pathname).toBe(NavLinks.FORGOT_PASSWORD);
    });
  });

  it('should make login call if fields are valid and login button is clicked', async () => {
    render();

    await screen.findByPlaceholderText('Username');

    // use valid field values to avoid validation errors
    userEvent.type(screen.getByLabelText('Username'), 'tester');
    userEvent.type(screen.getByLabelText('Password'), 'Tester123*');

    userEvent.click(screen.getByRole('button', { name: /Login/ }));

    expect(loginMock).toHaveBeenCalled();
  });

  // TODO: add tests for post-login and login errors once written
});
