import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import { createWrapper } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';
import { BaseUserWithToken } from '../../types/userTypes';
import SettingsDropdownComponent from '../SettingsDropdownComponent';
import { createMemoryHistory } from 'history';

describe('<SettingsDropdownComponent />', () => {
  const render = (component: JSX.Element, history?: any, width?: string) =>
    createWrapper({
      Comp: component,
      passedHistory: history,
      width,
      loggedIn: { token: 'asdasd' },
      isChild: true
    });

  it('should render all content', async () => {
    render(<SettingsDropdownComponent closeCb={() => {}} />);

    const prefs = await screen.findByText('Preferences');
    expect(prefs).toBeInTheDocument();
    expect(screen.getByText('Log out')).toBeInTheDocument();
    expect(screen.getByLabelText('Color theme')).toBeInTheDocument();
  });

  it('should navigate to preferences view and close dropdown when button clicked', async () => {
    const history = createMemoryHistory();
    render(<SettingsDropdownComponent closeCb={() => {}} />, history);

    const prefs = await screen.findByText('Preferences');
    expect(prefs).toBeInTheDocument();

    userEvent.click(prefs);
    expect(history.location.pathname).toBe('/user/preferences');
  });

  it('should delete token from localStorage and route to home when "Log out" clicked', async () => {
    const history = createMemoryHistory({ initialEntries: ['/gallery'] });
    render(<SettingsDropdownComponent closeCb={() => {}} />, history);

    localStorage.setItem('qpToken', '8675309');
    expect(localStorage.getItem('qpToken')).toEqual('8675309');

    const prefs = await screen.findByText('Preferences');
    expect(prefs).toBeInTheDocument();

    userEvent.click(screen.getByText('Log out'));
    expect(history.location.pathname).toBe('/');
  });
});
