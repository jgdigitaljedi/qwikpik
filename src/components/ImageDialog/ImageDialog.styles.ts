import { ThemeUIStyleObject } from 'theme-ui';

export const imageWrapperStyle: ThemeUIStyleObject = {
  height: 'auto',
  maxWidth: '75%',
  maxHeight: 'calc(97vh - 5.5rem)'
};

export const imageStyle: ThemeUIStyleObject = {
  objectFit: 'contain'
};

export const imageDetailsWrapperStyle: ThemeUIStyleObject = {
  flexDirection: 'column',
  pl: 2
};

export const dialogInnerWrapperStyle: ThemeUIStyleObject = {
  position: 'relative'
};

export const imageDetailsAttStyle: ThemeUIStyleObject = {
  mb: 3
};

export const imageDetailsLabelStyle: ThemeUIStyleObject = {
  fontWeight: 'bold',
  color: 'accent'
};
