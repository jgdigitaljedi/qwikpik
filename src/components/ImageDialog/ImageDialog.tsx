import { useUserContext } from 'contexts/user.context';
import { DateTime } from 'luxon';
import React from 'react';
import { Box, Flex, Image, Label, Text } from 'theme-ui';
import { BaseImage } from 'types/imageTypes';
import {
  dialogInnerWrapperStyle,
  imageDetailsAttStyle,
  imageDetailsLabelStyle,
  imageDetailsWrapperStyle,
  imageStyle,
  imageWrapperStyle
} from './ImageDialog.styles';

interface ImageDialogProps {
  image: BaseImage;
  fullImagePath: string;
  hide: () => void;
}

const ImageDialog: React.FC<ImageDialogProps> = ({ image, fullImagePath, hide }) => {
  const { userContextValue } = useUserContext();
  /**Plan
   * - show image on left as large as possible
   * - use right side to show image details
   * - if user that uploaded or admin (think about sup), add edit functionality
   * - if above scenario, also show delete option
   * - change modal so that if uploader of image or admin, it is an edit view. Otherwise, the details are like they are now
   */

  const formatDatesToUserFormat = (dateStr: string): string => {
    return DateTime.fromISO(dateStr).toFormat(userContextValue.dateTimeFormat);
  };

  return (
    <Flex sx={dialogInnerWrapperStyle}>
      <Flex sx={imageWrapperStyle}>
        <Image
          sx={imageStyle}
          src={fullImagePath}
          variant="fill"
          style={{ maxWidth: '100%' }}
          alt={image.title}
        />
      </Flex>
      <Flex sx={imageDetailsWrapperStyle}>
        <Box sx={imageDetailsAttStyle}>
          <Label sx={imageDetailsLabelStyle} htmlFor="title">
            Title
          </Label>
          <Text id="title">{image.title}</Text>
        </Box>
        <Box sx={imageDetailsAttStyle}>
          <Label sx={imageDetailsLabelStyle} htmlFor="fileName">
            File name
          </Label>
          <Text id="fileName">{image.fileName}</Text>
        </Box>
        <Box sx={imageDetailsAttStyle}>
          <Label sx={imageDetailsLabelStyle} htmlFor="tags">
            Tags
          </Label>
          <Text id="tags">{image.tags.join(', ')}</Text>
        </Box>
        <Box sx={imageDetailsAttStyle}>
          <Label sx={imageDetailsLabelStyle} htmlFor="uploadedDate">
            Uploaded on
          </Label>
          <Text id="title">{formatDatesToUserFormat(image.uploadDate)}</Text>
        </Box>
      </Flex>
    </Flex>
  );
};

export default ImageDialog;
