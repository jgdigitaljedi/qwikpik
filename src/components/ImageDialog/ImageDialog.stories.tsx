import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import ImageDialog from '.';
import { placeholderImage } from 'stories/storybook.mocks';

export default {
  title: 'Gallery/ImageDialog',
  component: ImageDialog
} as ComponentMeta<typeof ImageDialog>;

const Template: ComponentStory<typeof ImageDialog> = (args) => {
  return <ImageDialog {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  image: placeholderImage,
  fullImagePath: 'https://via.placeholder.com/300'
};
