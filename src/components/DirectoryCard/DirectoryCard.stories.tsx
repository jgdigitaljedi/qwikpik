import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import DirectoryCard from '.';

export default {
  title: 'Gallery/DirectoryCard',
  component: DirectoryCard
} as ComponentMeta<typeof DirectoryCard>;

const Template: ComponentStory<typeof DirectoryCard> = (args) => {
  return <DirectoryCard {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  directory: 'root',
  clickCb: () => {}
};
