import { imageCardWrapperStyle } from 'components/ImageCard/ImageCard.styles';
import React, { useMemo, useState } from 'react';
import { BsFillFolderFill, BsFillPlusCircleFill, BsFolderFill, BsImage } from 'react-icons/bs';
import { Box, Button, Card, Flex, Text } from 'theme-ui';
import { addButonWrapperStyle, iconAreaWrapper } from './DirectoryCard.style';

interface DirectoryCardProps {
  directory: string;
  clickCb: (dir: string) => void;
}

const DirectoryCard: React.FC<DirectoryCardProps> = ({ directory, clickCb }) => {
  const [addCardActive, setAddCardActive] = useState(false);
  const isAdd = useMemo(() => {
    return directory === '+';
  }, [directory]);

  const onCardClick = () => {
    if (isAdd) {
      setAddCardActive(!addCardActive);
    } else {
      clickCb(directory as string);
    }
  };

  const getContent = () => {
    if (isAdd && !addCardActive) {
      return <BsFillPlusCircleFill size="3rem" />;
    } else if (isAdd && addCardActive) {
      return (
        <Flex sx={addButonWrapperStyle}>
          <Button variant="outline" onClick={() => clickCb('image')}>
            <BsImage />
            Add image
          </Button>
          <Button variant="outline" onClick={() => clickCb('directory')}>
            <BsFolderFill />
            Add directory
          </Button>
        </Flex>
      );
    } else {
      return <BsFillFolderFill size="3rem" />;
    }
  };

  return (
    <React.Fragment>
      <Card variant="galleryCard" sx={imageCardWrapperStyle} onClick={onCardClick}>
        <Text as="div">{isAdd ? '' : directory}</Text>
        <Flex sx={iconAreaWrapper}>{getContent()}</Flex>
      </Card>
    </React.Fragment>
  );
};

export default DirectoryCard;
