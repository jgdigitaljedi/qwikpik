import { ThemeUIStyleObject } from 'theme-ui';

export const iconAreaWrapper: ThemeUIStyleObject = {
  height: '100%',
  justifyContent: 'center',
  alignItems: 'center',
  p: 3
};

export const addButonWrapperStyle: ThemeUIStyleObject = {
  flexDirection: 'column',
  button: {
    my: 2
  }
};
