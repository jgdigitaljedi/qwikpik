import { ThemeUIStyleObject } from 'theme-ui';

export const loginButtonsWrapperStyles: ThemeUIStyleObject = {
  justifyContent: 'flex-end',
  button: {
    ml: 3,
    mt: 3
  }
};

export const styleFieldErrors = (error: boolean | undefined): ThemeUIStyleObject | undefined => {
  if (error) {
    return {
      outlineStyle: 'solid',
      outlineColor: 'red',
      outlineWidth: '2px'
    };
  }
};

export const signUpTooltipContainerStyles: ThemeUIStyleObject = {
  ml: 2,
  mt: 3
};
