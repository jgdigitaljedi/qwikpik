import React, { useState } from 'react';
import { Card, Box, Heading, Flex, Switch, Alert } from 'theme-ui';
import { loginCardStyles } from 'theme/commonStyles';
import { LoginForm } from './LoginForm';
import { SignUpForm } from './SignUpForm';

const LoginComponent = () => {
  const [isSignUp, setIsSignUp] = useState(false);
  const [logSignError, setLogSignError] = useState('');

  const handleSignUp = (): void => {
    setIsSignUp(!isSignUp);
  };

  return (
    <Flex sx={{ flexDirection: 'column', maxWidth: '35rem', width: '100%' }}>
      {logSignError?.length > 0 && (
        <Alert variant="error" sx={{ mb: 2 }}>
          {logSignError}
          {/* <Close ml="auto" mr={-2} /> */}
        </Alert>
      )}
      <Card variant="primary" sx={loginCardStyles}>
        <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
          <Heading as="h2">{isSignUp ? 'Sign up' : 'Login'}</Heading>
          <Box sx={{ mt: 2 }}>
            <Switch label="Sign up" checked={isSignUp} onChange={handleSignUp} />
          </Box>
        </Flex>
        {isSignUp ? <SignUpForm errorCb={setLogSignError} /> : <LoginForm />}
      </Card>
    </Flex>
  );
};

export default LoginComponent;
