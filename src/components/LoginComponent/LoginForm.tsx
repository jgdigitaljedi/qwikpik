import FormErrorsComponent from 'components/common/FormErrorsComponent';
import React, { FormEvent, useState } from 'react';
import { Box, Button, Flex, Input } from 'theme-ui';
import { FormValidation, GenericError, NavLinks, StringKeyBoolVal } from 'types/commonTypes';
import { loginButtonsWrapperStyles, styleFieldErrors } from './LoginComponent.styles';
import { validateLoginValues } from '../../utils/userHelpers';
import { useNavigate } from 'react-router-dom';
import { logInUser } from 'services/user.service';
import PasswordInputComponent from 'components/common/PasswordInputComponent';
import { useUserContext } from 'contexts/user.context';
import { BaseUserWithToken } from 'types/userTypes';
import LoadingOverlay from 'components/common/LoadingOverlay';
import { inputWrapperStyles } from 'theme/commonStyles';

export const LoginForm: React.FC = () => {
  const [userNameInputValue, setUserNameInputValue] = useState('');
  const [passwordInputValue, setPasswordInputValue] = useState('');
  const [inputFieldErrors, setInputFieldErrors] = useState<StringKeyBoolVal>();
  const [formErrors, setFormErrors] = useState<FormValidation[]>([]);
  const { setUserContext } = useUserContext();
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const fieldErrors = {
    username: false,
    password: false
  };

  const makeLoginCall = async () => {
    setIsLoading(true);
    const user = await logInUser(userNameInputValue, passwordInputValue);
    if ((user as GenericError).error) {
      setIsLoading(false);
      setFormErrors([{ key: 'loginCreds', error: (user as GenericError).message }]);
    } else {
      setIsLoading(false);
      setUserContext(user as BaseUserWithToken);
      localStorage.setItem('qpToken', (user as BaseUserWithToken)?.token || '');
    }
    // TODO: handle post-login actions and errors
  };

  const tryLogin = (e: FormEvent): void => {
    e.preventDefault();
    const validationErrors = validateLoginValues(userNameInputValue, passwordInputValue);
    if (validationErrors?.length) {
      setFormErrors(validationErrors);
      const fieldErrorsCopy: StringKeyBoolVal = { ...fieldErrors };
      validationErrors.forEach((item) => {
        fieldErrorsCopy[item.key] = true;
      });
      setInputFieldErrors(fieldErrorsCopy);
    } else {
      setFormErrors([]);
      setInputFieldErrors(fieldErrors);
      makeLoginCall();
    }
  };

  const forgotPassword = () => {
    navigate(NavLinks.FORGOT_PASSWORD);
  };

  return (
    <Box as="form" sx={inputWrapperStyles} onSubmit={tryLogin}>
      {isLoading && <LoadingOverlay message="Logging in..." />}
      <Input
        value={userNameInputValue}
        placeholder="Username"
        aria-label="Username"
        onChange={(event) => setUserNameInputValue(event.target.value)}
        sx={styleFieldErrors(inputFieldErrors?.username)}
        autoFocus
      />
      <PasswordInputComponent
        baseStyles={styleFieldErrors(inputFieldErrors?.password)}
        password={passwordInputValue}
        setPasswordCb={setPasswordInputValue}
        placeHolder="Password"
        ariaLabel="Password"
        fieldName="password"
      />
      <Flex sx={loginButtonsWrapperStyles}>
        <Button variant="primary" onClick={tryLogin}>
          Login
        </Button>
      </Flex>
      <Flex sx={{ justifyContent: 'center' }}>
        <Button variant="linkButton" sx={{ mt: 2 }} onClick={forgotPassword}>
          Forgot password?
        </Button>
      </Flex>
      <FormErrorsComponent formErrors={formErrors} />
    </Box>
  );
};
