import CustomDatePicker from 'components/common/CustomDatePicker';
import FormErrorsComponent from 'components/common/FormErrorsComponent';
import LoadingOverlay from 'components/common/LoadingOverlay';
import PasswordInputComponent from 'components/common/PasswordInputComponent';
import TooltipComponent from 'components/common/TooltipComponent';
import { useUserContext } from 'contexts/user.context';
import { DateTime } from 'luxon';
import React, { MouseEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { addUser } from 'services/user.service';
import { Box, Button, Flex, Input } from 'theme-ui';
import { inputWrapperStyles } from 'theme/commonStyles';
import { FormValidation, GenericError, NavLinks, StringKeyBoolVal } from 'types/commonTypes';
import { BaseUserObj, BaseUserWithToken } from 'types/userTypes';
import { validateNewUserFields } from 'utils/userHelpers';
import {
  loginButtonsWrapperStyles,
  signUpTooltipContainerStyles,
  styleFieldErrors
} from './LoginComponent.styles';
import {
  answerMessage,
  birthdayMessage,
  confirmMessage,
  emailMessage,
  nameMessage,
  passwordMessage,
  questionMessage,
  usernameMessage
} from './SignUpTooltipMessages';

interface SignUpFormProps {
  errorCb: (arg: string) => void;
}

export const SignUpForm: React.FC<SignUpFormProps> = ({ errorCb }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [userNameInputValue, setUserNameInputValue] = useState('');
  const [passwordInputValue, setPasswordInputValue] = useState('');
  const [confirmPasswordInputValue, setConfirmPasswordInputValue] = useState('');
  const [nameInputValue, setNameInputValue] = useState('');
  const [emailInputValue, setEmailInputValue] = useState('');
  const [secQuestionInputValue, setSecQuestionInputValue] = useState('');
  const [secAnswerInputValue, setSecAnswerInputValue] = useState('');
  const [bdayInputValue, setbdayInputValue] = useState<Date>(new Date());
  const [inputFieldErrors, setInputFieldErrors] = useState<StringKeyBoolVal>();
  const [formErrors, setFormErrors] = useState<FormValidation[]>([]);
  const { setUserContext } = useUserContext();
  const navigate = useNavigate();

  const fieldErrors = {
    username: false,
    password: false,
    email: false,
    name: false,
    secQuestion: false,
    secAnswer: false
  };

  const trySignup = async (): Promise<void> => {
    const validationErrors = await validateNewUserFields(
      userNameInputValue,
      passwordInputValue,
      confirmPasswordInputValue,
      emailInputValue,
      nameInputValue,
      secQuestionInputValue,
      secAnswerInputValue,
      bdayInputValue
    );
    if (validationErrors?.length) {
      // @ts-ignore
      setFormErrors(validationErrors || []);
      const fieldErrorsCopy: StringKeyBoolVal = { ...fieldErrors };
      validationErrors.forEach((item) => {
        fieldErrorsCopy[item.key || 'key'] = true;
      });

      setInputFieldErrors(fieldErrorsCopy);
      errorCb('');
      setIsLoading(false);
    } else {
      setInputFieldErrors(fieldErrors);
      setFormErrors([]);
      try {
        setIsLoading(true);
        const newUser = await addUser({
          username: userNameInputValue,
          password: passwordInputValue,
          pwConfirm: confirmPasswordInputValue,
          email: emailInputValue,
          name: nameInputValue,
          secQuestion: secQuestionInputValue,
          secAnswer: secAnswerInputValue,
          birthday: bdayInputValue
        });
        if ((newUser as GenericError).error) {
          setIsLoading(false);
          errorCb(
            'There was an error creating your account. Please refresh the page and try again.'
          );
        } else if ((newUser as BaseUserObj).role) {
          const cUser: BaseUserObj = newUser as BaseUserWithToken;
          const bday = cUser.birthday;
          // @ts-ignore
          cUser.birthday = DateTime.fromJSDate(bday).toFormat('MM/dd/yyyy');
          setIsLoading(false);
          // @ts-ignore
          setUserContext(cUser as BaseUserWithToken);
          localStorage.setItem('qpToken', (cUser as BaseUserWithToken).token);
          navigate(NavLinks.NOTICE);
        }
      } catch (error) {
        errorCb('There was an error creating your account. Please refresh the page and try again.');
      }
    }
  };

  const preventDefaultAndTrySignup = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    trySignup();
  };

  return (
    <Box as="form" sx={inputWrapperStyles} onSubmit={trySignup}>
      {isLoading && <LoadingOverlay message="Creating user account..." />}
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <Input
          value={userNameInputValue}
          placeholder="Username"
          aria-label="Username"
          onChange={(event) => setUserNameInputValue(event.target.value)}
          sx={styleFieldErrors(inputFieldErrors?.username)}
          autoFocus
          aria-describedby="#username-tooltip"
        />
        <TooltipComponent
          identifier="username-tooltip"
          direction="left"
          message={usernameMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <PasswordInputComponent
          baseStyles={styleFieldErrors(inputFieldErrors?.password)}
          password={passwordInputValue}
          setPasswordCb={setPasswordInputValue}
          showComplexity={true}
          placeHolder="Password"
          ariaLabel="Password"
          fieldName="password"
          aria-describedby="#password-tooltip"
        />
        <TooltipComponent
          identifier="password-tooltip"
          direction="left"
          message={passwordMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <PasswordInputComponent
          baseStyles={styleFieldErrors(inputFieldErrors?.password)}
          password={confirmPasswordInputValue}
          setPasswordCb={setConfirmPasswordInputValue}
          placeHolder="Confirm password"
          ariaLabel="Confirm password"
          fieldName="confirmPassword"
          aria-describedby="#confirm-password-tooltip"
        />
        <TooltipComponent
          identifier="confirm-password-tooltip"
          direction="left"
          message={confirmMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <Input
          name="name"
          value={nameInputValue}
          placeholder="Name"
          aria-label="Name"
          onChange={(event) => setNameInputValue(event.target.value)}
          sx={styleFieldErrors(inputFieldErrors?.name)}
          aria-describedby="#name-tooltip"
        />
        <TooltipComponent
          identifier="name-tooltip"
          direction="left"
          message={nameMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <Input
          name="email"
          value={emailInputValue}
          placeholder="Email address"
          aria-label="Email address"
          onChange={(event) => setEmailInputValue(event.target.value)}
          sx={styleFieldErrors(inputFieldErrors?.email)}
          aria-describedby="email-tooltip"
        />
        <TooltipComponent
          identifier="email-tooltip"
          direction="left"
          message={emailMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <Input
          name="secQuestion"
          value={secQuestionInputValue}
          placeholder="Security question"
          aria-label="Security question"
          onChange={(event) => setSecQuestionInputValue(event.target.value)}
          sx={styleFieldErrors(inputFieldErrors?.secQuestion)}
          aria-describedby="#security-question-tooltip"
        />
        <TooltipComponent
          identifier="security-question-tooltip"
          direction="left"
          message={questionMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center' }}>
        <Input
          name="secAnswer"
          value={secAnswerInputValue}
          placeholder="Security question answer"
          aria-label="Security question answer"
          onChange={(event) => setSecAnswerInputValue(event.target.value)}
          sx={styleFieldErrors(inputFieldErrors?.secAnswer)}
          aria-describedby="#security-answer-tooltip"
        />
        <TooltipComponent
          identifier="security-answer-tooltip"
          direction="left"
          message={answerMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={{ justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
        <CustomDatePicker
          currentValue={bdayInputValue}
          dtFormat={'MM/dd/yyyy'}
          onChangeCb={(date: Date) => setbdayInputValue(date)}
          label="Birthday"
        />
        <TooltipComponent
          identifier="birthday-tooltip"
          direction="left"
          message={birthdayMessage}
          iconStyle={signUpTooltipContainerStyles}
        />
      </Flex>
      <Flex sx={loginButtonsWrapperStyles}>
        <Button variant="primary" onClick={preventDefaultAndTrySignup}>
          Create account
        </Button>
      </Flex>
      <FormErrorsComponent formErrors={formErrors} />
    </Box>
  );
};
