export const usernameMessage =
  'Your username must be unique and be at least 4 characters long with no special characters.';

export const passwordMessage =
  'Password must be 8-20 characters, contain at least 1 lower case and 1 capital letter, a number, and a special character.';

export const confirmMessage = 'Password in this field must match the field above.';

export const nameMessage = 'Name must be at least 1 character long.';

export const emailMessage = 'Must be a valid email address.';

export const questionMessage = 'You will have to answer this question to reset your password.';

export const birthdayMessage =
  'Your birthday will be used to determine if you can adjust your viewing settings for images and comments marked NSFW.';

export const answerMessage =
  'You will have to provide this answer to the question above to reset your password. Note that this is case sensitive!';
