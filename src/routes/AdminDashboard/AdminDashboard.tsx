import React, { useCallback, useEffect, useState } from 'react';
import { useUserContext } from 'contexts/user.context';
import { Box, Divider, Flex, Heading, Switch, Text } from 'theme-ui';
import { useNotificationContext } from 'contexts/notification.context';
import { useNavigate } from 'react-router-dom';
import { GenericError, NavLinks } from 'types/commonTypes';
import {
  dashPrefsInputFieldWrapperStyle,
  dashPrefsWrapper,
  mutedDividerStyle,
  userFieldStyle
} from 'theme/commonStyles';
import { adminDashMessaging } from './AdminDashboard.messaging';
import AdminUserList from 'routes/AdminDashboard/AdminUserList';
import AdminUserStatsComponent from 'routes/AdminDashboard/AdminUserStats';
import { AdminUserStats } from 'types/adminTypes';
import { getAdminUserStats } from 'services/admin.service';

/**Should have:
 * - list users, can delete users or make them inactive
 * - list inactive users, can change active status
 * - can change user password?
 */

const AdminDashboardComponent: React.FC = () => {
  const [inactiveOnly, setInactiveOnly] = useState(false);
  const [userStats, setUserStats] = useState<AdminUserStats | null>(null);
  const { userContextValue } = useUserContext();
  const { setNotificationContext } = useNotificationContext();
  const navigate = useNavigate();

  const handleInactiveSwitchChange = () => {
    setInactiveOnly(!inactiveOnly);
  };

  const getUserStats = useCallback(async () => {
    const statsResult = await getAdminUserStats();
    if ((statsResult as GenericError).error) {
      setUserStats(null);
    } else {
      setUserStats(statsResult as AdminUserStats);
    }
  }, []);

  useEffect(() => {
    if (userContextValue?._id && userContextValue?.role === 'basic') {
      setNotificationContext({
        message: 'You do not have permission to view this page. Redirecting to Home.',
        variant: 'warning',
        closable: false,
        position: 'top-left'
      });
      navigate(NavLinks.HOME);
    } else {
      getUserStats();
    }
  }, [userContextValue.role]);

  if (userContextValue?.role !== 'basic') {
    return (
      <Box sx={dashPrefsWrapper}>
        <Heading as="h1">Admin Dashboard</Heading>
        <Divider />
        <Box sx={userFieldStyle}>
          <AdminUserStatsComponent stats={userStats} />
        </Box>
        <Divider sx={mutedDividerStyle} />
        <Box sx={userFieldStyle}>
          <Heading as="h2">{adminDashMessaging.userManagementMessaging.title}</Heading>
          <Text>{adminDashMessaging.userManagementMessaging.sub}</Text>
          <Flex sx={{ ...dashPrefsInputFieldWrapperStyle, maxWidth: '100%' }}>
            <Box sx={{ my: 3 }}>
              <Switch
                label="Unapproved users only"
                checked={inactiveOnly}
                onChange={handleInactiveSwitchChange}
                aria-label="Unapproved users only"
              />
            </Box>
            {userContextValue?.role && (
              <AdminUserList
                inactiveOnly={inactiveOnly}
                emptyMessage={adminDashMessaging.userManagementMessaging.empty || ''}
                role={userContextValue?.role}
              />
            )}
          </Flex>
        </Box>
        <Divider sx={mutedDividerStyle} />
        <Box sx={userFieldStyle}>
          <Heading as="h2">{adminDashMessaging.imageManagementMessaging.title}</Heading>
          <Text>{adminDashMessaging.imageManagementMessaging.sub}</Text>
        </Box>
      </Box>
    );
  }
  return <></>;
};

export default AdminDashboardComponent;
