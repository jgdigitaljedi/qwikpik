import { UserPropMessages } from 'types/commonTypes';

interface AdminPropMessages extends UserPropMessages {
  empty?: string;
}

const userManagementMessaging: AdminPropMessages = {
  title: 'User management',
  sub: `Approve inactive users, delete trouble makers, alter user roles, change user settings, etc.`,
  error: 'An error occurred while trying to save your changes to this user. Please try again.',
  success: 'User successfully updated!',
  label: 'User management',
  empty: 'No users were returned.'
};

const imageManagementMessaging: AdminPropMessages = {
  title: 'Image management',
  sub: `Delete images, edit image tags and details, etc.`,
  error: 'An error occurred while trying to save your changes to this image. Please try again.',
  success: 'Image successfully updated!',
  label: 'Image management',
  empty: 'No images were returned.'
};

export const adminDashMessaging = {
  userManagementMessaging,
  imageManagementMessaging
};
