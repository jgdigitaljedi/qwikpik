import { ThemeUIStyleObject } from 'theme-ui';

export const statsMainWrapper: ThemeUIStyleObject = {
  my: 2,
  width: '100%'
};

export const statsFlexWrapper: ThemeUIStyleObject = {
  width: '100%',
  justifyContent: 'space-evenly',
  // borderRadius: 'default',
  // borderColor: 'text',
  // borderWidth: '1px',
  // borderStyle: 'solid',
  p: 2,
  mt: 2
};

export const statsNumberStyle: ThemeUIStyleObject = {
  fontSize: 3
};

export const statsSectionStyle: ThemeUIStyleObject = {
  alignItems: 'center',
  flexDirection: 'column'
};
