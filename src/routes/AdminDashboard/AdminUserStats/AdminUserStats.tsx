import React from 'react';
import { Box, Flex, Heading, Text } from 'theme-ui';
import { AdminUserStats } from 'types/adminTypes';
import {
  statsFlexWrapper,
  statsMainWrapper,
  statsNumberStyle,
  statsSectionStyle
} from './AdminUserStats.style';

interface AdminUserStatsProps {
  stats: AdminUserStats | null;
}

const AdminUserStatsComponent: React.FC<AdminUserStatsProps> = ({ stats }) => {
  if (stats) {
    return (
      <Box sx={statsMainWrapper}>
        <Heading as="h2">User stats</Heading>
        <Flex sx={statsFlexWrapper}>
          <Flex sx={statsSectionStyle}>
            <Heading as="h3">Total users</Heading>
            <Text sx={statsNumberStyle}>{stats.total}</Text>
          </Flex>
          <Flex sx={statsSectionStyle}>
            <Heading as="h3">Approved users</Heading>
            <Text sx={statsNumberStyle}>{stats.active}</Text>
          </Flex>
          <Flex sx={statsSectionStyle}>
            <Heading as="h3">Unapproved users</Heading>
            <Text sx={statsNumberStyle}>{stats.inactive}</Text>
          </Flex>
        </Flex>
      </Box>
    );
  }
  return <></>;
};

export default AdminUserStatsComponent;
