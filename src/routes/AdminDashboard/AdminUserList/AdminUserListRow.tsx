import { useUserContext } from 'contexts/user.context';
import React, { ChangeEvent, useState } from 'react';
import { Box, Card, Heading, Select } from 'theme-ui';
import { BaseUserObj } from 'types/userTypes';
import { listCard, userRowItemStyle } from './AdminUserList.style';
import { DateTime } from 'luxon';
import { useNotificationContext } from 'contexts/notification.context';
import { adminApproveUser, adminDeactivateUser, adminDeleteUser } from 'services/admin.service';
import { GenericError, ToastVariant } from 'types/commonTypes';
import useModal from 'hooks/useModal';
import ModalComponent from 'components/common/ModalComponent';
import AdminUserEditModalBody from './AdminUserEditModalBody';

interface AdminUserListRowProps {
  user: BaseUserObj;
  actionCb: () => void;
}

type UserActions = '' | 'onApproveUser' | 'onEditUser' | 'onDeactivateUser' | 'onDeleteUser';

// TODO: add image functionality after image uploads work
// TODO: add comment functionality after comments work
const AdminUserListRow: React.FC<AdminUserListRowProps> = ({ user, actionCb }) => {
  const { userContextValue } = useUserContext();
  const { setNotificationContext } = useNotificationContext();
  const [userAction, setUserAction] = useState<UserActions>('');
  const { toggle, isShowing } = useModal();

  const notify = (variant: ToastVariant, message: string, closable = true): void => {
    setNotificationContext({ variant, message, closable });
  };

  const getModalBody = () => {
    return <AdminUserEditModalBody user={user} hide={toggle} updatedCb={actionCb} />;
  };

  const onEditUser = () => {
    toggle();
    setUserAction('');
  };

  const onApproveUser = async () => {
    const result = await adminApproveUser(user);
    if ((result as GenericError)?.error) {
      notify(
        'error',
        'An error occurred and the user was not approved. Please refresh and try again.'
      );
    } else if ((result as BaseUserObj).active) {
      notify('success', 'User was approved and is now active!');
      actionCb();
    } else {
      notify(
        'warning',
        'Something went wrong and the user was not approved. Please refresh the page and try again.'
      );
      actionCb(); // just in case it did work
    }
  };

  const onDeleteUser = async () => {
    const result = await adminDeleteUser(user);
    if ((result as GenericError)?.error) {
      notify(
        'error',
        'An error occurred and the user was not deleted. Please refresh the page and try again.'
      );
    } else if (result) {
      // TODO: add something to ask about delting their images as well
      notify('success', 'User was deleted successfully!');
      actionCb();
    } else {
      notify(
        'warning',
        'Something went wrong and the user was not deleted. Please refresh the page and try again.'
      );
      actionCb(); // just in case it did work
    }
  };

  const onDeactivateUser = async () => {
    const result = await adminDeactivateUser(user);
    if ((result as GenericError)?.error) {
      notify(
        'error',
        'An error occurred and the user was not deactivated. Please refresh the page and try again.'
      );
    } else if (result) {
      // TODO: add something to ask about delting their images as well
      notify('success', 'User was deactivated successfully!');
      actionCb();
    } else {
      notify(
        'warning',
        'Something went wrong and the user was not deactivated. Please refresh the page and try again.'
      );
      actionCb(); // just in case it did work
    }
  };

  const actionsOptions = [
    { label: '---', value: '' },
    { label: !user.active ? 'Approve user' : null, value: 'onApproveUser' },
    { label: user.active ? 'Deactivate user' : null, value: 'onDeactivateUser' },
    { label: 'Edit user', value: 'onEditUser' },
    { label: 'Delete user', value: 'onDeleteUser' }
  ].filter((item) => item.label);

  const onSelectChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const action = event?.target?.value as UserActions;
    setUserAction(action);
    if (action) {
      switch (action) {
        case 'onApproveUser':
          onApproveUser();
          break;
        case 'onEditUser':
          onEditUser();
          break;
        case 'onDeactivateUser':
          onDeactivateUser();
          break;
        case 'onDeleteUser':
          onDeleteUser();
          break;
        default:
          return;
      }
    }
  };

  return (
    <React.Fragment>
      <Card sx={listCard}>
        <Box sx={userRowItemStyle}>
          <Heading as="h4">Name</Heading>
          {user.name}
        </Box>
        <Box sx={userRowItemStyle}>
          <Heading as="h4">Username</Heading>
          {user.username}
        </Box>
        <Box sx={userRowItemStyle}>
          <Heading as="h4">Active</Heading>
          {user.active.toString()}
        </Box>
        <Box sx={userRowItemStyle}>
          <Heading as="h4">Date added</Heading>
          {DateTime.fromISO(user.dateCreated).toFormat(userContextValue.dateTimeFormat)}
        </Box>
        <Box sx={userRowItemStyle}>
          <Heading as="h4">Actions</Heading>
          <Select onChange={onSelectChange} value={userAction} data-testid="user-actions">
            {actionsOptions.map((option, index) => {
              return (
                // @ts-ignore
                <option value={option.value} key={`user-action-${index}`}>
                  {option.label}
                </option>
              );
            })}
          </Select>
        </Box>
      </Card>
      <ModalComponent
        isShowing={isShowing}
        hide={toggle}
        title={'Edit user'}
        modalBody={getModalBody()}
        size={'md'}
      />
    </React.Fragment>
  );
};

export default AdminUserListRow;
