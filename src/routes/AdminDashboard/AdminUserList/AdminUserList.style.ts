import { ThemeUIStyleObject } from 'theme-ui';
import { userFieldStyle } from 'theme/commonStyles';

export const userListWrapper: ThemeUIStyleObject = {
  borderColor: 'text',
  borderWidth: '1px',
  borderRadius: 'default',
  borderStyle: 'solid',
  width: '100%',
  p: 2
};

export const listCard: ThemeUIStyleObject = {
  py: '16px !important',
  width: '100%',
  display: ['flex', 'flex', 'flex', 'grid', 'grid', 'grid'],
  gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr',
  flexDirection: 'column',
  my: 2
};

export const userRowItemStyle: ThemeUIStyleObject = {
  mr: [0, 0, 0, 3, 3, 3],
  mb: [3, 3, 3, 0, 0, 0]
};

export const userListTopControlsStyle: ThemeUIStyleObject = {
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%'
};

export const adminUsersTopToolbarStyle: ThemeUIStyleObject = {
  width: 'auto',
  display: 'flex',
  alignItems: 'center'
};

export const adminModalBodyField: ThemeUIStyleObject = {
  ...userFieldStyle,
  mt: 4,
  h4: { mb: 2, mt: 3 }
};

export const adminModalBodyFooter: ThemeUIStyleObject = {
  pt: 3,
  justifyContent: 'flex-end'
};

export const adminModalInput: ThemeUIStyleObject = {
  maxWidth: '20rem'
};
