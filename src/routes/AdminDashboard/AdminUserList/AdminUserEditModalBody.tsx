import React, { useState } from 'react';
import { Box, Button, Divider, Flex, Heading, Input, Label, Radio, Select, Switch } from 'theme-ui';
import { BaseUserObj } from 'types/userTypes';
import { adminModalBodyField, adminModalBodyFooter, adminModalInput } from './AdminUserList.style';
import {
  validateEmail,
  validateName,
  validateUsername,
  validatePassword,
  hashPassword
} from 'utils/userHelpers';
import { FormValidation } from 'types/commonTypes';
import FormErrorsComponent from 'components/common/FormErrorsComponent';
import { EditedUser } from 'types/adminTypes';
import { adminEditUser } from 'services/admin.service';
import { useNotificationContext } from 'contexts/notification.context';
import { useUserContext } from 'contexts/user.context';

interface ResetPw {
  resetPw: boolean;
  newPw: string;
}

interface AdminModalBodyProps {
  user: BaseUserObj;
  hide: () => void;
  updatedCb: () => void;
}

const AdminUserEditModalBody: React.FC<AdminModalBodyProps> = ({ user, hide, updatedCb }) => {
  const [userForm, setUserForm] = useState<BaseUserObj>(user);
  const [userPwReset, setUserPwReset] = useState<ResetPw>({ resetPw: false, newPw: '' });
  const [changeErrors, setChangeErrors] = useState<FormValidation[] | null>(null);
  const { setNotificationContext } = useNotificationContext();
  const { userContextValue } = useUserContext();

  const onChangeHandler = (value: string | boolean, which: keyof BaseUserObj) => {
    const newUserForm = { ...userForm, [which]: value };
    setUserForm(newUserForm);
  };

  const onResetPw = (value: boolean | string, which: keyof ResetPw) => {
    if (which === 'resetPw' && value && !userPwReset.newPw) {
      setUserPwReset({ resetPw: true, newPw: 'Qwikpik123!' });
    } else {
      setUserPwReset({ ...userPwReset, [which]: value });
    }
  };

  const onSaveChanges = async () => {
    let emailInvalid, nameInvalid, unameInvalid, pwInvalid;
    if (user.email !== userForm.email) {
      emailInvalid = await validateEmail(userForm.email);
    }
    if (user.name !== userForm.name) {
      nameInvalid = validateName(userForm.name);
    }
    if (user.username !== userForm.username) {
      unameInvalid = validateUsername(userForm.username);
    }
    if (userPwReset.resetPw) {
      pwInvalid = validatePassword(userPwReset.newPw, userPwReset.newPw);
    }
    if (emailInvalid || nameInvalid || unameInvalid || pwInvalid) {
      const errors = [
        { key: 'email', error: emailInvalid },
        { key: 'name', error: nameInvalid },
        { key: 'username', error: unameInvalid },
        { key: 'password', error: pwInvalid }
      ].filter((err) => err.error) as FormValidation[] | null;
      setChangeErrors(errors);
      return;
    } else {
      setChangeErrors(null);
      const editedUser: EditedUser = { ...userForm };
      if (userPwReset.resetPw && userPwReset.newPw) {
        editedUser.password = hashPassword(userPwReset.newPw);
      }
      const editResult = await adminEditUser(editedUser);
      if (editResult === true) {
        setNotificationContext({
          message: `${user.name} was successfully updated!`,
          variant: 'success',
          closable: true
        });
        updatedCb();
        hide();
      } else {
        setNotificationContext({
          message: `There was a problem updating ${user.name}. Please refresh and try again.`,
          variant: 'error',
          closable: true
        });
      }
    }
  };

  return (
    <Box sx={{ height: '90%', overflowY: 'auto', pb: 1 }}>
      {userContextValue?.role === 'admin' && (
        <Box sx={{ ...adminModalBodyField, mt: 0 }}>
          <Heading as="h4">Username</Heading>
          <Input
            value={userForm.username}
            placeholder="Username"
            onChange={(event) => onChangeHandler(event.target.value, 'username')}
            sx={adminModalInput}
          />
        </Box>
      )}
      {userContextValue?.role === 'admin' && (
        <Box sx={adminModalBodyField}>
          <Heading as="h4">Name</Heading>
          <Input
            value={userForm.name}
            placeholder="Name"
            onChange={(event) => onChangeHandler(event.target.value, 'name')}
            sx={adminModalInput}
          />
        </Box>
      )}
      {userContextValue?.role === 'admin' && (
        <Box sx={adminModalBodyField}>
          <Heading as="h4">Email</Heading>
          <Input
            value={userForm.email}
            placeholder="Name"
            onChange={(event) => onChangeHandler(event.target.value, 'email')}
            sx={adminModalInput}
          />
        </Box>
      )}
      {userContextValue?.role === 'admin' && (
        <Box sx={adminModalBodyField}>
          <Heading as="h4">Preferred label</Heading>
          <Flex>
            <Label sx={{ mr: 5, width: 'auto' }}>
              <Radio
                name="use-username"
                value="username"
                checked={userForm.preferredLabel === 'username'}
                onChange={() => onChangeHandler('username', 'preferredLabel')}
              />
              Username
            </Label>
            <Label>
              <Radio
                name="use-name"
                value="name"
                checked={userForm.preferredLabel === 'name'}
                onChange={() => onChangeHandler('name', 'preferredLabel')}
              />
              Name
            </Label>
          </Flex>
        </Box>
      )}
      <Box sx={adminModalBodyField}>
        <Heading as="h4">Approved/Unapproved status</Heading>
        <Switch
          onChange={() => onChangeHandler(!userForm.active, 'active')}
          label={'Approved'}
          checked={userForm.active}
        />
      </Box>
      {userContextValue?.role === 'admin' && (
        <Box sx={adminModalBodyField}>
          <Heading as="h4">Prefers high contrast</Heading>
          <Switch
            onChange={() => onChangeHandler(!userForm.prefersHighContrast, 'prefersHighContrast')}
            label={'High contrast'}
            checked={userForm.prefersHighContrast}
          />
        </Box>
      )}
      <Box sx={adminModalBodyField}>
        <Heading as="h4">Role</Heading>
        <Select
          onChange={(event) => onChangeHandler(event.target.value, 'role')}
          sx={adminModalInput}
          value={userForm.role}
          data-testid="user-role"
        >
          <option value="basic">basic</option>
          <option value="supervisor">supervisor</option>
          {userContextValue?.role === 'admin' && <option value="admin">admin</option>}
        </Select>
      </Box>
      <Box sx={adminModalBodyField}>
        <Heading as="h4">Reset password</Heading>
        <Switch
          onChange={() => onResetPw(!userPwReset.resetPw, 'resetPw')}
          label="Reset password"
          checked={userPwReset.resetPw}
          sx={{ mb: 2 }}
        />
        {userPwReset.resetPw && (
          <Input
            value={userPwReset.newPw}
            placeholder="Reset password"
            onChange={(event) => onResetPw(event.target.value, 'newPw')}
            sx={adminModalInput}
          />
        )}
      </Box>
      {changeErrors?.length && (
        <Box sx={adminModalBodyField}>
          <FormErrorsComponent formErrors={changeErrors} />
        </Box>
      )}
      <Divider />
      <Flex sx={adminModalBodyFooter}>
        <Button variant="outline" sx={{ mr: 3 }} onClick={hide}>
          Cancel
        </Button>
        <Button variant="primary" sx={{ color: 'darker', mr: 1 }} onClick={onSaveChanges}>
          Save changes
        </Button>
      </Flex>
    </Box>
  );
};

export default AdminUserEditModalBody;
