import React, { ChangeEvent, FormEvent, useCallback, useEffect, useState } from 'react';
import { BaseUserObj } from 'types/userTypes';
import { Heading, Box, Flex, Select, Label, Text } from 'theme-ui';
import AdminUserListRow from './AdminUserListRow';
import { getAllUsers } from 'services/user.service';
import {
  adminUsersTopToolbarStyle,
  userListTopControlsStyle,
  userListWrapper
} from './AdminUserList.style';
import PaginateComponent from 'components/common/PaginateComponent';
import { paginationPerPageOptions, sortByOptions } from 'utils/constants';

interface AdminUserListProps {
  emptyMessage: string;
  inactiveOnly: boolean;
  role: string;
}

interface GetUsersArgs {
  inactive?: boolean;
  newAmount?: number;
  page?: number;
  sortAttr?: string;
}

const AdminUserList: React.FC<AdminUserListProps> = ({ emptyMessage, inactiveOnly, role }) => {
  const [users, setUsers] = useState<BaseUserObj[]>([]);
  const [userPage, setUserPage] = useState(0);
  const [userAmount, setUserAmount] = useState(5);
  const [totalUsers, setTotalUsers] = useState(0);
  const [sortBy, setSortBy] = useState('name-asc');

  const getUsers = useCallback(
    async ({ inactive, newAmount, page, sortAttr }: GetUsersArgs) => {
      let newPage = page === undefined ? userPage : page;
      const amountToCall = newAmount || userAmount;
      if (newPage * amountToCall > totalUsers + amountToCall) {
        newPage = Math.floor(totalUsers / amountToCall);
        setUserPage(newPage);
      }
      const usersData = await getAllUsers(
        role,
        amountToCall,
        newPage,
        inactive,
        sortAttr || sortBy
      );
      setUsers(usersData.users);
      setTotalUsers(usersData.total);
    },
    [inactiveOnly, role, userAmount, userPage, setUsers, totalUsers]
  );

  const onPageChange = (page: number) => {
    setUserPage(page);
    getUsers({ inactive: inactiveOnly, page });
  };

  const onUserNumberDdChange = (event: ChangeEvent<HTMLSelectElement>) => {
    if (event?.target) {
      const newAmount = parseInt(event?.target?.value);
      setUserAmount(newAmount);
      getUsers({ inactive: inactiveOnly, newAmount });
    }
  };

  const onSortByChange = (event: ChangeEvent<HTMLSelectElement>) => {
    if (event?.target) {
      const newSort = event.target.value;
      setSortBy(newSort);
      getUsers({ sortAttr: newSort });
    }
  };

  const userActionCb = () => {
    getUsers({ inactive: inactiveOnly });
  };

  useEffect(() => {
    getUsers({ inactive: inactiveOnly });
  }, [inactiveOnly, getUsers]);

  if (users?.length > 0) {
    return (
      <Box sx={userListWrapper}>
        <Flex sx={userListTopControlsStyle}>
          <Label sx={adminUsersTopToolbarStyle}>
            <Text sx={{ mr: 2 }}>Users per page:</Text>
            <Select
              sx={{ pr: 4, width: 'auto' }}
              value={userAmount}
              onChange={onUserNumberDdChange}
              data-testid="user-num"
            >
              {paginationPerPageOptions.map((num: number) => {
                return (
                  <option key={`admin-user-list-option-${num}`} value={num}>
                    {num}
                  </option>
                );
              })}
            </Select>
          </Label>
          <Label sx={{ ...adminUsersTopToolbarStyle, ml: 4 }}>
            <Text sx={{ mr: 2 }}>Sort by:</Text>
            <Select value={sortBy} onChange={onSortByChange} data-testid="user-sort">
              {sortByOptions.map((option, index) => {
                return (
                  <option value={option.value} key={`user-sort-${index}`}>
                    {option.label}
                  </option>
                );
              })}
            </Select>
          </Label>
        </Flex>
        {users.map((user: BaseUserObj, index: number) => {
          return (
            <AdminUserListRow
              user={user}
              actionCb={userActionCb}
              key={`user-row-${user.username}-${index}`}
            />
          );
        })}

        <PaginateComponent
          total={totalUsers}
          page={userPage}
          amount={userAmount}
          pageSelectCb={onPageChange}
        />
      </Box>
    );
  }
  return <Heading as="h3">{emptyMessage}</Heading>;
};

export default AdminUserList;
