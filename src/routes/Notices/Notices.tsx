import React from 'react';
import {
  AdminNoticeComponent,
  UserNoticeComponent
} from 'components/AccountCreationNoticeComponent';
import { Flex } from 'theme-ui';
import { useUserContext } from 'contexts/user.context';

const NoticesComponent: React.FC = () => {
  const { userContextValue } = useUserContext();

  return (
    <Flex sx={{ justifyContent: 'center' }}>
      {userContextValue?.role === 'admin' ? (
        <AdminNoticeComponent name={userContextValue?.username || 'admin'} />
      ) : (
        <UserNoticeComponent name={userContextValue?.username || 'user'} />
      )}
    </Flex>
  );
};

export default NoticesComponent;
