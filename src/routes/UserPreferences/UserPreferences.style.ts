import { ThemeUIStyleObject } from 'theme-ui';
import { theme } from 'theme/theme';
import { inputWrapperStyles } from 'theme/commonStyles';

export const userPrefsInputWrapperStyle: ThemeUIStyleObject = {
  alignItems: 'center',
  mt: 2,
  select: { pr: '2rem', width: 'auto' }
};

export const userPrefsRadioWrapperStyle: ThemeUIStyleObject = {
  width: 'auto',
  mt: 2
};

export const userPrefsInputFieldWrapperStyle: ThemeUIStyleObject = {
  ...inputWrapperStyles,
  maxWidth: '35rem'
};
