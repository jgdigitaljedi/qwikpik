import React, { useEffect, useState } from 'react';
import PasswordInputComponent from 'components/common/PasswordInputComponent';
import { useUserContext } from 'contexts/user.context';
import {
  Box,
  Button,
  Checkbox,
  Divider,
  Flex,
  Heading,
  Input,
  Label,
  Radio,
  Select,
  Switch,
  Text
} from 'theme-ui';
import { FullUserObj, UserPrefsPreferredDisplay } from 'types/userTypes';
import {
  canToggleNsfw,
  dateTimeFormats,
  hashPassword,
  validateEmail,
  validateName,
  validatePassword,
  validateSecurity
} from 'utils/userHelpers';
import { userPrefsMessages } from './UserPreferences.messaging';
import {
  userPrefsInputFieldWrapperStyle,
  userPrefsRadioWrapperStyle
} from './UserPreferences.style';
import { editSelf } from 'services/user.service';
import {
  FormValidation,
  StringKeyBoolOrStringOrDateVal,
  StringKeyBoolOrStringVal,
  UserPropMessages
} from 'types/commonTypes';
import FormErrorsComponent from 'components/common/FormErrorsComponent';
import { useNotificationContext } from 'contexts/notification.context';
import {
  dashPrefsInputFieldWrapperStyle,
  dashPrefsWrapper,
  mutedDividerStyle,
  userFieldStyle
} from 'theme/commonStyles';
import { DateTime } from 'luxon';
import CustomDatePicker from 'components/common/CustomDatePicker';

interface ChangePasswordFields {
  password: string;
  confirmPassword: string;
  currentPassword: string;
}

interface SecurityFields {
  question: string;
  answer: string;
}

// it isn't all that dry, but its dead simple to follow and I didn't want to bring in Formik or something similar
const UserPreferencesComponent: React.FC = () => {
  // TODO: find common date/time formats and add options
  const { userContextValue, setUserContext } = useUserContext();
  const [highContrast, setHighContrast] = useState(false);
  const [userDtFormat, setUserDtFormat] = useState('MM/dd/yyyy HH:mm a');
  const [preferredDisplay, setPreferredDisplay] = useState<UserPrefsPreferredDisplay>('name');
  const [passwordFieldValues, setPasswordFieldValues] = useState<ChangePasswordFields>({
    password: '',
    confirmPassword: '',
    currentPassword: ''
  });
  const [emailFieldValue, setEmailFieldValue] = useState('');
  const [nameFieldValue, setNameFieldValue] = useState('');
  const [birthdayFieldValue, setBirthdayFieldValue] = useState<Date | null>(null);
  const [securityFieldValue, setSecurityFieldValue] = useState<SecurityFields>({
    question: '',
    answer: ''
  });
  const [deletePhotosCheckbox, setDeletePhotosCheckbox] = useState(false);
  const [pwError, setPwError] = useState<FormValidation[] | null>(null);
  const [emailError, setEmailError] = useState<FormValidation[] | null>(null);
  const [nameError, setNameError] = useState<FormValidation[] | null>(null);
  const [birthdayError, setBirthdayError] = useState<FormValidation[] | null>(null);
  const [secError, setSecError] = useState<FormValidation[] | null>(null);
  const [nsfwPreferences, setNsfwPreferences] = useState({ images: false, comments: false });
  const { setNotificationContext } = useNotificationContext();

  const onUserPropChange = async (
    which: keyof FullUserObj,
    value: string | boolean | Date,
    additional?: StringKeyBoolOrStringVal
  ) => {
    const changeUserProp: StringKeyBoolOrStringOrDateVal = { [which]: value };
    const updatedUser = await editSelf(changeUserProp, additional);
    if (updatedUser?.error) {
      setNotificationContext({
        message: (userPrefsMessages[which] as UserPropMessages).error,
        variant: 'error',
        closable: true
      });
    } else {
      setNotificationContext({
        message: (userPrefsMessages[which] as UserPropMessages).success,
        variant: 'success',
        closable: true
      });
      setUserContext(updatedUser);
    }
  };

  const onPasswordUpdate = async () => {
    const pwInvalid = validatePassword(
      passwordFieldValues.password,
      passwordFieldValues.confirmPassword
    );
    if (pwInvalid) {
      setPwError([{ key: 'pass-error', error: pwInvalid }]);
    } else {
      setPwError(null);
      await onUserPropChange('password', hashPassword(passwordFieldValues.password), {
        currentPw: hashPassword(passwordFieldValues.currentPassword)
      });
    }
  };

  const onEmailUpdate = async () => {
    const emailInvalid = await validateEmail(emailFieldValue);
    if (emailInvalid) {
      setEmailError([{ key: 'email-error', error: emailInvalid }]);
    } else {
      setEmailError(null);
      await onUserPropChange('email', emailFieldValue);
    }
  };

  const onNameUpdate = async () => {
    const nameInvalid = validateName(nameFieldValue);
    if (nameInvalid) {
      setNameError([{ key: 'name-error', error: nameInvalid }]);
    } else {
      setNameError(null);
      await onUserPropChange('name', nameFieldValue);
    }
  };

  const onBirthdayUpdate = async () => {
    const birthdayInvalid = false; // TODO: write this
    if (birthdayInvalid) {
      setBirthdayError([{ key: 'birthday-error', error: birthdayInvalid }]);
    } else if (birthdayFieldValue) {
      setBirthdayError(null);
      // await onUserPropChange('birthday', DateTime.fromJSDate(birthdayFieldValue).toISODate());
      await onUserPropChange('birthday', birthdayFieldValue);
    } else {
      setBirthdayError([
        { key: 'birthday-error-blank', error: 'You cannot leave the birthday field blank.' }
      ]);
    }
  };

  const onNsfwUpdate = async (which: any, value: boolean) => {
    const newNsfwSettings = { [which]: value };
    const updatedUser = await editSelf(newNsfwSettings);
    if (updatedUser?.error) {
      setNotificationContext({
        message: (userPrefsMessages.nsfwMessaging as UserPropMessages).error,
        variant: 'error',
        closable: true
      });
    } else {
      setNotificationContext({
        message: (userPrefsMessages.nsfwMessaging as UserPropMessages).success,
        variant: 'success',
        closable: true
      });
      setUserContext(updatedUser);
    }
  };

  const onSecurityUpdate = async () => {
    const secQueInvalid = validateSecurity(securityFieldValue.question);
    const secAnsInvalid = validateSecurity(securityFieldValue.answer, true);
    if (!secQueInvalid && !secAnsInvalid) {
      setSecError(null);
      await onUserPropChange('securityQuestion', securityFieldValue.question, {
        securityAnswer: securityFieldValue.answer
      });
    } else if (secQueInvalid) {
      setSecError([{ key: 'sec-que-error', error: secQueInvalid }]);
    } else if (secAnsInvalid) {
      setSecError([{ key: 'sec-ans-error', error: secAnsInvalid }]);
    }
  };

  // TODO: come back to the delete functions once basic photos functionality is written
  const deleteUserPhotos = () => {
    console.log('delete photos called');
    // TODO: show are you sure dialog
    // TODO: if sure, delete photos
    // TODO: if not sure, do nothing
  };

  const deleteUserAccount = () => {
    console.log('delete photos change val', deletePhotosCheckbox);
    // TODO: show are you sure dialog
    // TODO: if sure and checkbox, delete photos
    // TODO: if sure, delete account
    // TODO: if not sure, do nothing
    // TODO: restrict admin from deleting account unless there is at least 1 other admin; display message about making another admin first
  };

  useEffect(() => {
    if (userContextValue?.username) {
      setHighContrast(userContextValue.prefersHighContrast);
      setPreferredDisplay(userContextValue.preferredLabel || 'name');
      setUserDtFormat(userContextValue.dateTimeFormat || 'MM/dd/yyyy HH:mm a');
      setNameFieldValue(userContextValue.name || '');
      setEmailFieldValue(userContextValue.email || '');
      setBirthdayFieldValue(
        userContextValue?.birthday
          ? DateTime.fromISO(userContextValue.birthday).toJSDate()
          : new Date()
      );
      setNsfwPreferences({
        images: userContextValue.hideNsfwImages,
        comments: userContextValue.hideNsfwComments
      });
    }
  }, [userContextValue]);

  return (
    <Box sx={dashPrefsWrapper}>
      <Heading as="h1">User preferences</Heading>
      <Divider />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.prefersHighContrast?.title || ''}</Heading>
        <Text>{userPrefsMessages?.prefersHighContrast?.sub || ''}</Text>
        <Flex sx={dashPrefsInputFieldWrapperStyle}>
          <Switch
            onChange={() => {
              const newHc = !highContrast;
              setHighContrast(newHc);
              onUserPropChange('prefersHighContrast', newHc);
            }}
            label={userPrefsMessages?.prefersHighContrast?.label || ''}
            checked={highContrast}
          />
        </Flex>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.dateTimeFormat?.title || ''}</Heading>
        <Text>{userPrefsMessages?.dateTimeFormat?.sub || ''}</Text>
        <Flex sx={dashPrefsInputFieldWrapperStyle}>
          <Select
            onChange={(event) => {
              const newDt = event.target.value;
              setUserDtFormat(newDt);
              onUserPropChange('dateTimeFormat', newDt);
            }}
            aria-label={userPrefsMessages?.dateTimeFormat?.label || ''}
            value={userDtFormat}
            data-testid="dt-select"
          >
            {dateTimeFormats.map((item, index) => {
              return (
                <option value={item.format} key={`dt-format-option-${index}`}>
                  {item.display}
                </option>
              );
            })}
          </Select>
        </Flex>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.preferredLabel?.title || ''}</Heading>
        <Text>{userPrefsMessages?.preferredLabel?.sub || ''}</Text>
        <Flex sx={userPrefsRadioWrapperStyle}>
          <Label sx={{ mr: 5, width: 'auto' }}>
            <Radio
              name="use-username"
              value="username"
              checked={preferredDisplay === 'username'}
              onChange={() => {
                onUserPropChange('preferredLabel', 'username');
                setPreferredDisplay('username');
              }}
            />
            Username
          </Label>
          <Label>
            <Radio
              name="use-name"
              value="name"
              checked={preferredDisplay === 'name'}
              onChange={() => {
                onUserPropChange('preferredLabel', 'name');
                setPreferredDisplay('name');
              }}
            />
            Name
          </Label>
        </Flex>
      </Box>
      <Divider sx={mutedDividerStyle} />
      {canToggleNsfw(userContextValue?.birthday) && (
        <Box sx={userFieldStyle}>
          <Heading as="h2">{userPrefsMessages?.nsfwMessaging?.title || ''}</Heading>
          <Text>{userPrefsMessages?.nsfwMessaging?.sub || ''}</Text>
          <Box sx={userPrefsInputFieldWrapperStyle}>
            <Flex sx={{ ...dashPrefsInputFieldWrapperStyle, my: 1 }}>
              <Switch
                onChange={() => {
                  const newNsfwImages = !nsfwPreferences.images;
                  setNsfwPreferences({ ...nsfwPreferences, images: newNsfwImages });
                  onNsfwUpdate('hideNsfwImages', newNsfwImages);
                }}
                label={userPrefsMessages?.nsfwMessaging?.label || ''}
                checked={nsfwPreferences.images}
              />
            </Flex>
            <Flex sx={dashPrefsInputFieldWrapperStyle}>
              <Switch
                onChange={() => {
                  const newNsfwComments = !nsfwPreferences.comments;
                  setNsfwPreferences({ ...nsfwPreferences, comments: newNsfwComments });
                  onNsfwUpdate('hideNsfwComments', newNsfwComments);
                }}
                label={userPrefsMessages?.nsfwMessaging?.label2 || ''}
                checked={nsfwPreferences.comments}
              />
            </Flex>
          </Box>
        </Box>
      )}
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.password?.title || ''}</Heading>
        <Text>{userPrefsMessages?.password?.sub || ''}</Text>
        <Box sx={userPrefsInputFieldWrapperStyle}>
          <PasswordInputComponent
            placeHolder="Enter new password"
            setPasswordCb={(pass: string) => {
              setPasswordFieldValues({ ...passwordFieldValues, password: pass });
            }}
            showComplexity={true}
            fieldName="password"
            ariaLabel="Password"
            password={passwordFieldValues.password}
          />
          <PasswordInputComponent
            placeHolder="Confirm new password"
            setPasswordCb={(confirm: string) => {
              setPasswordFieldValues({ ...passwordFieldValues, confirmPassword: confirm });
            }}
            fieldName="confirmPassword"
            ariaLabel="Confirm password"
            password={passwordFieldValues.confirmPassword}
          />
          <PasswordInputComponent
            placeHolder="Enter current password for verification"
            setPasswordCb={(current: string) => {
              setPasswordFieldValues({ ...passwordFieldValues, currentPassword: current });
            }}
            fieldName="currentPassword"
            ariaLabel="CurrentPassword"
            password={passwordFieldValues.currentPassword}
          />
          {pwError && <FormErrorsComponent formErrors={pwError}></FormErrorsComponent>}
          <Button variant="outline" sx={{ mt: 4, width: 'auto' }} onClick={onPasswordUpdate}>
            Update password
          </Button>
        </Box>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.email?.title || ''}</Heading>
        <Text>{userPrefsMessages?.email?.sub || ''}</Text>
        <Box sx={userPrefsInputFieldWrapperStyle}>
          <Input
            placeholder="Enter new email address"
            value={emailFieldValue}
            onChange={(event) => setEmailFieldValue(event.target.value)}
          />
          {emailError && <FormErrorsComponent formErrors={emailError}></FormErrorsComponent>}
          <Button sx={{ mt: 4 }} variant="outline" onClick={onEmailUpdate}>
            Update email address
          </Button>
        </Box>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.name?.title || ''}</Heading>
        <Text>{userPrefsMessages?.name?.sub || ''}</Text>
        <Box sx={userPrefsInputFieldWrapperStyle}>
          <Input
            placeholder="Enter new name"
            value={nameFieldValue}
            onChange={(event) => setNameFieldValue(event?.target?.value || '')}
          />
          {nameError && <FormErrorsComponent formErrors={nameError}></FormErrorsComponent>}
          <Button sx={{ mt: 4 }} variant="outline" onClick={onNameUpdate}>
            Update name
          </Button>
        </Box>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.birthday?.title || ''}</Heading>
        <Text>{userPrefsMessages?.birthday?.sub || ''}</Text>
        <Box sx={userPrefsInputFieldWrapperStyle}>
          <CustomDatePicker
            currentValue={birthdayFieldValue}
            onChangeCb={(val: Date) => setBirthdayFieldValue(val)}
            dtFormat={userContextValue?.dateTimeFormat || null}
            id="user-prefs-birthday"
          />
          {birthdayError && <FormErrorsComponent formErrors={birthdayError}></FormErrorsComponent>}
          <Button sx={{ mt: 4 }} variant="outline" onClick={onBirthdayUpdate}>
            Update birthday
          </Button>
        </Box>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2">{userPrefsMessages?.securityQuestion?.title || ''}</Heading>
        <Text>{userPrefsMessages?.securityQuestion?.sub || ''}</Text>
        <Box sx={userPrefsInputFieldWrapperStyle}>
          <Input
            placeholder="Enter new security question"
            value={securityFieldValue.question}
            onChange={(event) =>
              setSecurityFieldValue({ ...securityFieldValue, question: event?.target?.value || '' })
            }
          />
          <Input
            placeholder="Enter new security answer"
            value={securityFieldValue.answer}
            onChange={(event) =>
              setSecurityFieldValue({ ...securityFieldValue, answer: event?.target?.value || '' })
            }
          />
          {secError && <FormErrorsComponent formErrors={secError}></FormErrorsComponent>}
          <Button sx={{ mt: 4 }} variant="outline" onClick={onSecurityUpdate}>
            Update security question
          </Button>
        </Box>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2" sx={{ color: 'error' }}>
          {userPrefsMessages?.deletePhotos?.title || ''}
        </Heading>
        <Text>{userPrefsMessages?.deletePhotos?.sub || ''}</Text>
        <Box>
          <Button variant="outline" sx={{ mt: 4 }} onClick={deleteUserPhotos}>
            Delete your photos
          </Button>
        </Box>
      </Box>
      <Divider sx={mutedDividerStyle} />
      <Box sx={userFieldStyle}>
        <Heading as="h2" sx={{ color: 'error' }}>
          {userPrefsMessages?.deleteAccount?.title || ''}
        </Heading>
        <Text>{userPrefsMessages?.deleteAccount?.sub || ''}</Text>
        <Box sx={{ ...userPrefsInputFieldWrapperStyle, mt: 4 }}>
          <Label>
            <Checkbox
              checked={deletePhotosCheckbox}
              onChange={() => setDeletePhotosCheckbox(!deletePhotosCheckbox)}
            />
            Delete your uploaded photos as well?
          </Label>
          <Button variant="outline" sx={{ mt: 4 }} onClick={deleteUserAccount}>
            Delete your account
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default UserPreferencesComponent;
