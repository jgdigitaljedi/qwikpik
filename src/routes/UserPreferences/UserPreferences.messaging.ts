// Just pulled out the copy to make the component file easier to read

import { UserPropMessages } from 'types/commonTypes';
import { FullUserObj } from 'types/userTypes';

type DeletePhotos = { deletePhotos: UserPropMessages };
type DeleteAccount = { deleteAccount: UserPropMessages };
type NSFWPreferences = { nsfwMessaging: UserPropMessages };
type UserPropsMessaging = { [Property in keyof FullUserObj]: UserPropMessages };
export type UserPrefsMessaging = Partial<
  UserPropsMessaging & NSFWPreferences & DeletePhotos & DeleteAccount
>;

const prefersHighContrast: UserPropMessages = {
  title: 'High contrast themes',
  sub: 'Enable high contrast dark and light themes if you find it difficult to read text with the default theme.',
  error:
    'An error occurred while trying to save your setting for high contrast themes. Please try again.',
  success: 'Your high contrast preference was saved!',
  label: 'Enable high contrast themes'
};

const dateTimeFormat: UserPropMessages = {
  title: 'Date/Time format',
  sub: 'Select the date/time format that will be displayed.',
  error:
    'An error occurred while trying to save your preferred date/time format. Please try again.',
  success: 'Your date/time format preference was saved!',
  label: 'Change preferred date/time format'
};

const preferredLabel: UserPropMessages = {
  title: 'Preferred display name',
  sub: 'Choose to display either your username or name when others visit your profile and uploaded images.',
  error:
    'An error occurred while trying to save your preferred display name setting. Please try again.',
  success: 'Your preferred label preference was saved!',
  label: 'Preferred display name'
};

const password: UserPropMessages = {
  title: 'Change password',
  sub: 'You can change your password here as long as it meets the requirements',
  error: 'An error occurred while trying to save your new password. Please try again.',
  success: 'Your password was changed!',
  label: 'Change password',
  tooltip: ''
};

const email: UserPropMessages = {
  title: 'Change email',
  sub: 'Change the email address associated with your user profile',
  error: 'An error occurred while trying to save your new email address. Please try again.',
  success: 'Your email address was updated!',
  label: 'Change email'
};

const name: UserPropMessages = {
  title: 'Change name',
  sub: 'Change the name associated with your profile',
  error: 'An error occurred while trying to save your name. Please try again.',
  success: 'Your name was updated!',
  label: 'Change name'
};

const birthday: UserPropMessages = {
  title: 'Change birthday',
  sub: 'Your birthday is used to determine if NSFW images and comments should be toggled off by default. If you are 17 or older, you can change your NSFW settings youself.',
  error: 'An error occurred while trying to change your birthday. Please try again.',
  success: 'Your birthday was updated!',
  label: 'Change birthday'
};

const nsfwMessaging: UserPropMessages = {
  title: 'NSFW preferences',
  sub: 'Choose whether or not images and comments marked as NSFW show by default or are hidden with the option to show on click.',
  error: 'An error occurred while trying to change you NSFW preferences. Please try again.',
  success: 'Your NSFW preferences were updated!',
  label: 'Hide NSFW images',
  label2: 'Hide NSFW comments'
};

const securityQuestion: UserPropMessages = {
  title: 'Change security question',
  sub: `Change the security question and answer that will be used if you need to reset your password. 
  PLEASE write this question and answer down somewhere because you will not be able to reset your password without it!`,
  error: 'An error occurred while trying to save your new security question. Please try again.',
  success: 'Your security question and answer were updated!',
  label: 'Change security question'
};

const deletePhotos: UserPropMessages = {
  title: 'Delete your photos',
  sub: `WARNING: This will delete all of the photos you have uploaded. You will be prompted for your current password 
  to confirm this action. This action cannot be undone!`,
  error:
    'An error occurred while trying to delete your uploaded photos. Please refresh and try again.',
  success: 'Your uploaded photos were deleted!',
  label: 'Delete your photos'
};

const deleteAccount: UserPropMessages = {
  title: 'Delete your account',
  sub: `WARNING: This will delete all your entire account. You can additionally choose to delete all of the photos 
  you have uploaded as well. If you do not choose to have your photos deleted, they will simply show that they 
  were uploaded by "UNKNOWN". You will be prompted for your current password to confirm this action. This action cannot 
  be undone!`,
  error: 'An error occurred while trying to delete your account. Please refresh and try again.',
  success: "Your account was deleted. We're sorry to se you go.",
  label: 'Delete your account'
};

export const userPrefsMessages: UserPrefsMessaging = {
  prefersHighContrast,
  dateTimeFormat,
  preferredLabel,
  password,
  email,
  name,
  birthday,
  nsfwMessaging,
  securityQuestion,
  deletePhotos,
  deleteAccount
};
