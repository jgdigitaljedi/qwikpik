import React from 'react';
import LoggedInHome from 'routes/Home/LoggedInHome';
import LoginComponent from 'components/LoginComponent';
import { useUserContext } from 'contexts/user.context';
import { Flex } from 'theme-ui';

const Home: React.FC = () => {
  const { userContextValue } = useUserContext();

  if (userContextValue.username) {
    return <LoggedInHome />;
  }

  return (
    <Flex sx={{ width: '100%', display: 'flex', justifyContent: 'center', mt: 2 }}>
      <LoginComponent />
    </Flex>
  );
};

export default Home;
