import Breadcrumbs from 'components/Breadcrumbs';
import ImageGallery from 'components/ImageGallery';
import React, { useEffect, useState } from 'react';
import { getGallery } from 'services/gallery.service';
import { Box, Heading } from 'theme-ui';
import { BaseDirectory } from 'types/directoryTypes';
import { BaseImage } from 'types/imageTypes';
import { useNotificationContext } from 'contexts/notification.context';

interface GalleryItems {
  images: BaseImage[];
  directory: BaseDirectory | null;
}

const Gallery: React.FC = () => {
  const { setNotificationContext } = useNotificationContext();
  const [galleryData, setGalleryData] = useState<GalleryItems>({ images: [], directory: null });
  const fetchGallery = async (dir?: string) => {
    const gallery = await getGallery(dir);
    if (gallery.error) {
      setNotificationContext({
        message: 'There was an error fetching the gallery content. Please refresh and try again.',
        variant: 'error',
        closable: false,
        position: 'bottom-right'
      });
    }
    console.log('gallery', gallery);
    setGalleryData(gallery);
  };

  const subdirCb = (dir: string) => {
    fetchGallery(dir);
  };

  const onBcClick = (subDir: string): void => {
    const newDir = subDir === 'home' ? undefined : subDir;
    fetchGallery(newDir);
  };

  /** plans
   * - show breadcrumbs to represent current directory and parent dirs
   * - have some sort of "Root" or "Home" option to go back to top of gallery instantly
   * - make sure to filer images marked nsfw depending on current user settings
   * - fetch call needs to be paginated
   * - gallery will be infinite scroll so cb between this and ImageGallery to know when to fetch more
   * - add search
   * - add filters and sorts
   */

  useEffect(() => {
    fetchGallery();
  }, []);
  return (
    <Box>
      <Heading as="h1">Gallery</Heading>
      <Breadcrumbs currentDir={galleryData?.directory} onBcClick={onBcClick} />
      {galleryData && (
        <ImageGallery
          images={galleryData.images}
          directory={galleryData.directory}
          subdirCb={subdirCb}
        />
      )}
    </Box>
  );
};

export default Gallery;
