import React from 'react';
import { screen, fireEvent, waitFor } from '@testing-library/react';
import { createWrapper, mockUser } from '../../__mocks__/renderHelpers';
import AdminUserListRow from '../AdminDashboard/AdminUserList/AdminUserListRow';
import { BaseUserObj, UserRoles } from '../../types/userTypes';
const MockUsers = require('../../../scripts/seedCollections/users/users');
import * as adminCalls from '../../services/admin.service';
import { Box } from 'theme-ui';

describe('<AdminUserListRow />', () => {
  const mockAdmin = { ...mockUser, role: 'admin' as UserRoles };
  const mockUserObj = (MockUsers as BaseUserObj[])[0];
  const mockInactiveUserObj = (MockUsers as BaseUserObj[])[2];
  const render = (Comp: JSX.Element) =>
    createWrapper({
      Comp: <Box sx={{ width: '100%' }}>{Comp}</Box>,
      loggedIn: mockAdmin,
      isChild: true
    });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should display the expect fields', async () => {
    render(<AdminUserListRow user={mockUserObj} actionCb={jest.fn()} />);

    await screen.findByText('Name');
    expect(screen.getByText('Username')).toBeInTheDocument();
    expect(screen.getByText('Active')).toBeInTheDocument();
    expect(screen.getByText('Date added')).toBeInTheDocument();
    expect(screen.getByText('Actions')).toBeInTheDocument();
    expect(screen.getByText(mockUserObj.name)).toBeInTheDocument();
    expect(screen.getByText(mockUserObj.username)).toBeInTheDocument();
  });

  it('should call to deactivate user when option selected from actions select', async () => {
    const deactivateCall = jest
      .spyOn(adminCalls, 'adminDeactivateUser')
      .mockImplementationOnce(() => Promise.resolve(true));
    const actionCb = jest.fn();
    render(<AdminUserListRow user={mockUserObj} actionCb={actionCb} />);

    await screen.findByText('Name');
    const actionsDd = screen.getByTestId('user-actions');
    fireEvent.change(actionsDd, { target: { value: 'onDeactivateUser' } });

    await waitFor(() => expect(actionCb).toHaveBeenCalled());
    expect(deactivateCall).toHaveBeenCalled();
  });

  it('should call delete user when option selected from actions select', async () => {
    const deleteCall = jest
      .spyOn(adminCalls, 'adminDeleteUser')
      .mockImplementationOnce(() => Promise.resolve(true));
    const actionCb = jest.fn();
    render(<AdminUserListRow user={mockUserObj} actionCb={actionCb} />);

    await screen.findByText('Name');
    const actionsDd = screen.getByTestId('user-actions');
    fireEvent.change(actionsDd, { target: { value: 'onDeleteUser' } });

    await waitFor(() => expect(actionCb).toHaveBeenCalled());
    expect(deleteCall).toHaveBeenCalled();
  });

  it('should call approve user when option selected from actions select', async () => {
    const approveCall = jest
      .spyOn(adminCalls, 'adminApproveUser')
      .mockImplementationOnce(() => Promise.resolve({ ...mockInactiveUserObj, active: true }));
    const actionCb = jest.fn();
    render(<AdminUserListRow user={mockInactiveUserObj} actionCb={actionCb} />);

    await screen.findByText('Name');
    const actionsDd = screen.getByTestId('user-actions');
    fireEvent.change(actionsDd, { target: { value: 'onApproveUser' } });

    await waitFor(() => expect(actionCb).toHaveBeenCalled());
    expect(approveCall).toHaveBeenCalled();
  });

  it('should open edit modal when option selected from actions select', async () => {
    const actionCb = jest.fn();
    render(<AdminUserListRow user={mockInactiveUserObj} actionCb={actionCb} />);

    await screen.findByText('Name');
    const actionsDd = screen.getByTestId('user-actions');
    fireEvent.change(actionsDd, { target: { value: 'onEditUser' } });

    await waitFor(() => expect(screen.getByText('Preferred label'))); // modal opened
  });
});
