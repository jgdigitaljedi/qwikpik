import React from 'react';
import { screen, fireEvent, waitFor } from '@testing-library/react';
import { createWrapper, mockUser } from '../../__mocks__/renderHelpers';
import AdminUserList from '../AdminDashboard/AdminUserList/index';
import * as userServiceCalls from '../../services/user.service';
import { BaseUserObj, UserRoles } from '../../types/userTypes';
const MockUsers = require('../../../scripts/seedCollections/users/users');

const mockAdmin = { ...mockUser, role: 'admin' as UserRoles };
const emptyMessage = 'Test empty message';
const getUsersMock = jest.fn(() => {
  return Promise.resolve({ users: MockUsers as BaseUserObj[], total: 18 });
});

jest.spyOn(userServiceCalls, 'getAllUsers').mockImplementation(getUsersMock);

// TODO: I hate how I did the dropdown tests (its better than nothing for now), but I was getting frustrated. Fix it.

describe('<AdminUserList />', () => {
  const render = (Comp: JSX.Element) => createWrapper({ Comp, loggedIn: mockAdmin, isChild: true });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('should render the expected result with from mockedData', async () => {
    render(<AdminUserList role="admin" inactiveOnly={false} emptyMessage={emptyMessage} />);

    await screen.findByText('Users per page:');

    expect(screen.getAllByText('Name')).toHaveLength(24);
    expect(screen.getAllByText('Username')).toHaveLength(24);
    expect(screen.getAllByText('Active')).toHaveLength(24);
    expect(screen.getAllByText('Date added')).toHaveLength(24);
    expect(screen.getAllByText('Actions')).toHaveLength(24);
    expect(screen.getByText('Aloy')).toBeInTheDocument();
    expect(screen.getByText('Horizon')).toBeInTheDocument();
  });

  it('should call getUsers when the sort select is changed', async () => {
    render(<AdminUserList role="admin" inactiveOnly={false} emptyMessage={emptyMessage} />);

    await screen.findByText('Users per page:');
    expect(getUsersMock).toHaveBeenCalledTimes(6);
    const sortDd = screen.getByTestId('user-sort');
    fireEvent.change(sortDd, { target: { value: 'lastModified-desc' } });

    await waitFor(() => {
      expect(getUsersMock).toHaveBeenCalledTimes(7);
    });
  });

  it('should call getUsers when the Users per page select is changed', async () => {
    render(<AdminUserList role="admin" inactiveOnly={false} emptyMessage={emptyMessage} />);

    await screen.findByText('Users per page:');
    expect(getUsersMock).toHaveBeenCalledTimes(10);
    const usersDd = screen.getByTestId('user-num');
    fireEvent.change(usersDd, { target: { value: '20' } });

    await waitFor(() => {
      expect(getUsersMock).toHaveBeenCalledTimes(12);
    });
  });

  it('should render the empty message in an h3 tag when the users call returns nothing', async () => {
    jest.spyOn(userServiceCalls, 'getAllUsers').mockImplementationOnce(() => Promise.resolve([]));
    render(<AdminUserList role="admin" inactiveOnly={false} emptyMessage={emptyMessage} />);

    await screen.findByText(emptyMessage);
  });
});
