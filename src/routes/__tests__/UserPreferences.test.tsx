import React from 'react';
import { screen, waitFor, fireEvent } from '@testing-library/react';
import { createWrapper, mockUser } from '../../__mocks__/renderHelpers';
import userEvent from '@testing-library/user-event';
import UserPreferences from '../UserPreferences';
import { BaseUserWithToken } from '../../types/userTypes';
import { userPrefsMessages } from '../UserPreferences/UserPreferences.messaging';
import * as userServiceCalls from '../../services/user.service';
import { dateTimeFormats, hashPassword } from '../../utils/userHelpers';

const editSelfMock = jest.fn();
jest.spyOn(userServiceCalls, 'editSelf').mockImplementation(editSelfMock);

// TODO: add tests for error handling and messaging
describe('<UserPreferences />', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });
  const render = (loggedIn?: Partial<BaseUserWithToken>) =>
    createWrapper({ Comp: UserPreferences, loggedIn });

  it('should render the expected elements on load', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    Object.keys(userPrefsMessages).map((key) => {
      // @ts-ignore
      expect(screen.getAllByText(userPrefsMessages[key].title).length).toBeGreaterThanOrEqual(1);
    });
    expect((await screen.findAllByRole('button')).length).toEqual(7);
    expect((await screen.findAllByRole('checkbox')).length).toEqual(4);
  });

  it('should change high contrast preferences', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const hcSwitch = screen.getByText('Enable high contrast themes');
    expect(hcSwitch).toBeInTheDocument();

    userEvent.click(hcSwitch);
    expect(editSelfMock).toHaveBeenCalledWith({ prefersHighContrast: true }, undefined);

    userEvent.click(hcSwitch);
    expect(editSelfMock).toHaveBeenCalledWith({ prefersHighContrast: false }, undefined);
  });

  it('should change date/time format preferences', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const dtDd = screen.getByTestId('dt-select');
    expect(dtDd).toBeInTheDocument();
    fireEvent.change(dtDd, { target: { value: dateTimeFormats[3].format } });

    await waitFor(() =>
      expect(editSelfMock).toHaveBeenCalledWith(
        { dateTimeFormat: dateTimeFormats[3].format },
        undefined
      )
    );
  });

  it('should change preferred label preferences', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const unameRadio = await screen.findByLabelText('Username');
    expect(unameRadio).toBeInTheDocument();
    userEvent.click(unameRadio);

    await waitFor(() =>
      expect(editSelfMock).toHaveBeenCalledWith({ preferredLabel: 'username' }, undefined)
    );
  });

  it('should change name', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const nameInput = await screen.findByPlaceholderText('Enter new name');
    expect(nameInput).toBeInTheDocument();

    userEvent.type(nameInput, ' Jr');
    expect(nameInput).toHaveValue('Testy McTesterson Jr');

    userEvent.click(screen.getByText('Update name'));
    await waitFor(() =>
      expect(editSelfMock).toHaveBeenCalledWith({ name: 'Testy McTesterson Jr' }, undefined)
    );
  });

  it('should change email address', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const emailInput = screen.getByPlaceholderText('Enter new email address');
    expect(emailInput).toBeInTheDocument();

    userEvent.type(emailInput, '.org');
    expect(emailInput).toHaveValue('test@test.com.org');

    userEvent.click(screen.getByText('Update email address'));
    await waitFor(() =>
      expect(editSelfMock).toHaveBeenCalledWith({ email: 'test@test.com.org' }, undefined)
    );
  });

  it('should change password', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    userEvent.type(screen.getByPlaceholderText('Enter new password'), 'Unittester1$');
    userEvent.type(screen.getByPlaceholderText('Confirm new password'), 'Unittester1$');
    userEvent.type(
      screen.getByPlaceholderText('Enter current password for verification'),
      'Tester123#'
    );
    userEvent.click(screen.getByText('Update password'));

    await waitFor(() =>
      expect(editSelfMock).toHaveBeenCalledWith(
        { password: hashPassword('Unittester1$') },
        { currentPw: hashPassword('Tester123#') }
      )
    );
  });

  it('should change security question and answer', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    userEvent.type(screen.getByPlaceholderText('Enter new security question'), 'What it be');
    userEvent.type(screen.getByPlaceholderText('Enter new security answer'), 'It be good');
    userEvent.click(screen.getByText('Update security question'));

    await waitFor(() =>
      expect(editSelfMock).toHaveBeenCalledWith(
        { securityQuestion: 'What it be' },
        { securityAnswer: 'It be good' }
      )
    );
  });

  it('should change nsfw prefs for images', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const nsfwImageRadio = await screen.findByLabelText('Hide NSFW images');
    expect(nsfwImageRadio).toBeInTheDocument();
    userEvent.click(nsfwImageRadio);

    await waitFor(() => expect(editSelfMock).toHaveBeenCalledWith({ hideNsfwImages: true }));
  });

  it('should change nsfw prefs for comments', async () => {
    render(mockUser);

    await screen.findByText('User preferences');

    const nsfwCommentRadio = await screen.findByLabelText('Hide NSFW comments');
    expect(nsfwCommentRadio).toBeInTheDocument();
    userEvent.click(nsfwCommentRadio);

    await waitFor(() => expect(editSelfMock).toHaveBeenCalledWith({ hideNsfwComments: true }));
  });
  // TODO: write delete functionality tests once photos base functionality added and delete functions written
});
