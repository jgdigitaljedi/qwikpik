import React from 'react';
import { screen } from '@testing-library/react';
import { createThemeOnlyWrapper } from '../../__mocks__/renderHelpers';
import AdminUserStats from '../AdminDashboard/AdminUserStats';

const mockedStats = { active: 6, inactive: 12, total: 18 };

const render = () => createThemeOnlyWrapper([<AdminUserStats stats={mockedStats} />]);

describe('<AdminUserStats />', () => {
  it('should render the expected stats with the given data', async () => {
    render();

    await screen.findByText('User stats');

    expect(screen.findByText('Approved users'));
    expect(screen.findByText('Total users'));
    expect(screen.findByText('Unapproved users'));
    expect(screen.findByText('6'));
    expect(screen.findByText('12'));
    expect(screen.findByText('18'));
  });
});
