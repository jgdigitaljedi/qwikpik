import React, { ReactElement } from 'react';
import { screen, fireEvent, waitFor } from '@testing-library/react';
import { within } from '@testing-library/dom';
import { createWrapper, mockUser } from '../../__mocks__/renderHelpers';
import AdminUserEditModalBody from '../AdminDashboard/AdminUserList/AdminUserEditModalBody';
import { BaseUserObj, UserRoles } from '../../types/userTypes';
const MockUsers = require('../../../scripts/seedCollections/users/users');
import * as adminCalls from '../../services/admin.service';
import { Box } from 'theme-ui';
import userEvent from '@testing-library/user-event';
import { hashPassword } from '../../utils/userHelpers';

describe('<AdminUserEditModalBody />', () => {
  const mockAdmin = { ...mockUser, role: 'admin' as UserRoles };
  const mockUserObj = (MockUsers as BaseUserObj[])[1];
  const render = (Comp: JSX.Element, supervisor?: boolean) =>
    createWrapper({
      Comp: <Box sx={{ width: '100%' }}>{Comp}</Box>,
      loggedIn: supervisor ? { ...mockUser, role: 'supervisor' } : mockAdmin,
      isChild: true
    });
  const editCall = jest
    .spyOn(adminCalls, 'adminEditUser')
    .mockImplementation(() => Promise.resolve(true));

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.resetAllMocks();
  });

  it('should show the modal fields', async () => {
    render(<AdminUserEditModalBody updatedCb={jest.fn()} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    expect(screen.getAllByText('Name')).toHaveLength(2);
    expect(screen.getByDisplayValue(mockUserObj.name)).toBeInTheDocument();
    expect(screen.getAllByText('Username')).toHaveLength(2);
    expect(screen.getByDisplayValue(mockUserObj.username)).toBeInTheDocument();
    expect(screen.getAllByText('Reset password')).toHaveLength(2);
    expect(screen.getByText('Email')).toBeInTheDocument();
    expect(screen.getByDisplayValue(mockUserObj.email)).toBeInTheDocument();
    expect(screen.getByText('Approved/Unapproved status')).toBeInTheDocument();
    expect(screen.getByLabelText('Approved')).toBeChecked();
    expect(screen.getByText('Role')).toBeInTheDocument();
    expect(screen.getByDisplayValue(mockUserObj.role)).toBeInTheDocument();
    expect(screen.getByText('Cancel')).toBeInTheDocument();
    expect(screen.getByText('Save changes')).toBeInTheDocument();
    const roleDd = screen.getByTestId('user-role');
    userEvent.click(roleDd);
    expect(within(roleDd).getByText('admin')).toBeInTheDocument();
  });

  it('should change username', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    const unameField = screen.getByDisplayValue(mockUserObj.username);

    userEvent.clear(unameField);
    userEvent.click(screen.getByText('Save changes'));
    expect(editCall).not.toHaveBeenCalled(); // didn't pass validation

    userEvent.clear(unameField);
    userEvent.type(unameField, 'Tifa');
    userEvent.click(screen.getByText('Save changes'));
    await waitFor(() =>
      expect(editCall).toHaveBeenCalledWith({ ...mockUserObj, username: 'Tifa' })
    );
    expect(cb).toHaveBeenCalled();
  });

  it('should change preferred label', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    userEvent.click(screen.getByDisplayValue('username'));
    userEvent.click(screen.getByText('Save changes'));
    await waitFor(() =>
      expect(editCall).toHaveBeenCalledWith({ ...mockUserObj, preferredLabel: 'username' })
    );
    expect(cb).toHaveBeenCalled();
  });

  it('should change name', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    const nameField = screen.getByDisplayValue(mockUserObj.name);

    userEvent.clear(nameField);
    userEvent.click(screen.getByText('Save changes'));
    expect(editCall).not.toHaveBeenCalled(); // didn't pass validation

    userEvent.clear(nameField);
    userEvent.type(nameField, 'Tifa');
    userEvent.click(screen.getByText('Save changes'));
    await waitFor(() => expect(editCall).toHaveBeenCalledWith({ ...mockUserObj, name: 'Tifa' }));
    expect(cb).toHaveBeenCalled();
  });

  it('should change email', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    const emailField = screen.getByDisplayValue(mockUserObj.email);

    userEvent.clear(emailField);
    userEvent.type(emailField, 'Tifa');
    userEvent.click(screen.getByText('Save changes'));
    expect(editCall).not.toHaveBeenCalled(); // didn't pass validation

    userEvent.clear(emailField);
    userEvent.type(emailField, 'Tifa@ffseven.com');
    userEvent.click(screen.getByText('Save changes'));
    await waitFor(() =>
      expect(editCall).toHaveBeenCalledWith({ ...mockUserObj, email: 'Tifa@ffseven.com' })
    );
    expect(cb).toHaveBeenCalled();
  });

  it('should change role', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    const roleDd = screen.getByTestId('user-role');
    fireEvent.change(roleDd, { target: { value: 'supervisor' } });
    userEvent.click(screen.getByText('Save changes'));
    await waitFor(() =>
      expect(editCall).toHaveBeenCalledWith({ ...mockUserObj, role: 'supervisor' })
    );
    expect(cb).toHaveBeenCalled();
  });

  it('should reset password', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />);

    await screen.findByText('Preferred label');
    userEvent.click(screen.getByLabelText('Reset password')); // expand password reset field
    const newPwField = screen.getByPlaceholderText('Reset password');

    userEvent.clear(newPwField);
    userEvent.type(newPwField, 'j');
    userEvent.click(screen.getByText('Save changes'));
    expect(editCall).not.toHaveBeenCalled(); // didn't pass validation

    userEvent.clear(newPwField);
    userEvent.type(newPwField, 'ThisIsATest1234$');
    userEvent.click(screen.getByText('Save changes'));

    await waitFor(() =>
      expect(editCall).toHaveBeenCalledWith({
        ...mockUserObj,
        password: hashPassword('ThisIsATest1234$')
      })
    );
  });

  it('should not allow changing to admin user if logged in user is only supervisor', async () => {
    const cb = jest.fn();
    render(<AdminUserEditModalBody updatedCb={cb} user={mockUserObj} hide={jest.fn()} />, true);

    await screen.findByText('Approved/Unapproved status');
    const roleDd = screen.getByTestId('user-role');
    userEvent.click(roleDd);
    expect(within(roleDd).getByText('supervisor')).toBeInTheDocument();
    expect(within(roleDd).queryByText('admin')).toBeNull();
  });
});
