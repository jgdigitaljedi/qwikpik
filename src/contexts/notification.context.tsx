import React, {
  createContext,
  useState,
  Dispatch,
  SetStateAction,
  useContext,
  ReactElement
} from 'react';
import { NotificationProps, ToastPosition, ToastVariant } from '../types/commonTypes';

interface NotificationContextProps {
  value?: NotificationProps;
  children: (ReactElement<any> | React.FC<any>)[];
}

interface UserContextHook {
  notificationContextValue: NotificationProps;
  setNotificationContext: (values: NotificationProps) => void;
  defaultNotificationState: NotificationProps;
}

const defaultNotificationState = {
  variant: 'info' as ToastVariant,
  message: '',
  position: 'bottom-right' as ToastPosition | undefined,
  closable: true
};

const NotificationContext = createContext<
  [NotificationProps, Dispatch<SetStateAction<NotificationProps>>]
>([defaultNotificationState, () => defaultNotificationState]);

const useNotificationContext = (): UserContextHook => {
  const [notificationContextVals, setNotificationContextVals] = useContext(NotificationContext);

  const handleChange = (newValues: NotificationProps) => {
    setNotificationContextVals(newValues);
  };
  return {
    notificationContextValue: notificationContextVals,
    setNotificationContext: handleChange,
    defaultNotificationState
  };
};

const NotificationContextProvider = ({ value, children }: NotificationContextProps) => {
  const [notify, setNotify] = useState(value || defaultNotificationState);
  return (
    // @ts-ignore
    <NotificationContext.Provider value={[notify, setNotify]}>
      {children}
    </NotificationContext.Provider>
  );
};

export { NotificationContext, NotificationContextProvider, useNotificationContext };
