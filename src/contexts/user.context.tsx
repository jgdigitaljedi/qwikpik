import React, {
  createContext,
  useContext,
  Dispatch,
  SetStateAction,
  useState,
  ReactElement
} from 'react';
import { DateTimeFormatType, UserPrefsPreferredDisplay, UserRoles } from 'types/userTypes';

interface UserContextValues {
  username: string | null;
  email: string | null;
  role: UserRoles | null;
  active: boolean;
  dateCreated: string | null;
  lastModified: string | null;
  token: string | null;
  name: string | null;
  birthday: string | null;
  hideNsfwImages: boolean;
  hideNsfwComments: boolean;
  prefersHighContrast: boolean;
  preferredLabel: UserPrefsPreferredDisplay;
  dateTimeFormat: DateTimeFormatType;
  _id: string | null;
}

interface UserContextProviderProps {
  value?: UserContextValues;
  children: ReactElement<any> | React.FC<any> | (ReactElement<any> | React.FC<any>)[];
}

interface UserContextHook {
  userContextValue: UserContextValues;
  setUserContext: (values: UserContextValues) => void;
}

export const defaultUserContextValues: UserContextValues = {
  username: null,
  name: null,
  birthday: null,
  hideNsfwImages: false,
  hideNsfwComments: false,
  email: null,
  role: null,
  active: false,
  dateCreated: null,
  lastModified: null,
  prefersHighContrast: false,
  preferredLabel: 'name',
  dateTimeFormat: 'MM/dd/yyyy HH:mm a',
  _id: null,
  token: null
};

const UserContext = createContext<[UserContextValues, Dispatch<SetStateAction<UserContextValues>>]>(
  [defaultUserContextValues, () => defaultUserContextValues]
);

const useUserContext = (): UserContextHook => {
  const [userContextVals, setUserContextVals] = useContext(UserContext);

  const handleChange = (newValues: UserContextValues) => {
    setUserContextVals(newValues);
  };
  return { userContextValue: userContextVals, setUserContext: handleChange };
};

const UserContextProvider = ({ value, children }: UserContextProviderProps) => {
  const [userContextVals, setUserContextVals] = useState(value || defaultUserContextValues);
  return (
    <UserContext.Provider value={[userContextVals, setUserContextVals]}>
      {children}
    </UserContext.Provider>
  );
};

export { UserContextProvider, useUserContext };
