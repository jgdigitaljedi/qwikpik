import React, {
  createContext,
  useContext,
  Dispatch,
  SetStateAction,
  useState,
  ReactElement
} from 'react';
import { BaseUserObj } from 'types/userTypes';

interface AppContextValues {
  navMenuOpen: boolean;
}

interface AppContextProviderProps {
  value?: AppContextValues;
  children: ReactElement<any> | React.FC<any> | (ReactElement<any> | React.FC<any>)[];
}

interface AppContextHook {
  appContextValue: AppContextValues;
  onAppContextChange: (values: AppContextValues) => void;
}

const defaultAppContextValues = {
  navMenuOpen: false
};

const AppContext = createContext<[AppContextValues, Dispatch<SetStateAction<AppContextValues>>]>([
  defaultAppContextValues,
  () => defaultAppContextValues
]);

const useAppContext = (): AppContextHook => {
  const [appContextVals, setAppContextVals] = useContext(AppContext);

  const handleChange = (newValues: AppContextValues) => {
    setAppContextVals(newValues);
  };
  return { appContextValue: appContextVals, onAppContextChange: handleChange };
};

const AppContextProvider = ({ value, children }: AppContextProviderProps) => {
  const [appContextVals, setAppContextVals] = useState(value || defaultAppContextValues);
  return (
    <AppContext.Provider value={[appContextVals, setAppContextVals]}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContextProvider, useAppContext };
