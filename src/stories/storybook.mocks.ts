export const mockUserContext = {
  email: 'test@test.com',
  username: 'tester',
  role: 'admin',
  active: true,
  dateCreated: '',
  lastModified: '',
  token: '8675309',
  name: 'Testy McTesterson',
  birthday: '',
  hideNsfwImages: false,
  hideNsfwComments: false,
  prefersHighContrast: false,
  preferredLabel: 'username',
  dateTimeFormat: 'MM/dd/yyyy',
  _id: '8675309'
};

export const placeholderImage = {
  _id: '6235c96d2404823b322d6fa4',
  tags: ['twitter', 'marriage', 'meme'],
  title: 'Placeholder 300',
  isNsfw: false,
  path: ',root,memes,',
  directory: 'memes',
  fileName: 'download (1).jpeg',
  uploadDate: '2022-03-19T12:15:41.238Z',
  lastModified: '2022-03-19T12:15:41.238Z',
  uploadedBy: '6235c96c2404823b322d6f8d'
};

export const mockDirectory = {
  _id: 'root',
  name: '',
  path: '',
  children: ['logos', 'memes'],
  createdDate: '2022-03-19T12:15:41.190+00:00',
  lastModified: '2022-03-19T12:24:14.705+00:00',
  createdBy: '6235c96c2404823b322d6f8c'
};
