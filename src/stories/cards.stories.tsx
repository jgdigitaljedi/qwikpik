import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Card, CardProps } from 'theme-ui';

export default {
  title: 'Theme-UI/Card',
  component: Card
} as ComponentMeta<typeof Card>;

interface CardStory extends CardProps {
  text: string;
}

const Template: ComponentStory<typeof Card> = (args) => {
  return <Card variant={args.variant}>{`${args.variant} card`}</Card>;
};

export const Default = Template.bind({});
Default.args = {
  variant: 'primary'
};

export const Sunken = Template.bind({});
Sunken.args = {
  variant: 'sunken'
};

export const Interactive = Template.bind({});
Interactive.args = {
  variant: 'interactive'
};

export const GalleryCard = Template.bind({});
GalleryCard.args = {
  variant: 'galleryCard'
};

export const Translucent = Template.bind({});
Translucent.args = {
  variant: 'translucent'
};

export const TranslucentDark = Template.bind({});
TranslucentDark.args = {
  variant: 'translucentDark'
};
