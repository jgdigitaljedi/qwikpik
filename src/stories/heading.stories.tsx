import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Box, Heading } from 'theme-ui';

export default {
  title: 'Theme-UI/Heading',
  component: Heading
} as ComponentMeta<typeof Heading>;

const Template: ComponentStory<typeof Heading> = (args) => {
  return (
    <Box sx={{ div: { my: 2 } }}>
      <Heading as="h1" {...args}>
        Heading H1
      </Heading>
      <Heading as="h2" {...args}>
        Heading H2
      </Heading>
      <Heading as="h3" {...args}>
        Heading H3
      </Heading>
      <Heading as="h4" {...args}>
        Heading H4
      </Heading>
      <Heading as="h5" {...args}>
        Heading H5
      </Heading>
      <Heading as="h6" {...args}>
        Heading H6
      </Heading>
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {};
