import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Button, Flex } from 'theme-ui';

export default {
  title: 'Theme-UI/Button',
  component: Button
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => {
  return (
    <Flex sx={{ flexWrap: 'wrap', button: { m: 1 } }}>
      <Button variant="primary" {...args}>
        Primary
      </Button>
      <Button variant="lg" {...args}>
        Large
      </Button>
      <Button variant="outline" {...args}>
        Outline
      </Button>
      <Button variant="outlineLg" {...args}>
        Outline Lg
      </Button>
      <Button variant="cta" {...args}>
        CTA
      </Button>
      <Button variant="ctaLg" {...args}>
        CTA Lg
      </Button>
      <Button variant="linkButton" {...args}>
        Link Button
      </Button>
      <Button variant="textButton" {...args}>
        Text Button
      </Button>
      <Button variant="pureText" {...args}>
        Pure Text
      </Button>
    </Flex>
  );
};

export const Default = Template.bind({});
Default.args = {};
