import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Box, Link, NavLink } from 'theme-ui';

export default {
  title: 'Theme-UI/Link',
  component: Link
} as ComponentMeta<typeof Link>;

const Template: ComponentStory<typeof Link> = (args) => {
  return (
    <Box sx={{ a: { my: 2 } }}>
      <Link href="!#" {...args}>
        Link
      </Link>
      <br />
      <Link href="!#" variant="nav" {...args}>
        Nav variant
      </Link>
      <br />
      <NavLink href="#!" p={2}>
        Navlink
      </NavLink>
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {};
