import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Box, Text } from 'theme-ui';

export default {
  title: 'Theme-UI/Text',
  component: Text
} as ComponentMeta<typeof Text>;

const Template: ComponentStory<typeof Text> = (args) => {
  return (
    <Box sx={{ div: { my: 2 } }}>
      <Text as="div" variant="heading" {...args}>
        Heading
      </Text>
      <Text as="div" variant="ultratitle" {...args}>
        Ultratitle
      </Text>
      <Text as="div" variant="title" {...args}>
        Title
      </Text>
      <Text as="div" variant="subtitle" {...args}>
        Subtitle
      </Text>
      <Text as="div" variant="headline" {...args}>
        Headline
      </Text>
      <Text as="div" variant="subheadline" {...args}>
        Sub headline
      </Text>
      <Text as="div" variant="eyebrow" {...args}>
        Eyebrow
      </Text>
      <Text as="div" variant="lead" {...args}>
        Lead
      </Text>
      <Text as="div" variant="caption" {...args}>
        Caption
      </Text>
      <Text as="div" variant="errorText" {...args}>
        ErrorText
      </Text>
    </Box>
  );
};

export const Default = Template.bind({});
Default.args = {};
