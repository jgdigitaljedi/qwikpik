import {
  signupErrorMessages,
  validateNewUserFields,
  validateEmail,
  validatePassword,
  validateUsername,
  validateName,
  validateSecurity,
  validateLoginValues,
  hashPassword
} from '../userHelpers';
import * as userServiceCalls from '../../services/user.service';

const emailMock = jest.fn(() => Promise.resolve({ inUse: false }));
const emailFnMock = jest.spyOn(userServiceCalls, 'checkIfEmailInUse').mockImplementation(emailMock);

const validValues = [
  'tester',
  'Tester1234*',
  'Tester1234*',
  'test@test.com',
  'Testsy McTesterson',
  'Is this a test',
  'Yes',
  new Date()
];

describe('userHelpers', () => {
  it('should return and empty array from validateNewUserFields when values are valid', async () => {
    const newUserVals = {
      username: validValues[0] as string,
      password: validValues[1] as string,
      pwConfirm: validValues[2] as string,
      email: validValues[3] as string,
      name: validValues[4] as string,
      secQuestion: validValues[5] as string,
      secAnswer: validValues[6] as string,
      birthday: validValues[7] as Date
    };
    const validated = await validateNewUserFields(
      newUserVals.username,
      newUserVals.password,
      newUserVals.pwConfirm,
      newUserVals.email,
      newUserVals.name,
      newUserVals.secQuestion,
      newUserVals.secAnswer,
      newUserVals.birthday
    );
    expect(validated).toEqual([]);
  });

  it('should get the proper failures and result from validateEmail', async () => {
    expect(await validateEmail('tester@tester.com')).toBeNull();
    expect(await validateEmail('')).toBe(signupErrorMessages.emailBlank);
    expect(await validateEmail('asd@o')).toBe(signupErrorMessages.emailInvalid);

    emailFnMock.mockImplementationOnce(() => Promise.resolve({ inUse: true }));
    expect(await validateEmail('asd@asd.com')).toBe(signupErrorMessages.emailTaken);
  });

  it('should get the proper failures and  result for validatePassword', () => {
    expect(validatePassword('', '')).toBe(signupErrorMessages.pwInvalid);
    expect(validatePassword('Asdfqwer23$', 'Asdfqwer34%')).toBe(signupErrorMessages.pwMatch);
  });

  it('should get the proper failures and  result for validateUsername', () => {
    expect(validateUsername('')).toBe(signupErrorMessages.unInvalid);
    expect(validateUsername('asd')).toBe(signupErrorMessages.unInvalid);
    expect(validateUsername('Asdfqwer asd')).toBeNull();
  });

  it('should get the proper failures and  result for validateName', () => {
    expect(validateName('')).toBe(signupErrorMessages.nameInvalid);
    expect(validateName('as$d')).toBe(signupErrorMessages.nameInvalid);
    expect(validateName('Asdfqwer asd')).toBeNull();
  });

  it('should get the proper failures and  result for validateSecurity', () => {
    expect(validateSecurity('')).toBe(signupErrorMessages.secQueInvalid);
    expect(validateSecurity('', true)).toBe(signupErrorMessages.secAnsInvalid);
    expect(validateSecurity('Test question')).toBeNull();
    expect(validateSecurity('Test answer', true)).toBeNull();
  });

  it('should get the proper failures and results for validateLoginValues', () => {
    expect(validateLoginValues('asdf', 'Tester1234$')).toEqual([]);
    expect(validateLoginValues('asdf', 'tes')).toEqual([
      { key: 'password', error: signupErrorMessages.loginPassword }
    ]);
    expect(validateLoginValues('', 'Tester1234$')).toEqual([
      { key: 'username', error: signupErrorMessages.loginUsername }
    ]);
  });

  it('should hash a password consistently', () => {
    expect(hashPassword('Tester1234$')).toEqual(
      '-1597229801-85008248-16321190141590388931-714258240-1832367343-1711533889329845969'
    );
  });
});
