export const alphaNumeric = (str: string) => /^[\w\d\/]+$/.test(str);
