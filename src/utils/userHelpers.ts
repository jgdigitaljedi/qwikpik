import { passwordIsValid } from 'stringman-utils';
import { checkIfEmailInUse } from 'services/user.service';
import { FormValidation } from 'types/commonTypes';
import sha256 from 'crypto-js/sha256';
import { DateTime } from 'luxon';

const lettersRegex = /^[\w\s]+$/;
export const signupErrorMessages = {
  emailBlank: 'You must provide an email address to register.',
  emailInvalid: 'Email address is not valid',
  emailTaken: 'Email address already registered.',
  pwInvalid: 'You must use a valid password.',
  pwMatch: 'Passwords do not match.',
  pwSimple: 'Password does not meet the requirements.',
  unInvalid: 'Username is not long enough (must be at least 4 characters).',
  nameInvalid:
    'Name is invalid and must be at least 1 letter and contain only letters, numbers, and spaces.',
  secAnsInvalid:
    'You must provide an answer to your security question so you will be able to reset your password, if need be.',
  secQueInvalid:
    'You must provide a security question so you will be able to reset your password, if need be.',
  loginUsername: 'You entered an invalid username.',
  loginPassword:
    'The password you entered does not meet the minimum requirements so it cannot be your password.'
};

export async function validateEmail(email: string | undefined): Promise<string | null> {
  if (!email || !email?.length) {
    return Promise.resolve(signupErrorMessages.emailBlank);
  }
  const isValid = email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  if (!isValid) {
    return Promise.resolve(signupErrorMessages.emailInvalid);
  }
  const { inUse } = await checkIfEmailInUse(email as string);
  if (inUse) {
    return Promise.resolve(signupErrorMessages.emailTaken);
  }
  return Promise.resolve(null);
}

export function validatePassword(password: string, confirmPassword: string): string | null {
  if (!password) {
    return signupErrorMessages.pwInvalid;
  }
  if (password !== confirmPassword) {
    return signupErrorMessages.pwMatch;
  }
  return passwordIsValid(password, {
    cap: true,
    lc: true,
    num: true,
    min: 8,
    max: 20,
    special: '-_=+*$&!@#%?'
  })
    ? null
    : signupErrorMessages.pwSimple;
}

export function validateUsername(username: string): string | null {
  return username?.length > 3 ? null : signupErrorMessages.unInvalid;
}

export function validateName(name: string): string | null {
  return name?.length > 0 && name.match(lettersRegex) ? null : signupErrorMessages.nameInvalid;
}

export function validateSecurity(str: string, isAnswer?: boolean): string | null {
  const hasLength = str?.length > 0;
  if (hasLength) {
    return null;
  } else if (isAnswer) {
    return signupErrorMessages.secAnsInvalid;
  }
  return signupErrorMessages.secQueInvalid;
}

export function validateBirthday(birthday: Date) {
  if (!birthday) {
    return 'Please enter a valid birthday.';
  }
  const earliest = DateTime.fromFormat('1900', 'yyyy'); // ain't gonna be anyone over 122 years old
  const latest = DateTime.now();
  const isAfter = DateTime.fromJSDate(birthday) > earliest;
  const isBefore = DateTime.fromJSDate(birthday) < latest;
  return isBefore && isAfter ? null : 'Please enter a valid birthday.';
}

export async function validateNewUserFields(
  username: string,
  password: string,
  pwConfirm: string,
  email: string,
  name: string,
  secQuestion: string,
  secAnswer: string,
  birthday: Date
): Promise<FormValidation[]> {
  const emailValidated = await validateEmail(email);
  return Promise.resolve(
    [
      { key: 'username', error: validateUsername(username) },
      { key: 'password', error: validatePassword(password, pwConfirm) },
      { key: 'email', error: emailValidated },
      { key: 'name', error: validateName(name) },
      { key: 'secQuestion', error: validateSecurity(secQuestion) },
      { key: 'secAnswer', error: validateSecurity(secAnswer, true) },
      { key: 'birthday', error: validateBirthday(birthday) }
    ].filter((e) => e.error)
  );
}

export function validateLoginValues(username: string, password: string) {
  const unValid = username?.length > 3 ? null : signupErrorMessages.loginUsername;
  const pwValid = !!validatePassword(password, password) ? signupErrorMessages.loginPassword : null;
  return [
    { key: 'username', error: unValid },
    { key: 'password', error: pwValid }
  ].filter((e) => e.error);
}

export function hashPassword(pw: string): any {
  return sha256(pw).words.join('');
}

export const dateTimeFormats = [
  { display: '01/31/2022 09:00 pm', format: 'MM/dd/yyyy hh:mm a' },
  { display: '01/31/2022 21:00', format: 'MM/dd/yyyy HH:mm' },
  { display: '31/01/2022 09:00 pm', format: 'dd/MM/yyyy hh:mm a' },
  { display: '31/01/2022 21:00', format: 'dd/MM/yyyy HH:mm' }
];

export const canToggleNsfw = (birthday: string | null): boolean => {
  const bday = birthday ? DateTime.fromISO(birthday) : DateTime.now();
  const today = DateTime.now();
  const diffInYears = today.diff(bday, 'years').toObject();
  return diffInYears?.years ? diffInYears.years >= 17 : false;
};
