export const paginationPerPageOptions = [3, 5, 10, 20, 50, 100];
export const sortByOptions = [
  { label: 'Name - asc', value: 'name-asc' },
  { label: 'Name - desc', value: 'name-desc' },
  { label: 'Date added - asc', value: 'dateCreated-asc' },
  { label: 'Date added - desc', value: 'dateCreated-desc' },
  { label: 'Date last modified - asc', value: 'lastModified-asc' },
  { label: 'Date last modified - desc', value: 'lastModified-desc' }
];
