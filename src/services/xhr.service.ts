import Axios from 'axios';

type ReqMethod = 'get' | 'post' | 'put' | 'delete';
const baseHeaders = {
  crossorigin: 'true',
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

const instance = Axios.create({
  baseURL: process.env.BASEURL,
  timeout: 2000
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return error?.response?.data
      ? { data: error.response.data }
      : { data: { error: true, message: 'An error has occurred. Please refresh and try again.' } };
  }
);
export interface ReqOptions {
  method: ReqMethod;
  endpoint: string;
  body?: object;
  includeToken?: boolean;
  includeMultiHeader?: boolean;
}

export function makeRequest({
  method,
  endpoint,
  body,
  includeToken,
  includeMultiHeader
}: ReqOptions): Promise<any> {
  const authHeader = { Authorization: `Bearer ${localStorage.getItem('qpToken')}` };
  const multipartHeader = { 'content-type': 'multipart/form-data' };
  const authedHeaders = includeToken ? { ...baseHeaders, ...authHeader } : baseHeaders;
  const finalHeaders = includeMultiHeader ? { ...authedHeaders, ...multipartHeader } : authHeader;
  return instance.request({
    method,
    url: endpoint,
    data: body,
    headers: finalHeaders
  });
}
