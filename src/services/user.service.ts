import { BaseUserWithToken, FullUserRequest } from 'types/userTypes';
import { makeRequest } from './xhr.service';
import { hashPassword, validateNewUserFields } from 'utils/userHelpers';
import {
  FormValidation,
  GenericError,
  StringKeyBoolOrStringOrDateVal,
  StringKeyBoolOrStringVal
} from 'types/commonTypes';

export async function checkIfEmailInUse(email: string) {
  try {
    const result = await makeRequest({ method: 'post', endpoint: '/email', body: { email } });
    return { inUse: result.data.inUse };
  } catch (error) {
    return Promise.resolve({
      error: true,
      message: 'Something went wrong. Please refresh and try again.',
      inUse: true // just sending true to stop validation process
    });
  }
}

export async function logInUser(
  username: string,
  password: string
): Promise<BaseUserWithToken | GenericError> {
  if (username?.length && password?.length) {
    try {
      const hashed = hashPassword(password);
      const { data } = await makeRequest({
        method: 'post',
        endpoint: '/login',
        body: { username, password: hashed }
      });
      if (data.error) {
        localStorage.removeItem('qpToken');
        return Promise.resolve({
          error: true,
          message: data.message
        });
      }
      return Promise.resolve(data.user);
    } catch (error) {
      return Promise.resolve({
        error: true,
        message: 'There was a problem logging you in. Please try again.'
      });
    }
  }
  return Promise.resolve({ error: true, message: 'Username or password field invalid' });
}

export async function getUserObjFromToken(): Promise<BaseUserWithToken | GenericError> {
  const token = localStorage.getItem('qpToken');
  if (token?.length) {
    const { data } = await makeRequest({
      method: 'get',
      endpoint: '/usertoken',
      includeToken: true
    });
    if (data.error) {
      localStorage.removeItem('qpToken');
      return Promise.resolve({
        error: true,
        message: 'You token has expired. Please login again.'
      });
    }
    return data;
  }
  return Promise.resolve({ error: true, message: 'Please login first.' });
}

export async function editSelf(
  prop: StringKeyBoolOrStringOrDateVal,
  additional?: StringKeyBoolOrStringVal
) {
  const body = additional ? { prop, ...additional } : { prop };
  const { data } = await makeRequest({
    method: 'post',
    endpoint: '/updateself',
    body,
    includeToken: true
  });
  if (!data || data?.error) {
    return data;
  }
  localStorage.setItem('qpToken', data.token);
  return data;
}

export async function addUser({
  username,
  password,
  pwConfirm,
  email,
  name,
  secQuestion,
  secAnswer,
  birthday
}: FullUserRequest): Promise<BaseUserWithToken | FormValidation[] | GenericError> {
  const validationErrors = await validateNewUserFields(
    username,
    password,
    pwConfirm,
    email,
    name,
    secQuestion,
    secAnswer,
    birthday
  );
  if (validationErrors?.length > 0) {
    return Promise.resolve(validationErrors);
  } else {
    const hashed = hashPassword(password);
    const newUser = {
      username,
      password: hashed,
      name,
      email,
      securityQuestion: secQuestion,
      securityAnswer: secAnswer,
      birthday
    };
    const { data } = await makeRequest({
      method: 'post',
      endpoint: '/register',
      body: { user: newUser }
    });
    return data;
  }
}

export function deleteSelf() {
  /** called when user chooses to delete account
   * - make delete call
   * - if success returned, maybe show something then log them out
   * - if failure, show error message and do nothing
   */
}

export async function getAllUsers(
  role: string | null,
  amount = 0,
  start = 0,
  inactiveOnly = false,
  sortBy = 'name-asc'
) {
  if (role === 'basic') {
    return Promise.resolve({ error: true, message: 'You do not have permission to do that.' });
  }
  const { data } = await makeRequest({
    method: 'post',
    endpoint: '/getusers',
    includeToken: true,
    body: { amount, start, sortBy, inactiveOnly }
  });
  return data;
}
