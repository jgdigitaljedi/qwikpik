import { AdminUserStats, EditedUser } from 'types/adminTypes';
import { GenericError } from 'types/commonTypes';
import { BaseUserObj } from 'types/userTypes';
import { getUserObjFromToken } from './user.service';
import { makeRequest } from './xhr.service';

interface EditUserProps {}

async function hasPermission(includeSuper?: boolean): Promise<boolean> {
  const currentUser = await getUserObjFromToken();
  if (includeSuper) {
    return Promise.resolve(
      !(currentUser as GenericError)?.error &&
        ((currentUser as BaseUserObj)?.role === 'admin' ||
          (currentUser as BaseUserObj)?.role === 'supervisor')
    );
  } else {
    return Promise.resolve(
      !(currentUser as GenericError)?.error && (currentUser as BaseUserObj)?.role === 'admin'
    );
  }
}

export async function adminEditUser(user: EditedUser) {
  const isAllowed = await hasPermission(true);
  if (isAllowed) {
    const { data } = await makeRequest({
      method: 'post',
      endpoint: '/adminedituser',
      body: { toEdit: user },
      includeToken: true
    });
    if (!data || data?.error) {
      return data;
    }
    return data;
  }
}

export async function adminApproveUser(user: BaseUserObj): Promise<BaseUserObj | GenericError> {
  const isAllowed = await hasPermission(true);
  if (isAllowed) {
    const { data } = await makeRequest({
      method: 'post',
      endpoint: '/approve',
      body: { whoseId: user._id },
      includeToken: true
    });
    return (
      data.value || {
        error: true,
        message:
          'Something went wrong and the user was not approved. Please refresh the page and try again.'
      }
    );
  } else {
    return { error: true, message: 'You do not have permission to do that.' };
  }
}

export async function adminDeleteUser(user: BaseUserObj): Promise<boolean | GenericError> {
  const isAllowed = await hasPermission(true);
  if (isAllowed) {
    const { data } = await makeRequest({
      method: 'delete',
      endpoint: '/deleteuser',
      body: { whoseId: user._id },
      includeToken: true
    });
    return (
      data || {
        error: true,
        message:
          'Something went wrong and the user was not deleted. Please refresh the page and try again.'
      }
    );
  } else {
    return { error: true, message: 'You do not have permission to do that.' };
  }
}

export async function adminDeactivateUser(user: BaseUserObj): Promise<boolean | GenericError> {
  const isAllowed = await hasPermission(true);
  if (isAllowed) {
    const { data } = await makeRequest({
      method: 'post',
      endpoint: '/deactivateuser',
      body: { whoseId: user._id },
      includeToken: true
    });
    return (
      data || {
        error: true,
        message:
          'Something went wrong and the user was not deactivated. Please refresh the page and try again.'
      }
    );
  } else {
    return { error: true, message: 'You do not have permission to do that.' };
  }
}

export async function getAdminUserStats(): Promise<GenericError | AdminUserStats> {
  const isAllowed = await hasPermission(true);
  if (isAllowed) {
    const { data } = await makeRequest({
      method: 'get',
      endpoint: '/adminuserstats',
      includeToken: true
    });
    return (
      data || {
        error: true,
        message:
          'Something went wrong and the user stats could not be fetched. Please refresh the page and try again.'
      }
    );
  } else {
    return { error: true, message: 'You do not have permission to do that.' };
  }
}
