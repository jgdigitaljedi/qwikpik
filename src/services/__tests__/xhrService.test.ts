import axios from 'axios';
import { makeRequest, ReqOptions } from '../xhr.service';

localStorage.setItem('token', 'asdfgasdfg');
const mockData = { test: true, error: false };
const baseUrl = 'http://test.com';
const baseHeaders = {
  crossorigin: 'true',
  Accept: 'application/json',
  'Content-Type': 'application/json'
};
jest.mock('axios', () => {
  return {
    create: () => {
      return {
        request: (options: any) => {
          return Promise.resolve({
            data: mockData,
            request: {
              method: options.method,
              url: `${baseUrl}${options.url}`,
              data: options.data,
              headers: options.headers
            }
          });
        },
        interceptors: {
          request: { use: jest.fn(), eject: jest.fn() },
          response: { use: jest.fn(), eject: jest.fn() }
        }
      };
    }
  };
});

describe('xhr.service', () => {
  beforeAll(() => {
    localStorage.setItem('qpToken', 'asdfgasdfg');
  });
  afterAll(() => {
    localStorage.removeItem('qpToken');
    jest.clearAllMocks();
  });

  it('should call makeRequest and get the expected return without sending bearer', async () => {
    // @ts-ignore
    const result = await makeRequest({
      method: 'get',
      endpoint: '/test',
      body: { isTest: true },
      includeToken: false
    });
    expect(result).toEqual({
      data: mockData,
      request: {
        method: 'get',
        url: `${baseUrl}/test`,
        data: { isTest: true },
        headers: baseHeaders
      }
    });
  });

  it('should call makeRequest and get the expected return sending bearer', async () => {
    // @ts-ignore
    const result = await makeRequest({
      method: 'get',
      endpoint: '/test',
      body: { isTest: true },
      includeToken: true
    });
    expect(result).toEqual({
      data: mockData,
      request: {
        method: 'get',
        url: `${baseUrl}/test`,
        data: { isTest: true },
        headers: { ...baseHeaders, Authorization: 'Bearer asdfgasdfg' }
      }
    });
  });
});
