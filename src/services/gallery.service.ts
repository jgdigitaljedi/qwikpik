import { makeRequest } from './xhr.service';

export function createGallery() {}

export function deleteGallery() {}

export async function getGallery(directory?: string) {
  const { data } = await makeRequest({
    method: 'post',
    endpoint: '/gallery',
    body: { directory }
  });
  if (data.error) {
    return Promise.resolve({
      error: true,
      message: data.message
    });
  }
  return Promise.resolve(data);
}
