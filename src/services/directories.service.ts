import { BaseDirectory } from 'types/directoryTypes';
import { makeRequest } from './xhr.service';

export async function getDirectories(directory?: string) {
  const { data } = await makeRequest({
    method: 'post',
    endpoint: '/directories',
    body: { directory }
  });
  if (data.error) {
    return Promise.resolve({
      error: true,
      message: data.message
    });
  }
  return Promise.resolve(data.directories);
}

export async function addDirectory(newDirName: string, currentDirectory: BaseDirectory) {
  const { data } = await makeRequest({
    method: 'put',
    endpoint: '/directories',
    body: { newDirName, currentDirectory },
    includeToken: true
  });
  if (data.error) {
    return Promise.resolve({
      error: true,
      message: data.message
    });
  }
  return Promise.resolve(data);
}
