import { ImageUploadBody } from 'types/imageTypes';
import { makeRequest } from './xhr.service';

export async function getImages(directory?: string) {
  const { data } = await makeRequest({
    method: 'post',
    endpoint: '/images',
    body: { directory },
    includeToken: true // TODO: make backend check token first
  });
  if (data.error) {
    return Promise.resolve({
      error: true,
      message: data.message
    });
  }
  return Promise.resolve(data.images);
}

export function searchImages() {}

// export async function addImage(files: any, details: any) {
//   // TODO: get images and corresponding details object
//   const { data } = await makeRequest({
//     method: 'post',
//     endpoint: '/upload',
//     body: { files, details }
//   });
//   if (data.error) {
//     return Promise.resolve({
//       error: true,
//       message: data.message
//     });
//   }
//   return Promise.resolve(data.images);
// }

export async function addImages(request?: FormData) {
  console.log('reqBody in service', request);
  const reqBody = { formData: request?.getAll('images'), details: request?.getAll('data') };
  if (reqBody) {
    const { data } = await makeRequest({
      method: 'put',
      endpoint: '/upload',
      body: { reqBody },
      includeToken: true,
      includeMultiHeader: true
    });
    if (data.error) {
      return Promise.resolve({
        error: true,
        message: data.message
      });
    }
    return Promise.resolve(data.images);
  }
  return Promise.resolve({
    error: true,
    message: 'You sent an empty request. No images were uploaded.'
  });
}

export function editImage() {}

export function deleteImage() {}
