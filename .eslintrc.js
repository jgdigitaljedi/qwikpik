module.exports = {
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaVersion: 11
  },
  files: ['server/*'],
  env: { jest: false, node: true, mongo: true, es2020: true, browser: false, amd: true },
  extends: [
    'eslint:recommended',
    'plugin:prettier/recommended',
    'eslint:recommended',
    'plugin:jest/recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
    'plugin:jsx-a11y/recommended',
    'prettier/@typescript-eslint'
  ],
  plugins: ['prettier'],
  rules: {
    'no-console': 'warn',
    yoda: 'error'
  },
  overrides: [
    {
      // typescript overrides
      files: ['*.ts', '*.tsx'],
      rules: {
        'spaced-comment': ['warn', 'always', { markers: ['#'] }], // gettext comments
        // note you must disable the base rule as it can report incorrect errors https://stackoverflow.com/a/63961972/1044498
        'no-shadow': 'off',
        '@typescript-eslint/no-shadow': ['error'],
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'warn'
      }
    },
    {
      // jest environment for test files
      files: ['**/__tests__/**', '**/__mocks__/**', '**/*.spec.*', '**/*.test.*'],
      env: {
        jest: true
      },
      extends: ['plugin:testing-library/react'],
      rules: {
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error',
        '@typescript-eslint/no-explicit-any': 'off'
      },
      settings: {
        jest: {
          version: require('jest/package.json').version
        }
      }
    }
  ]
};
