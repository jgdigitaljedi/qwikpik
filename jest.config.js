const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  globals: {
    'ts-jest': {
      tsconfig: 'tsconfig.json'
    },
    NODE_ENV: 'test'
  },
  coverageDirectory: 'coverage',
  cacheDirectory: '.jest-cache',
  collectCoverage: true,
  collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx,ts,tsx}'],
  // resolver: 'jest-webpack-resolver',
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '^.+\\.ts?$': 'ts-jest'
  },
  snapshotSerializers: [
    '@emotion/jest/serializer' /* if needed other snapshotSerializers should go here */
  ],
  setupFilesAfterEnv: ['<rootDir>/jest-setup.js'],
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.[jt]sx?$',
  // testEnvironment: 'jsdom',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  moduleDirectories: ['src', 'node_modules'],
  moduleNameMapper: {
    // Resolve .css and similar files to identity-obj-proxy instead.
    '.+\\.(css|styl|less|sass|scss)$': '<rootDir>/src/__mocks__/styleMock.js',
    // Resolve .jpg and similar files to __mocks__/file-mock.js
    '.+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': `<rootDir>/src/__mocks__/file-mock.js`,
    '@components': '<rootDir>/src/components',
    '@context': '<rootDir>/src/context',
    '^@routes/(.*)$': '<rootDir>/src/routes/$1',
    '@services': '<rootDir>/src/services',
    '@types': '<rootDir>/src/types',
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  verbose: true,
  testPathIgnorePatterns: ['/node_modules/', '/server/']
};
